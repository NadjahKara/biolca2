import React from "react";

import { Logo } from "../components/uikit/surfaces";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Logos",
  argTypes: {
    size: {
      control: {
        type: "select",
        options: ["xs", "s", "base", "md", "lg"]
      }
    },
    small: {
      control: {
        type: "boolean"
      }
    }
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <Logo {...args} />;

export const Small = Template.bind({});
Small.args = {
  small: true
};

export const Large = Template.bind({});
Large.args = {
  small: false
};
