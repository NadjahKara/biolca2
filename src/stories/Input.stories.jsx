import React from "react";

import { TextInput } from "../components/uikit/inputs";
import { BiSearch } from "react-icons/bi";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Input",
  args: {
    placeholder: "placeholder",
    defaultValue: "",
    label: "",
    error: "",
    underText: ""
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <TextInput {...args} />;

export const Default = Template.bind({});
export const Search = Template.bind({});
Search.args = {
  icon: <BiSearch />
};
