import React from "react";

import { TextArea } from "../components/uikit/inputs";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/TextArea",
  args: {
    placeholder: "placeholder",
    defaultValue: "",
    label: "",
    error: "",
    underText: "",
    rows: 2
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <TextArea {...args} />;

export const Default = Template.bind({});
