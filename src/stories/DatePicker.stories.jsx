import React from "react";
import { useForm } from "react-hook-form";
import { useArgs } from "@storybook/client-api";
import { Button, DatePicker } from "../components/uikit/inputs";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "UI-KIT/DatePicker",
  argTypes: {
    label: {
      type: "string",
      description: "label de l'input"
    },
    error: {
      type: "string",
      description: "Erreur à afficher à coté du label"
    },
    name: {
      type: "string",
      description: "Nom pour enregistrer l'input via react-hook-form"
    },
    underText: {
      type: "string",
      description: "Texte à afficher en dessous de l'input"
    },
    placeholder: {
      type: "string",
      description:
        "Texte à afficher dans l'input avant qu'une valeur lui soit affecté"
    },
    required: {
      type: "boolean",
      description: "Prop pour indiquer si l'input doit avoir une valeur ou pas"
    },
    setValue: {
      description:
        "Prop extrait de react-hook-form à passer sans modification (voir le code)"
    },
    shouldUnregister: {
      type: "boolean",
      description:
        "Prop pour indiquer si la valeur de l'input doit être effacée lorsqu'elle n'est visible"
    },
    register: {
      description:
        "Prop extrait de react-hook-form à passer sans modification (voir le code)"
    },
    defaultValue: {
      description:
        "la valeur par défaut à injecter dans l'input (et react-hook-form)",
      table: {
        type: { summary: "Date.toDateString() result" }
        // defaultValue: { summary: "Date.now()" }
      },
      control: "date"
    }
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => {
  const { register, handleSubmit, setValue } = useForm();
  const [, setArgs] = useArgs();
  return (
    <form
      onSubmit={handleSubmit((data) => {
        console.log({ data });
      })}
    >
      <DatePicker register={register} setValue={setValue} {...args} />
      <Button type="submit">Submit</Button>
      <Button
        type="button"
        className="mx-2"
        onClick={() => {
          setArgs({
            ...args,
            defaultValue: Date.now()
          });
        }}
      >
        date d'aujourd'hui
      </Button>
    </form>
  );
};

// sur cet exemple, on est entrain de récupérer la valeur de l'input via react-hook-form, en utilisant setValue et register
export const Default = Template.bind({});
Default.args = {
  label: "Calendrier",
  defaultValue: Date.now(),
  name: "date"
};
