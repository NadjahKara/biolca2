import React from "react";

import { DropArea } from "../components/uikit/inputs";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/DropArea",
  argTypes: {}
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <DropArea {...args} />;

export const Default = (args) => {
  const [files, setFiles] = React.useState([]);
  return (
    <div className="w-screen flex justify-center items-center">
      <Template {...args} files={files} setFiles={setFiles} />
    </div>
  );
};

Default.args = {
  title: "Cliquer ou glisser un fichier à cette zone pour l'uploader",
  caption: "Sous-texte pour expliquer l'utilisation concrète de la drop zone"
};
