import React from "react";

import { Divider } from "../components/uikit/surfaces";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Divider"
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <Divider {...args} />;

export const Default = Template.bind({});
