import React from "react";

import { Dropdown, DropdownItem } from "../components/uikit/inputs";

export default {
  title: "UI-KIT/Dropdown",
  args: {
    label: ""
  }
};

const Template = (args) => (
  <Dropdown {...args}>
    <DropdownItem label="Example 1" />
    <DropdownItem label="Example 2" />
    <DropdownItem label="Example 3" />
  </Dropdown>
);

export const Default = Template.bind({});
Default.args = {
  label: "...",
  overflow: "left"
};
