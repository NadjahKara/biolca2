import React from "react";

import { Modal } from "../components/uikit/surfaces";
import { Table } from "../components/uikit/dataDisplay";
import { Button } from "../components/uikit/inputs";
import styled from "styled-components";
import { useArgs } from "@storybook/client-api";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "UI-KIT/Modals",
  argTypes: {
    isOpen: {
      description:
        "state controlling the opening of the modal (true => modal open close otherwise)",
      type: "boolean"
    },
    setIsOpen: {
      description: "setter controling isOpen state",
      type: "() => void"
    },
    title: {
      description: "title display at the top of the modal",
      type: "string"
    },
    children: {
      description: "react children inside the modal",
      type: "react.element"
    },
    disabled: {
      description:
        "boolean use to disabled the clonsing of the dialog, usally use to disabled the dialog if an action is running",
      type: "boolean"
    },
    maxWidth: {
      description: "max width of the dialog 80% by default",
      type: "string"
    },
    buttonProps: {
      description: "props passed to the confirm button",
      type: "any"
    },
    cancelButtonProps: {
      description: "props passed to the cancel button",
      type: "any"
    },
    onClose: {
      description: "function trigger on close of the modal",
      type: "() => void"
    }
  },
  args: {
    isOpen: true,
    title: "Modal's title",
    buttonProps: {
      children: "Confirm",
      variant: "primary"
    },
    setIsOpen: () => alert("trigger isOpen Update"),
    children: []
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Container = styled.div`
  height: 100vh;
  width: 100vw;
`;

const DefaultTemplate = (args) => {
  const [, setArgs] = useArgs();

  const handleIsOpen = (value) => {
    setArgs({ ...args, isOpen: value });
  };

  return (
    <Container>
      <Modal
        {...args}
        setIsOpen={handleIsOpen}
        buttonProps={{
          children: "Confirm",
          variant: "primary",
          onClick: () => handleIsOpen(false)
        }}
      />
    </Container>
  );
};

const DeleteTemplate = (args) => {
  const [, setArgs] = useArgs();

  const handleIsOpen = (value) => {
    setArgs({ ...args, isOpen: value });
  };

  return (
    <Container>
      <Modal
        {...args}
        setIsOpen={handleIsOpen}
        buttonProps={{
          children: "Delete",
          variant: "danger",
          onClick: () => handleIsOpen(false)
        }}
      />
    </Container>
  );
};

const ButtonModalTemplate = (args) => {
  const [, setArgs] = useArgs();

  const handleIsOpen = (value) => {
    setArgs({ ...args, isOpen: value });
  };
  return (
    <>
      <Button
        onClick={() => {
          handleIsOpen(true);
        }}>
        Open Modal
      </Button>
      <Modal
        {...args}
        setIsOpen={handleIsOpen}
        buttonProps={{
          children: "Confirm",
          variant: "primary",
          onClick: () => handleIsOpen(false)
        }}
      />
    </>
  );
};

export const ConstructiveModal = DefaultTemplate.bind({});

export const DestructiveModal = DeleteTemplate.bind({});

export const ButtonWithModel = ButtonModalTemplate.bind({});
export const ModalWithTable = (args) => {
  const [, setArgs] = useArgs();

  const handleIsOpen = (value) => {
    setArgs({ ...args, isOpen: value });
  };
  return (
    <>
      <Button
        onClick={() => {
          handleIsOpen(true);
        }}>
        Open Modal
      </Button>
      <Modal
        {...args}
        setIsOpen={handleIsOpen}
        title="Modal's title"
        buttonProps={{
          children: "Confirm",
          variant: "primary",
          onClick: () => handleIsOpen(false)
        }}>
        <Table
          columns={[
            { title: "Nom de l'attribut", dataIndex: "name" },
            { title: "Type", dataIndex: "type", width: 125 },
            { title: "Description", dataIndex: "description", width: 200 }
          ]}
          minHeight=""
          dataSource={[
            {
              id: "1",
              name: "Test",
              type: "Type test",
              description:
                "ceci est une longue description * 5,ceci est une longue description * 5,ceci est une longue description * 5,ceci est une longue description * 5,ceci est une longue description * 5,ceci est une longue description * 5,"
            }
          ]}
        />
      </Modal>
    </>
  );
};
