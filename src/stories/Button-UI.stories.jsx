import React from "react";

import { Button } from "../components/uikit/inputs";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Buttons",
  argTypes: {
    onClick: {
      description: "function trigger when the button is clicked",
      type: "() => void"
    },
    children: {
      description: "children component place inside the button"
    },
    variant: {
      description: "color variant",
      type: "string"
    },
    loading: {
      description: "if set to true display a spinner inside the button",
      type: "boolean"
    },
    disabled: {
      description: "if set to true disabled the button",
      type: "boolean"
    }
  },
  args: {
    children: "Button",
    loading: false
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <Button {...args} />;

const onClick = (_) => {
  console.log("you are testing the button");
};

export const Primary = Template.bind({});
Primary.args = {
  variant: "primary",
  children: "Button",
  loading: false,
  disabled: false,
  onClick
};

export const Secondary = Template.bind({});
Secondary.args = {
  variant: "secondary",
  children: "Button",
  loading: false,
  disabled: false,
  onClick
};

export const Tertiary = Template.bind({});
Tertiary.args = {
  variant: "tertiary",
  children: "Button",
  loading: false,
  disabled: false,
  onClick
};

export const Danger = Template.bind({});
Danger.args = {
  variant: "danger",
  children: "Button",
  loading: false,
  disabled: false,
  onClick
};

export const Login = Template.bind({});
Login.args = {
  variant: "login",
  loading: false,
  disabled: false,
  onClick
};
