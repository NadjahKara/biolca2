import React from "react";
import styled from "styled-components";
import { Loading } from "../components/uikit/feedback";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Loading"
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Container = styled.div`
  height: 104px;
  width: 104px;
`;

const FSContainer = styled.div`
  height: 100vh;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: center;
  background: var(--neutral-400);
`;

export const Default = () => {
  return (
    <Container>
      <Loading />
    </Container>
  );
};

export const White = () => {
  return (
    <FSContainer>
      <Container>
        <Loading white={true} />
      </Container>
    </FSContainer>
  );
};
