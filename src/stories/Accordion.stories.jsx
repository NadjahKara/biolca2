import React from "react";

import { Accordion } from "../components/uikit/surfaces";
import { Text } from "../components/uikit/typography";
import styled from "styled-components";

export default {
  title: "UI-KIT/Accordion",
  argTypes: {
    label: {
      type: "string",
      description: "Le texte affiché en haut de l'accordeon"
    }
  }
};

const Template = (args) => (
  <Accordion {...args}>
    <Text variant="title">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris hendrerit
      ultrices bibendum. Curabitur ullamcorper sagittis nulla, interdum gravida
      metus mollis at. Vestibulum eget dignissim mauris. Sed porttitor felis eu
      velit mollis, nec fermentum erat fermentum. Nam viverra elit quis dolor
      condimentum ornare. Nulla eleifend eget leo vel consectetur. Sed quis
      fermentum nulla, sit amet cursus velit.
    </Text>
  </Accordion>
);

export const Default = Template.bind({});
Default.args = {
  label: "Accordion Example"
};

const MultipleAccordionsComponent = ({ label1, label2 }) => {
  const Container = styled("div")`
    position: absolute;
    height: 200px;
    width: 200px;
    transform: translate(-50%, -50%);
    width: 280px;
  `;

  return (
    <Container>
      <Accordion label={label1}>
        <Text variant="title">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris
          hendrerit ultrices bibendum. Curabitur ullamcorper sagittis nulla,
          interdum gravida metus mollis at.
        </Text>
      </Accordion>
      <Accordion label={label2}>
        <Text variant="title">
          Vestibulum eget dignissim mauris. Sed porttitor felis eu velit mollis,
          nec fermentum erat fermentum. Nam viverra elit quis dolor condimentum
          ornare. Nulla eleifend eget leo vel consectetur. Sed quis fermentum
          nulla, sit amet cursus velit.
        </Text>
      </Accordion>
    </Container>
  );
};

const MultipleAccordionsTemplate = (args) => (
  <MultipleAccordionsComponent {...args} />
);

export const MultipleAccordions = MultipleAccordionsTemplate.bind({});
MultipleAccordions.args = {
  label1: "Accordion 1",
  label2: "Accordion 2"
};
