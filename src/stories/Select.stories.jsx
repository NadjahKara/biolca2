import React from "react";

import { Select, SelectItem } from "../components/uikit/inputs";

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "UI-KIT/Select",
  argTypes: {
    placeholder: {
      type: "string",
      description:
        "Le placeholder affiché lorsqu'aucune option n'est séléctionné"
    },
    defaultValue: {
      type: "string",
      description:
        "La valeur séléctionné par défaut lors du premier render du composant"
    }
  },
  args: {
    placeholder: "Placeholder example",
    defaultValue: ""
  }
};

const TemplateDefault = (args) => (
  <Select {...args}>
    <SelectItem label="Example 1" value="option1" />
    <SelectItem label="Example 2" value="option2" />
    <SelectItem label="Very very long example" value="option3" />
  </Select>
);

const TemplateScrollable = (args) => (
  <Select {...args}>
    <SelectItem label="Example 1" value="option1" />
    <SelectItem label="Example 2" value="option2" />
    <SelectItem label="Example 3" value="option3" />
    <SelectItem label="Example 4" value="option4" />
    <SelectItem label="Example 5" value="option5" />
    <SelectItem label="Example 6" value="option6" />
    <SelectItem label="Example 7" value="option7" />
    <SelectItem label="Example 8" value="option8" />
    <SelectItem label="Example 9" value="option9" />
    <SelectItem label="Example 10" value="option10" />
  </Select>
);

export const Default = TemplateDefault.bind({});
export const MoreThan7Options = TemplateScrollable.bind({});

//Exemple de code fonctionnel avec React-hook-form :

// export const HookFormExample = () => {
//   const { register, handleSubmit, setValue } = useForm();
//
//   const onSubmit = (data) => console.log(data);
//
//   return (
//     <>
//       <Select
//         placeholder="Placeholder"
//         register={register}
//         name="testi"
//         setValue={setValue}>
//         <SelectItem label="Example 1" value="option1" />
//         <SelectItem label="Example 2" value="option2" />
//         <SelectItem label="Example 3" value="option3" />
//       </Select>
//       <button onClick={handleSubmit(onSubmit)}>console.log</button>
//     </>
//   );
// };
