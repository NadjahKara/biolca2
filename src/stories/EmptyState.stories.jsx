import React from "react";

import { EmptyState } from "../components/uikit/feedback";

export default {
  title: "UI-KIT/EmptyState",
  argTypes: {
    size: {
      description:
        "thz size of the displayed icon, can either be in px, rem, vh/vw or any other metric",
      type: "string"
    },
    text: {
      description: "The text to display when no data is displayed",
      type: "string"
    }
  },
  args: {
    text: "Aucune données trouvées",
    size: "2.5rem"
  }
};

const Template = (args) => (
  <div className="h-screen w-screen">
    <EmptyState {...args} />
  </div>
);

export const Default = Template.bind({});
