import React from "react";

import { Snackbar } from "../components/uikit/feedback";
import { Button } from "../components/uikit/inputs";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export default {
  title: "UI-KIT/Snackbar"
};

const Template = (args) => {
  const notify = () => toast(`${args.message}`);
  return (
    <div className="h-screen w-screen">
      <Button onClick={notify}>Click me !</Button>
      <Snackbar {...args} />
    </div>
  );
};

export const Default = Template.bind({});
Default.args = {
  message: "this is an error"
};
