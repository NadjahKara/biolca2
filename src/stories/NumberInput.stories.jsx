import React from "react";

import { NumberInput } from "../components/uikit/inputs";
import { BiSearch } from "react-icons/bi";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/NumberInput",
  argTypes: {
    min: {
      description: "Min value",
      control: {
        type: "number"
      }
    },
    max: {
      description: "Max value",
      control: {
        type: "number"
      }
    }
  },
  args: {
    placeholder: 0,
    defaultValue: "",
    label: "",
    error: "",
    underText: ""
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <NumberInput {...args} />;

export const Default = Template.bind({});
export const Search = Template.bind({});
Search.args = {
  icon: <BiSearch />
};
