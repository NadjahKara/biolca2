import React from "react";
import styled from "styled-components";
import { Table } from "../components/uikit/dataDisplay";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export

const data = [
  {
    id: "test",
    c1: "Example Text",
    c2: "Example Text",
    c3: 100,
    c4: "Example Text"
  },
  {
    id: "c2",
    c1: "Example Text",
    c2: "Example Text",
    c3: 100,
    c4: "Example Text"
  },
  {
    id: "c3",
    c1: "Example Text",
    c2: "Example Text",
    c3: 100,
    c4: "Example Text"
  },
  {
    id: "c4",
    c1: "Example Text",
    c2: "Example Text",
    c3: 100,
    c4: "Example Text"
  }
];

const columns = [
  {
    title: "Title",
    dataIndex: "c1"
  },
  {
    title: "Title",
    dataIndex: "c2"
  },
  {
    title: "Title",
    dataIndex: "c3"
  },
  {
    title: "Title",
    dataIndex: "c4"
  }
];

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "UI-KIT/Table",
  argsType: {
    dataSource: {
      description: "data array display inside table rows",
      type: "any[]"
    },
    columns: {
      description:
        "array of the table columns containing the title of the column and the accessor of this column named dataIndex",
      type: "{title: string, dataIndex: string, fixed?: boolean}[]"
    },
    rowSelection: {
      selectedRowKeys: {
        description:
          "array that contain the selected rows id (must be a state)",
        type: "string[]"
      },
      onChange: {
        description:
          "function trigger when a row is selected (must update the rowSelection state)",
        type: "(e: any) => void"
      }
    }
  },
  args: {
    dataSource: [...data],
    columns
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Container = styled.div`
  height: 100vh;
  width: 100vw;
  padding: 2rem 1rem;
`;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => {
  return (
    <Container>
      <Table {...args} />
    </Container>
  );
};

export const Default = Template.bind({});
export const Checkable = Template.bind({});
Checkable.args = {
  rowSelection: {
    selectedRowKeys: ["test", "c4"],
    onChange: (newArray) =>
      alert(
        "here you should update selectedRowKey array with the new array inject in the parameter of this function"
      )
  }
};

export const Overflow = Template.bind({});
Overflow.args = {
  dataSource: [...data, ...data, ...data]
};

export const SubColumns = Template.bind({});
SubColumns.args = {
  columns: [
    ...columns,
    {
      title: "Parent",
      childrens: [
        {
          title: "child 1",
          dataIndex: "p1"
        },
        {
          title: "child 2",
          dataIndex: "p2"
        },
        {
          title: "child 3",
          dataIndex: "p3"
        }
      ]
    }
  ]
};

export const FixedColumn = Template.bind({});
FixedColumn.args = {
  columns: [
    {
      title: "Fixed",
      dataIndex: "f1",
      fixed: true
    },
    ...columns,
    {
      title: "Parent",
      childrens: [
        {
          title: "child 1",
          dataIndex: "p1"
        },
        {
          title: "child 2",
          dataIndex: "p2"
        },
        {
          title: "child 3",
          dataIndex: "p3"
        }
      ]
    }
  ],
  dataSource: [
    {
      f1: "1",
      c1: "Example Text",
      c2: "Example Text",
      c3: 100,
      c4: "Example Text",
      p1: "child 1 value",
      p2: "child 2 value",
      p3: "child 3 value"
    },
    {
      f1: "1",
      c1: "Example Text",
      c2: "Example Text",
      c3: 100,
      c4: "Example Text",
      p1: "child 1 value",
      p2: "child 2 value",
      p3: "child 3 value"
    }
  ]
};

export const Loading = Template.bind({});
Loading.args = {
  loading: true
};

export const Empty = Template.bind({});
Empty.args = {
  dataSource: []
};

// @TODO add an empty state inside of the table
