import React from "react";

import { ListElement } from "../components/uikit/dataDisplay";
import { Button } from "../components/uikit/inputs";

export default {
  title: "UI-KIT/ListElement",
  argTypes: {
    title: {
      description: "Title of the element display in bold typo",
      type: "string"
    },
    description: {
      description: "description of the list display below the title",
      type: "string"
    },
    children: {
      description:
        "React element passed inside the list Body display on the right of the element",
      type: "react.element"
    }
  },
  args: {
    title: "A great Title",
    description: "damn look at this description",
    children: []
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const TemplateDefault = (args) => <ListElement {...args} />;

export const Default = TemplateDefault.bind({});

export const WithChildrenButton = TemplateDefault.bind({});
WithChildrenButton.args = {
  children: [<Button>Click me</Button>]
};
