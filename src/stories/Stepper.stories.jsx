import React from "react";

import { Stepper } from "../components/uikit/feedback";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Stepper",
  argTypes: {}
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => <Stepper {...args} />;

export const Default = (args) => {
  return (
    <div className="w-screen">
      <Template {...args} />
    </div>
  );
};

Default.args = {
  steps: [1, 2, 3],
  current: 0
};
