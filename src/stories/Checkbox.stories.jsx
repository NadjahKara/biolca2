import React from "react";

import { Checkbox } from "../components/uikit/inputs";
import { useArgs } from "@storybook/client-api";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "UI-KIT/Checkbox",
  argTypes: {
    checked: {
      description: "Value of the checkbox (must be a state)",
      type: "boolean"
    },
    onCheck: {
      description: "function trigger when checkbox is clicked",
      type: "(e) => void"
    }
  },
  args: {
    checked: true,
    onCheck: () => alert("here you should update the checked state")
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => {
  const [, setArgs] = useArgs();

  const handleIsOpen = (value) => {
    setArgs({ ...args, checked: value });
  };

  return <Checkbox {...args} onCheck={handleIsOpen} />;
};

export const Default = Template.bind({});
