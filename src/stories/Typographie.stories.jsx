import React from "react";
import styled from "styled-components";
import { Text } from "../components/uikit/typography";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Typography",
  argTypes: {
    children: {
      description:
        "React element passed inside the typography (must be plain text)",
      type: "react.element"
    },
    variant: {
      description: "variant string to change the type of text",
      control: {
        type: "select",
        options: [
          "PageTitle",
          "PageSubTitle",
          "Heading",
          "Subheading",
          "Title",
          "Body",
          "Caption"
        ]
      }
    }
  },
  args: {
    variant: "PageTitle",
    children: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ac t"
    ]
  }
};

const Container = styled("div")`
  height: 100vh;
  width: 100vw;
  background: ${(props) => (props?.inverted ? "var(--text)" : "#FFF")};
`;

const TemplateDefault = (args) => {
  return <Text variant={args.variant}>{args.children}</Text>;
};

export const PageTitleVariant = TemplateDefault.bind({});
