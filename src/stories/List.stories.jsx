import React from "react";

import { List } from "../components/uikit/dataDisplay";

export default {
  title: "UI-KIT/ListElement"
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const TemplateDefault = (args) => <List {...args} />;

export const Default = TemplateDefault.bind({});
Default.args = {
  title: "Exemple de titre"
};
