import React from "react";

import { Skeleton } from "../components/uikit/feedback";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  title: "UI-KIT/Skeleton",
  argTypes: {
    variant: {
      description: "La variante du skeleton à montrer",
      control: {
        type: "select",
        options: ["text", "button", "block", "input"]
      },
      defaultValue: "text"
    },
    lines: {
      description:
        "Le nombre de lignes à montrer (valide que pour les variantes text et block)",
      type: "number",
      defaultValue: 1,
      control: {
        type: "number"
      }
    }
  }
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

const Template = (args) => {
  return (
    <div className="w-80">
      <Skeleton {...args} />
    </div>
  );
};

export const Default = Template.bind({});
