import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

import { Header } from "../components/uikit/surfaces";
// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
export default {
  title: "UI-KIT/Header"
};

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args

export const Default = (args) => {
  return (
    <Router>
      <Header {...args} />
    </Router>
  );
};

export const TitleView = (args) => {
  return (
    <Router>
      <Header {...args} />
    </Router>
  );
};
TitleView.args = {
  title: "Nom du rapport"
};

export const RoutesView = (args) => {
  return (
    <Router>
      <Header {...args} />
    </Router>
  );
};
RoutesView.args = {
  routes: [
    {
      title: "Liste des rapports",
      route: "/reports"
    },
    {
      title: "Liste des objets métier personnalisés",
      route: "/objects"
    }
  ]
};
