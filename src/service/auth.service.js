import { loginAuth } from "../api";

export const auth = async (code) => {
  try {
    const response = await loginAuth(code);
    localStorage.setItem("access_token", response.data.access_token);
    localStorage.setItem("refresh_token", response.data.refresh_token);
  } catch (error) {
    console.error(error);
  }
};

export const disconnect = () => {
  localStorage.removeItem("access_token");
  localStorage.removeItem("refresh_token");
};
