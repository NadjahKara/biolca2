# How to

Create a file that regroups all the related logic for a specific entity / m.s,
ex : `example.service.js` that would deal with an "example" entity; it's can be
used as either a mapper (as some do in the backend) or just a file for logic
related functions / mapping .

<p>
The exported function would return / throw any custom object  :
</p>
```js
export const fetchExample = async () => {
  try {
    const { data } = await getExample();
    /*
     * The rest of the logic
     */
  } catch (error) {
    /*
     * error logic
     */
  }
};
```
---

you can import any other function, either an api call or another function
defined in another service
