import {
  getAllRBO,
  getRBOById,
  createRBOModel,
  updateRBOModel,
  deleteRBOModel
} from "../api";

export const fetchAllRBO = async () => {
  try {
    const { data } = await getAllRBO();

    return data;
  } catch (error) {
    console.log("rbo.service : getAllRBO", error);
  }
};

export const fetchRBO = async (id) => {
  try {
    const { data } = await getRBOById(id);

    return data;
  } catch (error) {
    console.log("rbo.service : fetchRBO", error);
  }
};

export const createRBO = async (rbo) => {
  try {
    const { data } = await createRBOModel(rbo);

    return data;
  } catch (error) {
    console.log("rbo.service : createRBO", error);
  }
};

export const updateRBO = async (id, rbo) => {
  try {
    const { data } = await updateRBOModel(id, rbo);

    return data;
  } catch (error) {
    console.log("rbo.service : updateRBO", error);
  }
};

export const deleteRBO = async (id) => {
  try {
    const { data } = await deleteRBOModel(id);

    return data;
  } catch (error) {
    console.log("rbo.service : deleteRBO", error);
  }
};
