import { auth } from "../auth.service";
import { loginAuth } from "../../api/auth.instance";

jest.mock("../../api/auth.instance", () => ({
  loginAuth: jest.fn()
}));

jest.mock("js-cookie", () => ({
  set: jest.fn()
}));

global.console = { error: jest.fn() };

jest.spyOn(window.localStorage.__proto__, "setItem");
window.localStorage.__proto__.setItem = jest.fn();

describe("Authentication", () => {
  it("Should have been called with code", async () => {
    // Given
    const mockResponse = {
      data: {
        access_token: "token",
        refresh_token: "refresh",
        expires_in: 54464
      }
    };
    const mockedCode = "455";
    loginAuth.mockResolvedValue(mockResponse);

    // When
    await auth(mockedCode);

    // Then
    expect(loginAuth).toHaveBeenCalled();
    expect(loginAuth).toHaveBeenCalledWith(mockedCode);
  });

  it("should set access_token in localStorage successfully", async () => {
    // Given
    const mockResponse = {
      data: {
        access_token: "token",
        refresh_token: "refresh",
        expires_in: 54464
      }
    };
    const mockedCode = "455";

    loginAuth.mockResolvedValue(mockResponse);

    // When
    await auth(mockedCode);

    // Then
    expect(localStorage.setItem).toHaveBeenCalled();
    expect(localStorage.setItem).toHaveBeenCalledWith(
      "access_token",
      mockResponse.data.access_token
    );
  });

  it("should set refresh_token in localStorage successfully", async () => {
    // Given
    const mockResponse = {
      data: {
        access_token: "token",
        refresh_token: "refresh",
        expires_in: 54464
      }
    };
    const mockedCode = "455";

    loginAuth.mockResolvedValue(mockResponse);

    // When
    await auth(mockedCode);

    // Then
    expect(localStorage.setItem).toHaveBeenCalled();
    expect(localStorage.setItem).toHaveBeenCalledWith(
      "refresh_token",
      mockResponse.data.refresh_token
    );
  });

  it("Should not set localStorage successfully because of resfresh_token missing", async () => {
    // Given
    const mockResponse = {
      data: {
        access_token: "token",
        expires_in: 54464
      }
    };
    const mockedCode = "455";

    loginAuth.mockResolvedValue(mockResponse);

    // When
    await auth(mockedCode);

    // Then
    expect(localStorage.setItem).toHaveBeenCalled();
    expect(mockResponse.data).not.toHaveProperty("refresh_token");
  });

  it("it should console error because of wrong response", async () => {
    // Given
    const mockResponse = {
      data: {
        access_token: "token",
        refresh_token: "refresh",
        expires_in: 54464
      }
    };

    const mockedCode = "455";
    loginAuth.mockRejectedValue(mockResponse);

    // When
    await auth(mockedCode);

    // Then
    expect(console.error).toHaveBeenCalled();
  });

  it("it should console error because of wrong code", async () => {
    // Given
    const mockedCode = jest.fn().mockRejectedValue();

    // When
    await auth(mockedCode);

    // Then
    expect(console.error).toHaveBeenCalled();
  });
});
