import { getExample } from "../api";

export const fetchExample = async () => {
  try {
    const { data } = await getExample();
    /*
     * The rest of the logic
     */
  } catch (error) {
    /*
     * error logic
     */
  }
};

export const fetchWithMultipleReturn = async () => {
  try {
    const { data } = await getExample();
    /*
     * The rest of the logic
     */
    const a = 0; // a value to return
    const b = 0; // another value to return

    return [a, b];

    // if you want to get the values, call it as so :
    // const [a, b] = await fetchWithMultipleReturn()
  } catch (error) {
    /*
     * error logic
     */
  }
};
