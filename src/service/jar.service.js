import { createJar, deleteJar, getTechModels } from "../api";

export const newJar = async (jar) => {
  try {
    const { data } = await createJar(jar);

    return data;
  } catch (error) {
    console.log("jar.service : newJar", error);
  }
};

export const delJar = async (id) => {
  try {
    const { data } = await deleteJar(id);

    return data;
  } catch (error) {
    console.log("jar.service : delJar", error);
  }
};

export const fetchAllTechModels = async (id) => {
  try {
    const { data } = await getTechModels(id);

    return data;
  } catch (error) {
    console.log("jar.service : fetchAllTechModels", error);
  }
};
