import { getReportById, createReport, getAllReport, getReportByTitle } from "../api/report.instance.js";

export const fetchReportById = async () => {
  try {
    const { data } = await getReportById();
    return data;
  } catch (error) {
    console.error(error);
  }
};

export const addReport = async (report) => {
  try {
    const { data } = await createReport(report);
    return data;
  } catch (error) {
    console.error(error);
  }
};


export const getReport = async (offset,limit) => {
  try {
    const { data } = await getAllReport(offset,limit);    
    return data;
  } catch (error) {
    console.error(error);
  }
};

export const getFiltredReport = async (offset, limit, title) => {
  try {
    const { data } = await getReportByTitle(offset, limit, title);    
    return data;
  } catch (error) {
    console.error(error);
  }
};
