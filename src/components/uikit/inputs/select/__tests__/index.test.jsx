import { render, screen, fireEvent } from "@testing-library/react";
import { Select, SelectItem } from "..";

describe("Select", () => {
  it("should render", () => {
    render(<Select></Select>);

    expect(screen.getByTestId("select-test")).toBeInTheDocument();
  });

  it("should have a placeholder and a hidden placeholder", () => {
    render(<Select placeholder="placeholder-test"></Select>);

    let placeholderArray = screen.getAllByText("placeholder-test");

    expect(placeholderArray.length).toBe(2);
  });

  it("should have a default value", () => {
    render(<Select defaultValue="test-value"></Select>);
    expect(screen.getByTestId("select-hidden-input").value).toBe("test-value");
  });

  it("should be closed by default", () => {
    render(<Select></Select>);
    expect(screen.getByTestId("select-options")).toHaveStyle("height: 0px;");
  });

  it("should open on click", () => {
    render(<Select></Select>);

    fireEvent(
      screen.getByTestId("select-test"),
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true
      })
    );

    expect(screen.getByTestId("select-options")).toHaveAttribute("open");

    //exactement 9 pixels car le Select ne possède aucune option.
    expect(screen.getByTestId("select-options")).toHaveStyle("height: 9px;");
  });
});

describe("SelectItem", () => {
  it("should render & have a label", () => {
    render(<SelectItem label="item-test" />);

    expect(screen.getByText("item-test")).toBeInTheDocument();
  });
});
