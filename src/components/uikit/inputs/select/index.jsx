import React, { useState } from "react";
import styled from "styled-components";
import { Text } from "../../typography";

/* ================================================================= */
/*                              STYLE                                */
/* ================================================================= */

const Bg = styled("div")`
  user-select: none;
  position: relative;
  display: flex;
  cursor: pointer;
  justify-content: space-between;
  transition: border 150ms ease-in-out;
  height: 35px;
  width: 100%;
  box-sizing: border-box;
  padding: 6px 38px 6px 12px;
  background-color: var(--neutral-100);
  border-radius: 4px;
  border: solid 1px var(--neutral-600);

  outline: none !important;

  ${({ open }) =>
    !open && "&:focus { outline: none; border: solid 2px var(--primary-300);"}
`;

const SelectIcon = styled("svg")`
  position: absolute;
  top: 14px;
  right: 14px;
`;

const Selected = styled("div")`
  display: flex;
  flex-direction: column;
`;

const HiddenOptions = styled("div")`
  height: 0px;
  overflow: hidden;
`;

const SelectDivider = styled("div")`
  position: absolute;
  bottom: -1px;
  left: -1px;
  height: 4px;
  width: calc(100% + 2px);
  background-color: var(--neutral-100);
  z-index: 1001;

  border-left: 1px solid var(--neutral-600);
  border-right: 1px solid var(--neutral-600);

  transition: border 0.065s ease-in-out;

  ${({ visible }) => !visible && "display: none;"}

  &:before {
    content: "";
    position: absolute;
    bottom: 0;
    height: 1px;
    left: 12px;
    width: calc(100% - 24px);
    background-color: var(--neutral-600);
  }
`;

const Options = styled("div")`
  position: absolute;
  top: 100%;
  left: -1px;
  height: 0px;
  width: calc(100% + 2px);
  background-color: var(--neutral-100);
  border-radius: 0 0 4px 4px;
  z-index: 1000;

  overflow: hidden;

  outline: none !important;

  transition: height 0.065s ease-in-out, border 0.065s ease-in-out,
    padding 0.065s ease-in-out;

  max-height: 219px;

  ${({ open, nbItem }) =>
    open
      ? "padding: 4px 0;" +
        "height: " +
        (9 + nbItem * 30) +
        "px;" +
        "overflow: auto;" +
        "border-left: solid 1px var(--neutral-600);" +
        "border-right: solid 1px var(--neutral-600);" +
        "border-bottom: solid 1px var(--neutral-600);"
      : "> * { display: none !important; }"};
`;

const Option = styled("div")`
  width: 100%;
  height: 30px;
  padding: 5px 12px;
  display: flex;
  align-items: center;

  white-space: nowrap;

  transition: background-color 0.065s ease-in-out;

  outline: none !important;

  background-color: var(--neutral-100);

  &:hover,
  &:focus,
  &:active {
    background-color: var(--neutral-200);
  }
`;

const SpecialText = styled(Text)`
  transition: none;
  ${({ ghost }) => ghost && "color: var(--text-ghost);"}
`;

/* ================================================================= */
/*                            COMPONENTS                             */
/* ================================================================= */

/**
 * Composant pour afficher une option à l'intérieur d'un Select
 * @component
 * @param {string} value La valeur de l'item
 * @param {string} label Le texte affiché à l'utilisateur
 * @param {function} changeFunction La fonction appelée lorsque l'option est séléctionnée (Si le composant parent est un Select, cette fonction sera automatiquement attribuée)
 * @param {boolean} selectable Si le composant doit être accessible grâce à la navigation au clavier
 *
 */
function SelectItem({
  value,
  label,
  changeFunction = () => null,
  selectable = true
}) {
  function handleKeyDown(e) {
    if (e.code === "Space" || e.code === "Enter") {
      changeFunction(value);
    }
  }

  return (
    <Option
      value={value}
      onClick={() => changeFunction(value)}
      onKeyDown={handleKeyDown}
      tabIndex={selectable ? "0" : undefined}
    >
      <Text variant="Title">{label}</Text>
    </Option>
  );
}

/**
 * Composant qui agit comme un input Select custom
 * @component
 * @param {string} placeholder Le texte qui doit être affiché lorsqu'aucune value - ou une value incorrecte - est sélectionnée
 * @param {string} defaultValue La valeur du Select la première fois qu'il est render
 * @param {Object[]} children Une liste des éléments à l'intérieur du Select (devrait être une liste de SelectItem)
 * @param {function} register La fonction register de react-hook-form afin de gérer la valeur du composant via cette bibliothèque
 * @param {string} name Le nom de l'input afin d'accéder à sa valeur via react-hook-form
 * @param {function} setValue La function setValue de react-hook-form afin de gérer la valeur du Select via cette bibliothèque
 * @param {boolean} required Si le composant doit avoir une value avant de submit le formulaire dont il fait partie
 * @param {function} onChange Une fonction appelée à chaque fois que la valeur du composant change. Son premier paramètre est la nouvelle valeur du composant.
 *
 */
function Select({
  placeholder = "",
  defaultValue = "",
  children = [],
  register = (_, rules = {}) => {},
  name = "",
  shouldUnregister = false,
  setValue = () => null,
  required = false,
  onChange = () => null
}) {
  const [open, setOpen] = useState(false);
  const [value, setValueState] = useState(defaultValue);

  let label = placeholder;
  let hasSelected = false;

  //Vérifie, pour tous les SelectItem de children, si l'un d'entre eux possède la valeur séléctionnée
  //afin d'afficher le label du SelectItem comme label du Select
  for (let child of children) {
    if (
      typeof child === "object" &&
      child.type.name === "SelectItem" &&
      child.props.value === value
    ) {
      label = child.props.label;
      hasSelected = true;
    }
  }

  function handleKeyDown(e) {
    if (e.code === "Space" || e.code === "Enter") setOpen(!open);
  }

  function changeValue(newValue) {
    setValueState(newValue);
    setValue(name, newValue);
    onChange(newValue);
  }

  return (
    <Bg
      data-testid="select-test"
      onClick={() => {
        setOpen(!open);
      }}
      onKeyDown={handleKeyDown}
      tabIndex="0"
      open={open}
    >
      <Selected>
        <HiddenOptions>
          {children.map((child, i) => (
            <SelectItem
              key={i}
              value={child.props.value}
              label={child.props.label}
              selectable={false}
            />
          ))}
          {<Text variant="Title">{placeholder}</Text>}
        </HiddenOptions>
        <SpecialText variant="Title" ghost={!hasSelected}>
          {label}
        </SpecialText>
      </Selected>
      <SelectIcon
        width="12"
        height="8"
        viewBox="0 0 12 8"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M10.5134 0L6 4.5134L1.48659 0L0 1.48659L6 7.48659L12 1.48659L10.5134 0Z"
          fill="var(--text)"
        />
      </SelectIcon>
      <SelectDivider visible={open} />
      <Options
        open={open}
        nbItem={children.length}
        tabIndex="-1"
        data-testid="select-options"
      >
        {children.map((child, i) => (
          <SelectItem
            key={i}
            value={child.props.value}
            label={child.props.label}
            changeFunction={(newValue) => {
              changeValue(newValue);
            }}
          />
        ))}
      </Options>

      <input
        type="hidden"
        value={value}
        {...register(name, {
          shouldUnregister,
          required
        })}
        data-testid="select-hidden-input"
      />
    </Bg>
  );
}

export { Select, SelectItem };
