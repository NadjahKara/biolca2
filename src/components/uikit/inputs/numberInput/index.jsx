import React from "react";
import { Text } from "../../typography";
import { StyledNumberError, StyledNumberInput } from "./styles";

/**
 * InputText
 * @description
 * @param {object} props InputText props.
 * @param {string} props.label text of the input label.
 * @param {string} props.type type of the input (text - password ...).
 * @param {string} props.error input error text
 * @param {string} props.name name of the input
 * @param {boolean} props.required field required
 * @param {JSX} props.register useForm register
 * @param {string} props.underText text under input field
 * @param {React.ReactNode} props.icon icon display at the left in the number input
 * @returns JSX.Element
 * @author
 */
const NumberInput = (props) => {
  const {
    label = "",
    underText = "",
    error = "",
    name = "",
    register = (_, rules = {}) => {},
    required = false,
    pattern,
    minLength,
    maxLength,
    defaultValue,
    placeholder,
    shouldUnregister = false,
    icon,
    min,
    max
  } = props;
  return (
    <div className={`flex flex-col my-2`}>
      <Text
        variant="Body"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {label}
        {error === "" ? (
          ""
        ) : (
          <StyledNumberError>{" - " + error}</StyledNumberError>
        )}
      </Text>
      <div className="relative flex items-center">
        {icon && (
          <div
            className="absolute left-0 select-none outline-none p-2 h-11 flex items-center justify-center"
            data-testid="icon">
            {icon}
          </div>
        )}
        <StyledNumberInput
          name={name}
          type="number"
          min={min}
          max={max}
          defaultValue={defaultValue}
          placeholder={placeholder}
          {...register(name, {
            shouldUnregister,
            required,
            pattern: pattern ?? {},
            minLength: minLength ?? null,
            maxLength: maxLength ?? null
            // validate: validate,
          })}
          error={error !== ""}
          className={`${icon ? "pl-7 pr-2" : "px-2"}`}
          icon={icon}
        />
      </div>
      <Text
        variant="Caption"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {underText}
      </Text>
    </div>
  );
};

export default NumberInput;
