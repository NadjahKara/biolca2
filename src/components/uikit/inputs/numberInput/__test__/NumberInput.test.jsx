import { render, screen } from "@testing-library/react";
import NumberInput from "..";
import { BiSearch } from "react-icons/bi";

describe("NumberInput", () => {
  it("should display number input label", () => {
    // given
    // when
    render(<NumberInput label="test label" />);
    const labelElement = screen.getByText(/test label/i);
    // then
    expect(labelElement).toBeInTheDocument();
  });

  it("should display number input undertext", () => {
    // given
    // when
    render(<NumberInput label="test label" underText="undertext" />);
    const undertextElement = screen.getByText(/undertext/i);
    // then
    expect(undertextElement).toBeInTheDocument();
  });

  it("should display number input error", () => {
    // given
    // when
    render(
      <NumberInput label="test label" underText="undertext" error="error" />
    );
    const errorElement = screen.getByText(/error/i);
    // then
    expect(errorElement).toBeInTheDocument();
  });

  it("should display input placeholder", () => {
    // given
    // when
    render(
      <NumberInput
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
      />
    );
    const inputElement = screen.getByPlaceholderText("number");
    // then
    expect(inputElement).toBeInTheDocument();
  });

  it("should have fixed min value", () => {
    // given
    // when
    render(
      <NumberInput
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
        min={20}
      />
    );
    const inputElement = screen.getByPlaceholderText("number");
    // then
    expect(inputElement).toHaveAttribute("min", "20");
  });

  it("should have fixed max value", () => {
    // given
    // when
    render(
      <NumberInput
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
        max={25}
      />
    );
    const inputElement = screen.getByPlaceholderText("number");
    // then
    expect(inputElement).toHaveAttribute("max", "25");
  });
  it("should display icon", () => {
    // given
    // when
    render(
      <NumberInput
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
        max={25}
        icon={<BiSearch />}
      />
    );
    const iconElement = screen.getByTestId("icon");
    // then
    expect(iconElement).toBeInTheDocument();
  });
});
