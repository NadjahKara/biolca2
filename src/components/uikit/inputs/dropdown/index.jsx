import React, { useState } from "react";
import styled from "styled-components";
import { Text } from "../../typography";

/* ================================================================= */
/*                              STYLE                                */
/* ================================================================= */

const Bg = styled("div")`
  user-select: none;
  position: relative;
  display: flex;
  cursor: pointer;
  justify-content: space-between;
  transition: 150ms ease-in-out;
  height: 35px;
  width: 100%;
  box-sizing: border-box;
  padding: 6px 12px;
  background-color: var(--neutral-100);
  border-radius: 4px;
  border: solid 1px var(--neutral-600);

  outline: none;

  ${({ open }) =>
    !open && "&:focus { outline: none; border: solid 2px var(--primary-300);"}
`;

const Label = styled("div")`
  pointer-events: none;
`;

const Options = styled("div")`
  position: absolute;
  top: calc(100% + 8px);
  height: 0px;
  background-color: var(--neutral-100);
  border-radius: 4px;
  overflow: hidden;
  z-index: 1000;

  transition: height 0.065s ease-in-out, border 0.065s ease-in-out,
    padding 0.065s ease-in-out;

  ${({ open, nbItem }) =>
    open
      ? "padding-top: 4px;" +
        "height: " +
        (10 + nbItem * 30) +
        "px;" +
        "border: solid 1px var(--neutral-600);"
      : "> * { visibility: hidden }"}

  ${({ overflow }) => overflow === "left" && "left: -1px;"}
  ${({ overflow }) => overflow === "right" && "right: -1px;"}
`;

const Option = styled("div")`
  width: 100%;
  height: 30px;
  padding: 5px 12px;
  display: flex;
  align-items: center;

  white-space: nowrap;

  transition: background-color 0.065s ease-in-out;

  background-color: var(--neutral-100);

  &:hover,
  &:focus,
  &:active {
    background-color: var(--neutral-200);
    outline: none;
  }
`;

/* ================================================================= */
/*                            COMPONENTS                             */
/* ================================================================= */
/**
 * Composant pour afficher une option à l'intérieur d'un Dropdown
 * @param {string} label Le texte affiché à l'utilisateur
 * @param {function} onClick La fonction appelée à chaque fois que l'option est sélectionnée
 */
function DropdownItem({ label, onClick = () => null }) {
  function handleKeyDown(e) {
    if (e.code === "Space" || e.code === "Enter") {
      onClick();
    }
  }

  return (
    <Option onClick={onClick} onKeyDown={handleKeyDown} tabIndex="0">
      <Text variant="Title">{label}</Text>
    </Option>
  );
}

/**
 * Composant qui agit comme un menu Dropdown
 * @param {string} label Le texte qui doit être affiché à l'utilisateur
 * @param {Object[]} children Une liste des éléments à l'intérieur du Dropdown (devrait être une liste de DropdownItem)
 * @param {string} overflow La direction dans laquelle les options du Dropdown devraient s'étendre si leur taille dépasse celle du Dropdown
 *
 */
function Dropdown({ label, children = [], overflow = "right" }) {
  const [open, setOpen] = useState(false);

  function handleKeyDown(e) {
    if (e.code === "Space" || e.code === "Enter") {
      setOpen(!open);
    }
  }

  function handleOpen(event) {
    setOpen(!open);
  }

  return (
    <Bg onClick={handleOpen} onKeyDown={handleKeyDown} tabIndex="0" open={open}>
      <Label>
        <Text variant="Title">{label}</Text>
      </Label>
      <Options
        open={open}
        nbItem={children.length}
        overflow={overflow}
        data-testid="options-button"
      >
        {children}
      </Options>
    </Bg>
  );
}

export { Dropdown, DropdownItem };
