import { fireEvent, render, screen } from "@testing-library/react";
import { DropdownItem, Dropdown } from "..";

const mockedOnClick = jest.fn();

describe("Dropdown", () => {
  it("should render & have a label", () => {
    render(<Dropdown label="dropdown-test" />);
    const dropElement = screen.getByText("dropdown-test");

    expect(dropElement).toBeInTheDocument();
  });

  it("should be closed by default", () => {
    render(<Dropdown label="dropdown-test" />);
    const optionsBtnElement = screen.getByTestId("options-button");

    expect(optionsBtnElement).toHaveStyle("height: 0px;");
  });

  it("should be opened on click", () => {
    render(<Dropdown label="dropdown-test" />);
    const optionsBtnElement = screen.getByTestId("options-button");

    fireEvent.click(optionsBtnElement);
    expect(optionsBtnElement).toHaveAttribute("open");
    expect(optionsBtnElement).not.toHaveStyle(`height: 0px;`);
  });
});

describe("DropdownItem", () => {
  it("should render & have a label", () => {
    render(<DropdownItem label="item-test" />);

    expect(screen.getByText("item-test")).toBeInTheDocument();
  });

  it("should trigger onClick when the option isClicked", () => {
    // given
    render(<DropdownItem label="item-test" onClick={mockedOnClick} />);
    const itemElement = screen.getByText("item-test");
    // when
    fireEvent.click(itemElement);
    // then
    expect(mockedOnClick).toHaveBeenCalledTimes(1);
  });
});
