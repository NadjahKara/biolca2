import { range } from "lodash";
import React, { useState } from "react";
import { StyledError, StyledInput } from "../textInput/styles";
import { BiCalendar } from "react-icons/bi";
import { Text } from "../../typography";
import { StyledPicker } from "./styles";
import "./calendar.css";

// this component is the same as a textinput, the only difference is that it takes a value prop, since the value is injected from the calendar
const Input = (props) => {
  const {
    onClick,
    error = "",
    label = "",
    nameRef = "",
    underText = "",
    registerRef = () => {},
    value,
    placeholder
  } = props;
  const icon = <BiCalendar data-testid="picker-icon" />;
  return (
    <div className={`flex flex-col my-2`} onClick={onClick}>
      <Text
        data-testid="picker-label"
        variant="Body"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {label}
        {error === "" ? (
          ""
        ) : (
          <StyledError data-testid="picker-error">{" - " + error}</StyledError>
        )}
      </Text>
      <div className="relative flex items-center">
        {icon && (
          <div className="absolute left-0 select-none outline-none p-3 h-11 flex items-center justify-center input-icon">
            {icon}
          </div>
        )}
        <StyledInput
          data-testid="picker-input"
          name={nameRef}
          value={value}
          placeholder={placeholder}
          {...registerRef}
          error={error !== ""}
          icon={icon}
        />
      </div>
      <Text
        data-testid="picker-undertext"
        variant="Caption"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {underText}
      </Text>
    </div>
  );
};

function DatePicker({
  label = "",
  error = "",
  name = "",
  underText = "",
  placeholder = "",
  required = false,
  defaultValue,
  shouldUnregister = false,
  setValue = () => {},
  register = () => {}
}) {
  const registerRef = register(name, {
    required,
    shouldUnregister,
    value: new Date().toISOString().split("T")[0] + "-00-00-00"
  });
  const [startDate, setStartDate] = useState(new Date());
  const years = range(1990, new Date().getFullYear() + 1, 1);
  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  React.useEffect(() => {
    if (typeof defaultValue === "number") {
      const parsed = new Date(defaultValue);
      setValue(name, parsed.toISOString().split("T")[0] + "-00-00-00");
      setStartDate(parsed);
    } else {
      setValue(name, defaultValue);
      setStartDate(defaultValue);
    }
  }, [setValue, name, defaultValue]);
  return (
    <StyledPicker
      calendarClassName="calendar"
      renderCustomHeader={({ date, changeYear, changeMonth }) => (
        <div className="calendar-header" data-testid="picker-calendar">
          <select
            value={months[date.getMonth()]}
            onChange={({ target: { value } }) =>
              changeMonth(months.indexOf(value))
            }
          >
            {months.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </select>

          <select
            value={date.getFullYear()}
            onChange={({ target: { value } }) => changeYear(value)}
          >
            {years.map((option) => (
              <option key={option} value={option}>
                {option}
              </option>
            ))}
          </select>
        </div>
      )}
      selected={startDate}
      onChange={(date) => {
        setValue(name, date.toISOString().split("T")[0] + "-00-00-00");
        setStartDate(date);
      }}
      customInput={
        <Input
          error={error}
          label={label}
          nameRef={name}
          underText={underText}
          registerRef={registerRef}
          placeholder={placeholder}
        />
      }
    />
  );
}
/**
 * A date input
 * @component
 * @param {string} label The label that would be displayed on top of the input
 * @param {string} error The error to display
 * @param {string} name The name that would be used to register the input
 * @param {string} underText The text to be displayed under the input
 * @param {string} placeholder The input's placeholder
 * @param {boolean} required Prop to indicate wheter this input is required or not
 * @param {boolean} shouldUnregister Prop to indicate wheter the input should be cleared when unmounted
 * @param {()=>void} setValue React-hook-form's setValue attribute (no additional overhead)
 * @param {()=>void} register React-hook-form's register attribute (no additional overhead)
 */
export default React.memo(DatePicker);
