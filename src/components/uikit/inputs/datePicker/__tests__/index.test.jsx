import { render, screen, fireEvent } from "@testing-library/react";
import Picker from "..";

describe("Datepicker", () => {
  it("renders calendar", () => {
    render(<Picker name="date" setValue={() => {}} register={() => {}} />);
    fireEvent(
      screen.getByTestId("picker-input"),
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true
      })
    );
    expect(screen.getByTestId("picker-calendar")).toBeInTheDocument();
  });
  it("renders input", () => {
    render(<Picker name="date" setValue={() => {}} register={() => {}} />);
    expect(screen.getByTestId("picker-input")).toBeInTheDocument();
  });
  it("renders icon", () => {
    render(<Picker name="date" setValue={() => {}} register={() => {}} />);
    expect(screen.getByTestId("picker-icon")).toBeInTheDocument();
  });
  it("renders label", () => {
    render(
      <Picker
        name="date"
        setValue={() => {}}
        register={() => {}}
        label="This is a test"
      />
    );
    expect(screen.getByTestId("picker-label")).toBeInTheDocument();
    expect(screen.getByText("This is a test")).toBeInTheDocument();
  });
  it("renders error", () => {
    render(
      <Picker
        name="date"
        setValue={() => {}}
        register={() => {}}
        error="This is a test"
      />
    );
    expect(screen.getByTestId("picker-error")).toBeInTheDocument();
  });
  it("renders undertext", () => {
    render(
      <Picker
        name="date"
        setValue={() => {}}
        register={() => {}}
        underText="This is a test"
      />
    );
    expect(screen.getByTestId("picker-undertext")).toBeInTheDocument();
    expect(screen.getByText("This is a test")).toBeInTheDocument();
  });
  it("renders all props", () => {
    render(
      <Picker
        name="date"
        setValue={() => {}}
        register={() => {}}
        label="This is a label"
        error="This is an error"
        underText="This is an undertext"
      />
    );
    const input = screen.getByTestId("picker-input");
    fireEvent(
      input,
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true
      })
    );
    expect(screen.getByTestId("picker-calendar")).toBeInTheDocument();
    expect(input).toBeInTheDocument();
    expect(screen.getByTestId("picker-icon")).toBeInTheDocument();
    expect(screen.getByTestId("picker-label")).toBeInTheDocument();
    expect(screen.getByTestId("picker-error")).toBeInTheDocument();
    expect(screen.getByTestId("picker-undertext")).toBeInTheDocument();
  });
});
