import { render, screen } from "@testing-library/react";
import TextInput from "..";
import { BiSearch } from "react-icons/bi";

describe("TextInput", () => {
  it("should display number input label", () => {
    // given
    // when
    render(<TextInput label="test label" />);
    const labelElement = screen.getByText(/test label/i);
    // then
    expect(labelElement).toBeInTheDocument();
  });

  it("should display number input undertext", () => {
    // given
    // when
    render(<TextInput label="test label" underText="undertext" />);
    const undertextElement = screen.getByText(/undertext/i);
    // then
    expect(undertextElement).toBeInTheDocument();
  });

  it("should display number input error", () => {
    // given
    // when
    render(
      <TextInput label="test label" underText="undertext" error="error" />
    );
    const errorElement = screen.getByText(/error/i);
    // then
    expect(errorElement).toBeInTheDocument();
  });

  it("should display input placeholder", () => {
    // given
    // when
    render(
      <TextInput
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
      />
    );
    const inputElement = screen.getByPlaceholderText("number");
    // then
    expect(inputElement).toBeInTheDocument();
  });

  it("should display the icon inside input", () => {
    // given
    // when
    render(
      <TextInput
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
        icon={<BiSearch />}
      />
    );
    const inputElement = screen.getByPlaceholderText("number");
    // then
    expect(inputElement).toBeInTheDocument();
  });
});
