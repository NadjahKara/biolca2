import React from "react";
import { Text } from "../../typography";
import { StyledError, StyledInput } from "./styles";

/**
 * InputText
 * @description
 * @param {object} props InputText props.
 * @param {string} props.label texte label affiché au dessus de l'input
 * @param {string} props.type type de l'input (text - password ...).
 * @param {string} props.error text d'erreur de l'input
 * @param {string} props.name nom de l'input
 * @param {boolean} props.required si vrai l'input est indiquer comme requis
 * @param {JSX} props.register useForm register
 * @param {string} props.underText texte affiché sous l'input
 * @returns JSX.Element
 */
const InputText = (props) => {
  const {
    label = "",
    underText = "",
    error = "",
    name = "",
    register = (_, rules = {}) => {},
    required = false,
    pattern,
    minLength,
    maxLength,
    defaultValue,
    placeholder,
    shouldUnregister = false,
    icon
  } = props;
  return (
    <div className={`flex flex-col my-2`}>
      <Text
        variant="Body"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {label}
        {error === "" ? "" : <StyledError>{" - " + error}</StyledError>}
      </Text>
      <div className="relative flex items-center">
        {icon && (
          <div
            className="absolute left-0 select-none outline-none p-3 h-11 flex items-center justify-center input-icon"
            data-testid="icon">
            {icon}
          </div>
        )}
        <StyledInput
          name={name}
          type="text"
          defaultValue={defaultValue}
          placeholder={placeholder}
          {...register(name, {
            shouldUnregister,
            required,
            pattern: pattern ?? {},
            minLength: minLength ?? null,
            maxLength: maxLength ?? null
            // validate: validate,
          })}
          error={error !== ""}
          icon={icon}
        />
      </div>
      <Text
        variant="Caption"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {underText}
      </Text>
    </div>
  );
};

export default InputText;
