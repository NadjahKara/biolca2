import styled from "styled-components";
export const StyledCheckbox = styled.input`
  appearance: none;
  height: 1.25rem;
  width: 1.25rem;
  border: 1px solid var(--neutral-600);
  background: white;
  border-radius: 0.125rem;
  vertical-align: top;
  background-repeat: no-repeat;
  background-size: contain;
  cursor: pointer;
  background-position: center;
  transition-property: color, background-color, border-color,
    text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter,
    backdrop-filter;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 150ms;
  &:checked {
    background: var(--primary-300);
  }
  &:focus {
    outline: none;
  }
`;
