import { fireEvent, render, screen } from "@testing-library/react";
import Checkbox from "..";

const mockedOnCheck = jest.fn();

describe("Checkbox", () => {
  it("should trigger onCheck when the checkbox is clicked", () => {
    // given
    render(<Checkbox onCheck={mockedOnCheck} />);
    const checkBoxElement = screen.getByRole("checkbox");
    // when
    fireEvent.click(checkBoxElement);
    // then
    expect(mockedOnCheck).toHaveBeenCalledTimes(1);
  });

  it("should be check when checked is set to true", () => {
    // given
    render(<Checkbox onCheck={mockedOnCheck} checked={true} />);
    const checkBoxElement = screen.getByRole("checkbox");
    // when
    fireEvent.click(checkBoxElement);
    // then
    expect(checkBoxElement).toBeChecked();
  });

  it("should not be check when checked is set to false", () => {
    // given
    render(<Checkbox onCheck={mockedOnCheck} checked={false} />);
    const checkBoxElement = screen.getByRole("checkbox");
    // when
    fireEvent.click(checkBoxElement);
    // then
    expect(checkBoxElement).not.toBeChecked();
  });
});

// With jest-dom, you can use expect(node).toBeChecked();
