import React from "react";
import { StyledCheckbox } from "./styles";

/**
 * Display checkbox
 * @param {boolean} checked if set to true the box is checked
 * @param {() => void} onCheck function trigger when the box is checked (must updated checked state)
 * @returns
 */
const Checkbox = (props) => {
  const { checked = false, onCheck = () => {}, className } = props;
  return (
    <StyledCheckbox
      type="checkbox"
      className={`form-check-input checked:bg-primary-300 transition duration-200 ${className}`}
      checked={checked}
      onChange={async (_) => {
        await onCheck(!checked);
      }}
    />
  );
};

export default Checkbox;
