import React, { useEffect } from "react";
import { useDropzone } from "react-dropzone";
import { BiUpload, BiXCircle } from "react-icons/bi";
import { Text } from "../../typography/";
import { StyledDropper, Container } from "./styles";

function Dropper({ title, caption, files = [], setFiles }) {
  const { getRootProps, getInputProps } = useDropzone({
    //   only accept jar
    accept: "application/java-archive",
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file)
          })
        )
      );
    }
  });

  useEffect(
    () => () => {
      console.log({ files });
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );

  return (
    <Container>
      <StyledDropper {...getRootProps({})}>
        <input {...getInputProps()} />
        <BiUpload className="icon" />
        <Text variant="Title">{title}</Text>
        <div className="space" />
        <Text variant="Caption">{caption}</Text>
      </StyledDropper>
      <div>
        {files.map((file, index) => {
          return (
            <div className="preview">
              <Text color="var(--neutral-400)">{file.name}</Text>
              <BiXCircle
                className="icon"
                onClick={() => {
                  setFiles((files) => {
                    const aux = [...files];
                    aux.splice(index, 1);
                    return aux;
                  });
                }}
              />
            </div>
          );
        })}
      </div>
    </Container>
  );
}

export default Dropper;
