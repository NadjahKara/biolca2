import { render, screen } from "@testing-library/react";
import DropArea from "..";

describe("DropArea", () => {
  it("should display the title", () => {
    // given
    render(<DropArea title="Hello Test" />);
    // when
    const titleElement = screen.getByText(/hello test/i);
    // then
    expect(titleElement).toBeInTheDocument();
  });

  it("should display the caption", () => {
    // given
    render(<DropArea title="Hello Test" caption="it's me again" />);
    // when
    const captionElement = screen.getByText(/it's me again/i);
    // then
    expect(captionElement).toBeInTheDocument();
  });
});
