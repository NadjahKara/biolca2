import styled from "styled-components";

export const StyledDropper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  cursor: pointer;

  min-height: 18.5rem;
  overflow: hidden;

  background: var(--neutral-200);

  outline: none;
  border-radius: 0.5rem;

  transform: translate(var(--tw-translate-x), var(--tw-translate-y))
    rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y))
    scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));

  .icon {
    height: 2.5rem;
    width: 2.5rem;
    color: var(--primary-300);
    margin: 2rem 0;
  }
  .space {
    height: 0.5rem;
  }
`;

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  div {
    width: 100%;
    max-width: 36rem;
    @media (man-width: 768px) {
      max-width: 80%;
    }
    .preview {
      display: flex;
      align-items: center;
      justify-content: space-between;
      .icon {
        color: var(--error-300);
        cursor: pointer;
      }
    }
  }
`;
