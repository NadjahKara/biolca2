import { render, screen } from "@testing-library/react";
import Button from "..";

describe("Button", () => {
  it("should succesfully render Text when the button isn't loading", () => {
    // given
    render(<Button>example-button</Button>);
    const buttonElement = screen.getByText("example-button");
    // when
    // then
    expect(buttonElement).toBeInTheDocument();
  });

  it("should  render the text and the loader when the button is loading", () => {
    // given
    render(<Button loading={1}>example-button</Button>);
    const buttonElement = screen.getByText("example-button");
    const loaderElement = screen.getByTestId("loader-icon");
    // when
    // then
    expect(buttonElement).toBeInTheDocument();
    expect(loaderElement).toBeInTheDocument();
  });

  it("should succesfully render the Button with primary variant by default", () => {
    // given
    render(<Button />);
    const primaryButtonElement = screen.getByTestId("primary-button");
    // when
    // then
    expect(primaryButtonElement).toBeInTheDocument();
  });

  it("should succesfully render secondary variant button", () => {
    // given
    render(<Button children="test click me" variant="secondary" />);
    const secondaryButtonElement = screen.getByTestId("secondary-button");
    // when
    // then
    expect(secondaryButtonElement).toBeInTheDocument();
  });

  it("should succesfully render tertiary variant button", () => {
    // given
    render(<Button children="test click me" variant="tertiary" />);
    const tertiaryButtonElement = screen.getByTestId("tertiary-button");
    // when
    // then
    expect(tertiaryButtonElement).toBeInTheDocument();
  });

  it("should succesfully render danger variant button", () => {
    // given
    render(<Button children="test click me" variant="danger" />);
    const dangerButtonElement = screen.getByTestId("danger-button");
    // when
    // then
    expect(dangerButtonElement).toBeInTheDocument();
  });

  it("should succesfully render login variant button", () => {
    // given
    render(<Button children="test click me" variant="login" />);
    const loginButtonElement = screen.getByTestId("login-button");
    const logoElement = screen.getByAltText("ollca's logo");
    // when
    // then
    expect(loginButtonElement).toBeInTheDocument();
    expect(logoElement).toBeInTheDocument();
  });
});
