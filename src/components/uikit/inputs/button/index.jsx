import React from "react";
import { Loading } from "../../feedback";
import { Text } from "../../typography";
import ollca from "../../../../assets/images/ollca.png";

import {
  StyledPrimary,
  StyledTertiary,
  StyledSecondary,
  StyledDanger,
  StyledLogin,
  LogoWrapper
} from "./styles";

const ButtonLoader = ({ loading, disabled, ...props }) => {
  return (
    <>
      {disabled && (
        <div
          className="absolute bg-black opacity-60 flex top-0 left-0 justify-center items-center h-full w-full"
          data-testid="loader-icon"
        >
          {loading && (
            <LogoWrapper>
              <Loading white={true} />
            </LogoWrapper>
          )}
        </div>
      )}
    </>
  );
};

/**
 * Display a Button
 * @param {string} variant button variant to change the style must be one of
 * "primary"|"secondary"|"tertiary"|"danger"|"login"
 * @param {boolean} loading if set to true a spinner is display inside the button
 * @param {boolean} disabled if set to true the button is disabled
 *
 */
const Button = (props) => {
  const { children, variant = "primary", loading = false } = props;
  const descendant = (
    <>
      <ButtonLoader
        loading={loading}
        disabled={loading || props.disabled}
        data-testid="loader-icon"
      />
      <Text
        variant="Title"
        inverted={["primary", "danger"].includes(variant)}
        primary={["secondary"].includes(variant)}
      >
        {children}
      </Text>
    </>
  );
  return (
    <>
      {["primary", "", undefined, null].includes(variant) ? (
        <StyledPrimary
          data-testid="primary-button"
          {...props}
          loading={props?.loading ? 1 : 0}
        >
          {descendant}
        </StyledPrimary>
      ) : variant === "secondary" ? (
        <StyledSecondary
          data-testid="secondary-button"
          {...props}
          loading={props?.loading ? 1 : 0}
        >
          {descendant}
        </StyledSecondary>
      ) : variant === "tertiary" ? (
        <StyledTertiary
          data-testid="tertiary-button"
          {...props}
          loading={props?.loading ? 1 : 0}
        >
          {descendant}
        </StyledTertiary>
      ) : variant === "danger" ? (
        <StyledDanger
          data-testid="danger-button"
          {...props}
          loading={props?.loading ? 1 : 0}
        >
          {descendant}
        </StyledDanger>
      ) : (
        variant === "login" && (
          <StyledLogin
            data-testid="login-button"
            {...props}
            loading={props?.loading ? 1 : 0}
          >
            <img alt="ollca's logo" src={ollca} />
          </StyledLogin>
        )
      )}
    </>
  );
};

export default Button;
