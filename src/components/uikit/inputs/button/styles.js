import styled from "styled-components";
export const StyledTertiary = styled.button`
  height: 40px;
  border-radius: 0.25rem;
  position: relative;
  border: 2px solid var(--text);
  padding: 0 2.5rem;
  transition: 150ms ease-in-out;
  cursor: pointer;
  &:focus,
  &:active {
    outline: none;
    background: rgba(24, 24, 24, 0.15);
  }
  overflow: hidden;
`;

export const StyledPrimary = styled(StyledTertiary)`
  background: var(--primary-300);
  border-width: 0px !important;
  &:hover {
    background: var(--primary-200);
  }
  &:focus,
  &:active {
    outline: 2px solid var(--text-inverted);
    outline-offset: -4px;
    background: var(--primary-400);
  }
`;

export const StyledSecondary = styled(StyledTertiary)`
  border-color: var(--primary-300);
  color: var(--primary-300);
  background: transparent;
  &:hover {
    border-color: var(--primary-200);
    color: var(--primary-200);
  }
  &:focus,
  &:active {
    background: none;
    border-color: var(--primary-400);
    color: var(--primary-400);
  }
`;

export const StyledDanger = styled(StyledTertiary)`
  background: var(--error-300);
  border-width: 0px !important;
  &:hover {
    background: var(--error-200);
  }
  &:focus,
  &:active {
    outline: 2px solid var(--text-inverted);
    outline-offset: -4px;
    background: var(--error-400);
  }
`;

export const StyledLogin = styled(StyledTertiary)`
  height: 3.375rem;
  background: transparent;
  border-color: var(--primary-300);
  border-radius: 3.125rem;
  &:hover {
    border-color: var(--primary-200);
  }
  &:focus,
  &:active {
    background: none;
    border-color: var(--primary-400);
  }
  img {
    height: 1.75rem;
    width: 6.25rem;
  }
`;

export const LogoWrapper = styled("div")`
  height: 18px;
  width: 18px;
`;
