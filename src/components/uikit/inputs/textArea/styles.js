import styled from "styled-components";
export const StyledAreaError = styled.span`
  font-style: italic;
  text-transform: initial;
`;

export const StyledAreaInput = styled("textarea")`
  transition: 150ms ease-in-out;
  resize: none;
  display: block;
  outline: none;
  width: 100%;
  font-size: 0.875rem;
  line-height: 1.25rem;
  border: 1px var(--neutral-600) solid;
  box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);
  padding: 0.5rem 0.75rem;
  max-height: 8rem;
  ${({ error }) =>
    error && "border-color : #ef4444; &:focus{border-color:#fca5a5;  }"};
  border-radius: 0.375rem;
  &:focus {
    outline: none;
    border-color: var(--primary-300);
    --tw-ring-color: var(--primary-300);
    box-shadow: var(--tw-ring-inset) 0 0 0
      calc(1px + var(--tw-ring-offset-width)) var(--tw-ring-color);
  }
`;
