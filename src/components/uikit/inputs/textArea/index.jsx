import React from "react";
import { Text } from "../../typography";
import { StyledAreaError, StyledAreaInput } from "./styles";

/**
 * InputText
 * @description
 * @param {object} props InputText props.
 * @param {string} props.label texte du label affiché au dessus de l'input.
 * @param {string} props.type type de l'input (text - password ...).
 * @param {string} props.error texte d'erreur
 * @param {string} props.name nom de l'input
 * @param {boolean} props.required si vrai le text est requis dans ce champs
 * @param {JSX} props.register useForm register
 * @param {string} props.underText texte sous l'input
 * @returns JSX.Element
 * @author
 */
const TextArea = (props) => {
  const {
    label = "",
    underText = "",
    error = "",
    name = "",
    register = (_, rules = {}) => {},
    required = false,
    pattern,
    minLength,
    maxLength,
    defaultValue,
    placeholder,
    shouldUnregister = false,
    rows = 2
  } = props;
  return (
    <div className={`flex flex-col my-2`}>
      <Text
        variant="Body"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {label}
        {error === "" ? "" : <StyledAreaError>{" - " + error}</StyledAreaError>}
      </Text>
      <div className="relative flex items-center">
        <StyledAreaInput
          name={name}
          type="text"
          defaultValue={defaultValue}
          placeholder={placeholder}
          {...register(name, {
            shouldUnregister,
            required,
            pattern: pattern ?? {},
            minLength: minLength ?? null,
            maxLength: maxLength ?? null
            // validate: validate,
          })}
          error={error !== ""}
          rows={rows}
          className={"px-2"}
        />
      </div>
      <Text
        variant="Caption"
        color={error === "" ? "var(--text)" : "rgb(239,68,68)"}
      >
        {underText}
      </Text>
    </div>
  );
};

export default TextArea;
