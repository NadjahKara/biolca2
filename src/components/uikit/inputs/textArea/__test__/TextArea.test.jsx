import { render, screen } from "@testing-library/react";
import TextArea from "..";

describe("TextArea", () => {
  it("should display label", () => {
    // given
    // when
    render(<TextArea label="test label" />);
    const labelElement = screen.getByText(/test label/i);
    // then
    expect(labelElement).toBeInTheDocument();
  });
  it("should display undertext", () => {
    // given
    // when
    render(<TextArea label="test label" underText="undertext" />);
    const undertextElement = screen.getByText(/undertext/i);
    // then
    expect(undertextElement).toBeInTheDocument();
  });

  it("should display error", () => {
    // given
    // when
    render(<TextArea label="test label" underText="undertext" error="error" />);
    const errorElement = screen.getByText(/error/i);
    // then
    expect(errorElement).toBeInTheDocument();
  });

  it("should display input placeholder", () => {
    // given
    // when
    render(
      <TextArea
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
      />
    );
    const inputElement = screen.getByPlaceholderText("number");
    // then
    expect(inputElement).toBeInTheDocument();
  });

  it("should have fixed rows set to 3", () => {
    // given
    // when
    render(
      <TextArea
        label="test label"
        underText="undertext"
        error="error"
        placeholder="number"
        rows={3}
      />
    );
    const inputElement = screen.getByPlaceholderText("number");
    // then
    expect(inputElement).toHaveAttribute("rows", "3");
  });
});
