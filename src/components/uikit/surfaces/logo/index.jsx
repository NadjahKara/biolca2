import React from "react";

import logoSmall from "../../../../assets/images/logo-small.svg";
import logoLarge from "../../../../assets/images/logo-large.svg";
import { StyledImage } from "./styles";

/**
 * Affiche le logo biollca
 * @param {boolean} small si mis a true le petit logo est affiché le logo large
 * sinon
 * @param {string} size variant taille du composant parmis xs s base md lg (base par defaut)
 */
const Logo = ({ small = true, size = "base" }) => {
  return (
    <StyledImage
      src={small ? logoSmall : logoLarge}
      alt="app's logo"
      className={`${size}`}
    />
  );
};
export default Logo;
