import styled from "styled-components";

export const StyledImage = styled.img`
  &.xs {
    height: 3rem;
    width: 3rem;
  }
  &.s {
    height: 4rem;
    width: 4rem;
  }
  &.base {
    height: 5rem;
    width: 5rem;
  }
  &.md {
    height: 7.5rem;
    width: 7.5rem;
  }
  &.lg {
    height: 10rem;
    width: 10rem;
  }
`;
