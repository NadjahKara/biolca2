import Logo from "..";
import { screen, render } from "@testing-library/react";

describe("Logo", () => {
  it("should render small logo by default", () => {
    // given
    // when
    render(<Logo />);
    const logo = screen.getByAltText("app's logo");
    // then
    expect(logo.src).toContain("logo-small");
  });

  it("should render large logo when small is set to false", () => {
    // given
    // when
    render(<Logo small={false} />);
    const logo = screen.getByAltText("app's logo");
    // then
    expect(logo.src).toContain("logo-large");
  });

  it("should render logo with size set to base by default", () => {
    // given
    // when
    render(<Logo small={false} />);
    const logo = screen.getByAltText("app's logo");
    // then
    expect(logo.className).toContain("base");
  });

  it("should render logo with the size passed in props", () => {
    // given
    // when
    render(<Logo small={false} size="xs" />);
    const logo = screen.getByAltText("app's logo");
    // then
    expect(logo.className).toContain("xs");
  });
});
