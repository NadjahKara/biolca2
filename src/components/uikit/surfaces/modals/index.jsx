import React from "react";
import { Transition } from "@headlessui/react";
import { Fragment } from "react";
import { BiX } from "react-icons/bi";
import { Button } from "../../inputs";
import { Text } from "../../typography";
import { Divider } from "../../surfaces";
import {
  Body,
  Container,
  Footer,
  Dialog,
  Overlay,
  CenteredContent,
  Header,
  Content
} from "./styles";
const Section = ({ children, ...props }) => {
  return (
    <div>
      <div className="my-6 border border-gray-200" />
      {children}
    </div>
  );
};

/**
 *
 * @param {boolean} isOpen is vrai la modal s'ouvre fermé sinon
 * @param {() => void} isOpen setter du state is open
 * @param {string} title titre affiché en haut de la modal
 * @param {React.ReactNode} composant fils affiché dans le corp de la modal
 * @param {() => void} onConfirm function déclanché quand le boutton de confirmation
 * est cliqué
 * @param {boolean} disabled si vrai la modal est désctivé (impossible de fermer la modal)
 * @param {string} maxWidth taille maximal occupé par la modal (80% par defaut)
 * @param {{children: string,variant: string,onClick: () => void}} buttonProps propriété transmise au bouton de confirmation
 * @param {{children: string,variant: string,onClick: () => void}} cancelButtonProps
 * propriété transmise au bouton d'annulation si défini le bouton est affiché
 * @param {() => void} onClose callback fonction exécuté apres la fermeture de la modal
 * @returns
 */
const Modal = ({
  isOpen = false,
  setIsOpen,
  title,
  children,
  onConfirm,
  disabled,
  maxWidth = "80%",
  buttonProps,
  cancelButtonProps,
  onClose = () => {},
  ...props
}) => {
  function closeModal() {
    setIsOpen(false);
    onClose();
  }

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" onClose={disabled ? () => {} : closeModal}>
        <Container>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            // enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Overlay />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <CenteredContent />
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            <Content maxWidth={maxWidth}>
              <Header>
                <Text variant="Heading">{title}</Text>
                <BiX
                  data-testid="closeIcon"
                  className="icon"
                  onClick={(event) => {
                    if (disabled) return;
                    event.stopPropagation();
                    closeModal();
                  }}
                />
              </Header>
              <Divider />
              <Body>{children}</Body>
              <Footer>
                {cancelButtonProps && (
                  <Button
                    variant="tertiary"
                    className="mx-2"
                    {...cancelButtonProps}
                  />
                )}
                {buttonProps && <Button {...buttonProps} />}
              </Footer>
            </Content>
          </Transition.Child>
        </Container>
      </Dialog>
    </Transition>
  );
};
Modal.Section = Section;
export default Modal;
