import { render, screen, fireEvent } from "@testing-library/react";
import Modal from "..";

describe("Modal", () => {
  const mockTitle = "Hello test";

  const mockHandleClick = jest.fn();
  const mockOnClose = jest.fn();
  const mockSetIsOpen = jest.fn();

  const mockButtonProps = {
    children: "button test",
    variant: "primary",
    onClick: mockHandleClick
  };

  const mockCancelButtonProps = {
    children: "cancel button test",
    variant: "primary",
    onClick: mockOnClose
  };

  it("should not display the modal since isOpen is not set to true", () => {
    // given
    render(<Modal title={mockTitle} />);
    // when
    const titleElement = screen.queryByText(/hello test/i);
    // then
    expect(titleElement).toBeNull();
  });

  it("should display the modal since isOpen is set to true", () => {
    // given
    render(<Modal title={mockTitle} isOpen={true} />);
    // when
    const titleElement = screen.getByText(/hello test/i);
    // then
    expect(titleElement).toBeInTheDocument();
  });

  it("should display the children inside the modal body", () => {
    // given
    render(
      <Modal
        title={mockTitle}
        isOpen={true}
        children={<h1>test children</h1>}
      />
    );
    // when
    const childrenElement = screen.getByText(/test children/i);
    // then
    expect(childrenElement).toBeInTheDocument();
  });

  it("should trigger the onClick when the button is clicked", () => {
    // given
    render(
      <Modal
        title={mockTitle}
        isOpen={true}
        children={<h1>test children</h1>}
        buttonProps={mockButtonProps}
      />
    );
    // when
    const buttonElement = screen.getByText(/button test/i);
    fireEvent.click(buttonElement);
    // then
    expect(buttonElement).toBeInTheDocument();
    expect(mockHandleClick).toHaveBeenCalledTimes(1);
  });

  it("should trigger the setIsOpen, onClose when the close icon is clicked", () => {
    // given
    render(
      <Modal
        title={mockTitle}
        isOpen={true}
        children={<h1>test children</h1>}
        buttonProps={mockButtonProps}
        onClose={mockOnClose}
        setIsOpen={mockSetIsOpen}
      />
    );
    // when
    const iconElement = screen.getByTestId("closeIcon");
    fireEvent.click(iconElement);
    // then
    expect(iconElement).toBeInTheDocument();
    expect(mockOnClose).toHaveBeenCalledTimes(1);
    expect(mockSetIsOpen).toHaveBeenCalledTimes(1);
    expect(mockSetIsOpen).toHaveBeenCalledWith(false);
  });

  it("click on icon should not close modal when it is disabled", () => {
    // given
    render(
      <Modal
        title={mockTitle}
        isOpen={true}
        children={<h1>test children</h1>}
        buttonProps={mockButtonProps}
        onClose={mockOnClose}
        setIsOpen={mockSetIsOpen}
        disabled={true}
      />
    );
    // when
    const iconElement = screen.getByTestId("closeIcon");
    fireEvent.click(iconElement);
    // then
    expect(iconElement).toBeInTheDocument();
    expect(mockOnClose).toHaveBeenCalledTimes(0);
    expect(mockSetIsOpen).toHaveBeenCalledTimes(0);
  });

  it("cancelbutton should appear on screen if cancelButtonProps is not null", () => {
    // given
    render(
      <Modal
        title={mockTitle}
        isOpen={true}
        children={<h1>test children</h1>}
        cancelButtonProps={mockCancelButtonProps}
      />
    );
    // when
    const iconElement = screen.getByText(/cancel button test/i);
    // then
    expect(iconElement).toBeInTheDocument();
  });

  it("cancelbutton should not appear on screen if cancelButtonProps is null", () => {
    // given
    render(
      <Modal
        title={mockTitle}
        isOpen={true}
        children={<h1>test children</h1>}
        cancelButtonProps={null}
      />
    );
    // when
    const iconElement = screen.queryByText(/cancel button test/i);
    // then
    expect(iconElement).not.toBeInTheDocument();
  });

  it("should trigger the onClick when the cancelButton is clicked", () => {
    // given
    render(
      <Modal
        title={mockTitle}
        isOpen={true}
        children={<h1>test children</h1>}
        cancelButtonProps={mockCancelButtonProps}
      />
    );
    // when
    const buttonElement = screen.getByText(/cancel button test/i);
    fireEvent.click(buttonElement);
    // then
    expect(buttonElement).toBeInTheDocument();
    expect(mockOnClose).toHaveBeenCalledTimes(1);
  });
});
