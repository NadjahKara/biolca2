import styled from "styled-components";
import { Dialog as OgDialog } from "@headlessui/react";

export const Dialog = styled(OgDialog)`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  overflow-y: auto;
  z-index: 50 !important;
`;
export const Container = styled.div`
  min-height: 100vh;
  padding: 0 1rem;
  text-align: center;
`;
export const Overlay = styled.div`
  background-color: rgba(0, 0, 0, 0.5);
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
`;

export const CenteredContent = styled.span`
  height: 100vh;
  vertical-align: middle;
  display: inline-block;
`;
export const Content = styled.div`
  display: inline-block;
  width: 100%;
  max-width: 36rem;
  padding: 1rem 0;
  margin: 2rem 0;
  overflow: hidden;
  text-align: left;
  vertical-align: middle;
  transition-property: all;
  transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);
  transition-duration: 150ms;
  background: white;
  border-radius: 0.75rem;
  box-shadow: 0 20px 25px -5px rgb(0 0 0 / 0.1),
    0 8px 10px -6px rgb(0 0 0 / 0.1);

  transform: translate(var(--tw-translate-x), var(--tw-translate-y))
    rotate(var(--tw-rotate)) skewX(var(--tw-skew-x)) skewY(var(--tw-skew-y))
    scaleX(var(--tw-scale-x)) scaleY(var(--tw-scale-y));

  @media (min-width: 768px) {
    ${({ maxWidth }) => maxWidth && `max-width: ${maxWidth};`}
  }
`;
export const Header = styled.div`
  color: var(--text);
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 1.5rem;
  .icon {
    cursor: pointer;
    height: 1.25rem;
    width: 1.25rem;
  }
`;
export const Body = styled.div`
  width: 100%;
  padding: 0 1.5rem;
  margin-bottom: 1rem;
`;
export const Footer = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  padding: 0 1.5rem;
`;
