import React from "react";
const Divider = () => {
  return <hr className="w-full my-4 bg-neutral-300 border-0 h-px" />;
};

export default Divider;
