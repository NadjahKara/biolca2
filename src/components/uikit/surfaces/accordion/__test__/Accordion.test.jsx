import { render, screen, fireEvent } from "@testing-library/react";
import Accordion from "..";

describe("Accordion", () => {
  it("should render", () => {
    render(<Accordion />);

    expect(screen.getByTestId("accordion-test")).toBeInTheDocument();
  });

  it("should have a label", () => {
    render(<Accordion label="label-test" />);

    expect(screen.getByText("label-test")).toBeInTheDocument();
  });

  it("should be closed by default", () => {
    render(<Accordion />);
    expect(screen.getByTestId("accordion-body-test")).toHaveStyle(
      "max-height: 0;"
    );
  });

  it("should open on click", () => {
    render(<Accordion />);

    fireEvent(
      screen.getByTestId("accordion-test"),
      new MouseEvent("click", {
        bubbles: true,
        cancelable: true
      })
    );

    expect(screen.getByTestId("accordion-test")).toHaveAttribute("open");
    expect(screen.getByTestId("accordion-body-test")).toHaveAttribute("open");
    expect(screen.getByTestId("accordion-body-test")).not.toHaveStyle(
      "max-height: 0;"
    );
  });

  it("should open on Enter key event", () => {
    render(<Accordion />);

    fireEvent(
      screen.getByTestId("accordion-test"),
      new KeyboardEvent("keydown", {
        code: "Enter",
        bubbles: true,
        cancelable: true
      })
    );

    expect(screen.getByTestId("accordion-test")).toHaveAttribute("open");
    expect(screen.getByTestId("accordion-body-test")).toHaveAttribute("open");
    expect(screen.getByTestId("accordion-body-test")).not.toHaveStyle(
      "max-height: 0;"
    );
  });

  it("should open on Space key event", () => {
    render(<Accordion />);

    fireEvent(
      screen.getByTestId("accordion-test"),
      new KeyboardEvent("keydown", {
        code: "Space",
        bubbles: true,
        cancelable: true
      })
    );

    expect(screen.getByTestId("accordion-test")).toHaveAttribute("open");
    expect(screen.getByTestId("accordion-body-test")).toHaveAttribute("open");
    expect(screen.getByTestId("accordion-body-test")).not.toHaveStyle(
      "max-height: 0;"
    );
  });

  it("should render its children", () => {
    render(<Accordion>accordion-test</Accordion>);

    expect(screen.getByText("accordion-test")).toBeInTheDocument();
  });
});
