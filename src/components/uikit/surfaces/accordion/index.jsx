import React, { useState } from "react";
import styled from "styled-components";
import { Text } from "../../typography";

/* ================================================================= */
/*                              STYLE                                */
/* ================================================================= */

const Bg = styled("div")`
  position: relative;
  display: flex;
  flex-direction: column;
  transition: 150ms ease-in-out;
  width: 100%;
  box-sizing: border-box;
`;

const IconAccordion = styled("svg")`
  margin-right: 12px;

  transition: 0.13s transform ease-in-out;

  ${({ open }) => open && "transform: rotate(90deg);"}
`;

const Label = styled("div")`
  position: relative;
  cursor: pointer;
  user-select: none;
  display: flex;
  height: 48px;
  width: 100%;
  background-color: var(--neutral-100);
  border-bottom: 1px var(--neutral-300) solid;
  text-align: center;
  align-items: center;
  padding: 16px;

  &:focus-visible {
    background: var(--neutral-200);
    outline: none;
  }
`;

const Body = styled("div")`
  position: relative;
  width: 100%;
  background-color: var(--neutral-100);
  padding: 0 16px;

  max-height: 0;
  overflow: hidden;

  transition: max-height 0.065s ease-in-out, border 0.065s ease-in-out,
    padding 0.065s ease-in-out;

  ${({ open }) =>
    open
      ? "max-height: 160vh; padding: 16px; border-bottom: 1px var(--neutral-300) solid; overflow: auto;"
      : "> * { visibility: hidden; }"}
`;

/* ================================================================= */
/*                            COMPONENTS                             */
/* ================================================================= */

/**
 * Composant pour afficher une option à l'intérieur d'un Select
 * @component
 * @param {string} label Le texte affiché à l'utilisateur comme titre de l'accordeon
 * @param {function} children Une liste des éléments à l'intérieur de l'Accordion
 *
 */
function Accordion({ label, children }) {
  const [open, setOpen] = useState(false);

  function handleKeyDown(e) {
    if (e.code === "Space" || e.code === "Enter") setOpen(!open);
  }

  function handleOpen(event) {
    setOpen(!open);
  }

  return (
    <Bg>
      <Label
        onClick={handleOpen}
        open={open}
        tabIndex="0"
        onKeyDown={handleKeyDown}
        data-testid="accordion-test"
      >
        <IconAccordion
          open={open}
          width="24"
          height="24"
          viewBox="0 0 24 24"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path d="M9 19L17 12L9 5V19Z" fill="var(--text)" />
        </IconAccordion>
        <Text variant="Title">{label}</Text>
      </Label>
      <Body open={open} data-testid="accordion-body-test">
        <div>{children}</div>
      </Body>
    </Bg>
  );
}

export default Accordion;
