import React from "react";
import { useNavigate } from "react-router-dom";
import { Text } from "../../typography";
import { disconnect } from "../../../../service/auth.service";
import {
  StyledHeader,
  StyledLogout,
  StyledNavigation,
  StyledRoutesContainer,
  StyledTitleContainer
} from "./styles";

/**
 *
 * @param {{title: string, route: string}[]} routes Tableau d'objet contenant les
 * elements de navigation à afficher chaque objet doit contenir les champs *title*
 * reprensentant le label de l'élement de navigation ainsi que *route* chemin
 * vers lequel l'élement navigue
 * @param {string} title titre affiché dans le header, si ce titre est défini les
 * élements de navigations *routes* son ignoré
 */
const Header = ({ routes, title }) => {
  const navigate = useNavigate();

  const handleLogout = () => {
    disconnect();
    navigate("/");
  };

  return (
    <StyledHeader>
      {!["", null, undefined].includes(title) ? (
        <StyledTitleContainer>
          <span className="dot" />
          <Text variant="PageSubTitle" inverted>
            {title}
          </Text>
        </StyledTitleContainer>
      ) : (
        <StyledRoutesContainer>
          <StyledNavigation>
            {routes?.map((route) => {
              return (
                <li
                  key={`${route.title}-navElement`}
                  onClick={() => {
                    navigate(route?.route);
                  }}
                >
                  <Text variant="Title" inverted>
                    {route.title}
                  </Text>
                </li>
              );
            })}
          </StyledNavigation>
          <StyledLogout onClick={handleLogout}>
            <Text variant="Title" inverted>
              Se déconnecter
            </Text>
          </StyledLogout>
        </StyledRoutesContainer>
      )}
    </StyledHeader>
  );
};

export default Header;
