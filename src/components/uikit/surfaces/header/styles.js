import styled from "styled-components";

export const StyledHeader = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 2000;
  width: 100vw;
  background: var(--neutral-500);
  height: 3.75rem;
  display: flex;
  align-items: center;
`;

export const StyledTitleContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
  .dot {
    height: 0.5rem;
    width: 0.5rem;
    margin: 0 0.75rem;
    background: var(--secondary-300);
    border-radius: 100%;
  }
`;

export const StyledRoutesContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: auto;
`;

export const StyledLogout = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 0 2.5rem;
  border-left: 1px solid var(--text-inverted);
  cursor: pointer;
`;

export const StyledNavigation = styled.nav`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: auto;
  list-style-type: none;
  li {
    cursor: pointer;
    margin: 1.25rem;
    &:first-of-type {
      margin-left: 0rem;
    }
    &:last-of-type {
      margin-right: 2.5rem;
    }
  }
`;
