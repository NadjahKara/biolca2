import { fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter } from "react-router-dom";
import Header from "..";

const MockedHeader = (props) => {
  return (
    <BrowserRouter>
      <Header {...props} />
    </BrowserRouter>
  );
};

const mockNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockNavigate
}));

describe("Header", () => {
  const mockedRoutes = [
    {
      title: "route1",
      route: "/navRoute1"
    }
  ];

  it("should display the Title in the header", () => {
    // given
    // when
    render(<MockedHeader title="Test header" />);
    const titleElement = screen.getByText(/test header/i);
    // then
    expect(titleElement).toBeInTheDocument();
  });

  it("should not display the routes element in the header since the title is defined", () => {
    // given
    // when
    render(<MockedHeader title="Test header" routes={mockedRoutes} />);
    const titleElement = screen.queryByText(mockedRoutes[0].title);
    // then
    expect(titleElement).toBeNull();
  });

  it("should display the routes element in the header since the title is not defined", () => {
    // given
    // when
    render(<MockedHeader routes={mockedRoutes} />);
    const routeElement = screen.getByText(mockedRoutes[0].title);
    // then
    expect(routeElement).toBeInTheDocument();
  });

  it("should trigger navigate when the route element is clicked", () => {
    // given
    render(<MockedHeader routes={mockedRoutes} />);
    const titleElement = screen.getByText(mockedRoutes[0].title);
    // when
    fireEvent.click(titleElement);
    // then
    expect(titleElement).toBeInTheDocument();
    expect(mockNavigate).toHaveBeenCalledTimes(1);
    expect(mockNavigate).toHaveBeenCalledWith(mockedRoutes[0].route);
  });
});
