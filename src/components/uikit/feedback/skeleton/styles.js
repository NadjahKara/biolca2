import styled from "styled-components";

export const SkeletonContainer = styled.div`
  width: 100%;
  animation: pulse 2s cubic-bezier(0.4, 0, 0.6, 1) infinite;
  @keyframes pulse {
    50% {
      opacity: 0.5;
    }
  }
`;

export const GridContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(
    ${({ threeLines }) => (threeLines ? "4" : "3")},
    minmax(0, 1fr)
  );
  gap: 1rem;
`;

export const StyledTextSkeleton = styled.div`
  margin: 0.375rem 0;
  height: 0.5rem;
  border-radius: 0.25rem;
  background: rgb(226, 232, 240);
  ${({ span }) => span && `grid-column: span ${span} / span ${span};`}
`;

export const StyledButtonSkeleton = styled.div`
  margin: 0.375rem 0;
  height: 2.5rem;
  width: 100%;
  border-radius: 0.25rem;
  background: rgb(226, 232, 240);
`;

export const StyledBlockSkeleton = styled.div`
  margin: 0.375rem 0;
  height: calc(2.5rem * ${({ lines }) => lines});
  width: 100%;
  border-radius: 0.25rem;
  background: rgb(226, 232, 240);
`;

export const StyledInputSkeleton = styled.div`
  margin: 0.375rem 0;
  width: 100%;
  border-radius: 0.25rem;
  background: rgb(226, 232, 240);
  outline: none;
  height: 2.25rem;
  box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);
`;
