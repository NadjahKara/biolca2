import { render, screen } from "@testing-library/react";
import Skeleton from "..";

describe("Skeleton renders", () => {
  it("by default render a text variant with 1 line", () => {
    render(<Skeleton />);
    expect(screen.getAllByTestId("large-text-skeleton").length).toBe(1);
  });
  it("renders a text variant with as many lines as mentionned", () => {
    let lines = 19;

    let countShort = 0;
    let countLong = 0;

    new Array(lines).fill(0).forEach((_, index) => {
      if ((index + 1) % 2 === 0) {
        const threeLines = (index + 1) % 4 === 0;
        if (threeLines) {
          countShort += 2;
        } else {
          countShort += 1;
        }
        countLong++;
        return;
      }
      countLong++;
    });

    render(<Skeleton variant="text" lines={lines} />);
    expect(screen.getAllByTestId("large-text-skeleton").length).toBe(countLong);
    expect(screen.getAllByTestId("short-text-skeleton").length).toBe(
      countShort
    );
  });
  it("renders a button variant", () => {
    render(<Skeleton variant="button" />);
    expect(screen.getByTestId("button-skeleton")).toBeInTheDocument();
  });
  it("renders a block variant", () => {
    render(<Skeleton variant="block" />);
    expect(screen.getByTestId("block-skeleton")).toBeInTheDocument();
  });
  it("renders an input", () => {
    render(<Skeleton variant="input" />);
    expect(screen.getByTestId("input-skeleton")).toBeInTheDocument();
  });
  it("renders the perfect size for the block", () => {
    const lines = 9;
    render(<Skeleton variant="block" lines={lines} />);
    expect(screen.getByTestId("block-skeleton")).toBeInTheDocument();
    expect(screen.getByTestId("block-skeleton")).toHaveStyle(
      `height : calc(2.5rem * ${lines});`
    );
  });
});
