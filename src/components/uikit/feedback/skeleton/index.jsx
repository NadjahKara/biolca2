import React from "react";
import {
  GridContainer,
  SkeletonContainer,
  StyledBlockSkeleton,
  StyledButtonSkeleton,
  StyledInputSkeleton,
  StyledTextSkeleton
} from "./styles";

const TextSkeleton = ({ lines = 1 }) => {
  return (
    <>
      {new Array(lines).fill(0).map((_, index) => {
        if ((index + 1) % 2 === 0) {
          const threeLines = (index + 1) % 4 === 0;
          return (
            <GridContainer threeLines={threeLines} key={index}>
              {threeLines && (
                <StyledTextSkeleton
                  span={1}
                  data-testid="short-text-skeleton"
                />
              )}
              <StyledTextSkeleton span={2} data-testid="large-text-skeleton" />
              <StyledTextSkeleton span={1} data-testid="short-text-skeleton" />
            </GridContainer>
          );
        }
        return (
          <StyledTextSkeleton key={index} data-testid="large-text-skeleton" />
        );
      })}
    </>
  );
};

const Skeleton = ({ lines = 1, variant = "text" }) => {
  return (
    <SkeletonContainer test>
      {variant === "text" ? (
        <TextSkeleton lines={lines} />
      ) : variant === "button" ? (
        <StyledButtonSkeleton data-testid="button-skeleton" />
      ) : variant === "block" ? (
        <StyledBlockSkeleton lines={lines} data-testid="block-skeleton" />
      ) : (
        <StyledInputSkeleton data-testid="input-skeleton" />
      )}
    </SkeletonContainer>
  );
};

export default Skeleton;
