import { render, screen } from "@testing-library/react";
import Stepper from "..";

describe("Stepper", () => {
  it("should render a stepper with 3 step", () => {
    // given
    const mockSteps = ["1", "2", "3"];
    render(<Stepper steps={mockSteps} />);
    // when
    const stepsElements = screen.getAllByTestId("steps");
    // then
    expect(stepsElements).toHaveLength(3);
  });

  it("should render a stepper with a current step of 2", () => {
    // given
    const mockSteps = ["1", "2", "3"];
    render(<Stepper steps={mockSteps} current={2} />);
    // when
    const stepsElements = screen.getAllByTestId("check-icon");
    // then
    expect(stepsElements).toHaveLength(2);
  });
});
