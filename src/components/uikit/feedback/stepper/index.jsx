import React from "react";
import { BiCheck } from "react-icons/bi";
import { Container, Indicator, Separator, Step } from "./styles";

/**
 * Display Stepper
 * @param {any[]} steps array that must have as much element as steps
 * @param {number} current current steps
 */
const Steps = ({ steps = [], current }) => {
  return (
    <Container>
      {steps.map((_, index) => {
        return (
          <Step
            data-testid="steps"
            notLast={index !== steps.length - 1}
            key={`step-${index}`}>
            <Indicator reached={index <= current}>
              {index < current ? (
                <BiCheck data-testid="check-icon" className="icon" />
              ) : (
                index + 1
              )}
            </Indicator>
            {index !== steps.length - 1 && <Separator />}
          </Step>
        );
      })}
    </Container>
  );
};

export default Steps;
