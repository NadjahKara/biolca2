import styled from "styled-components";

export const Container = styled.div`
  margin: 1.5rem 0;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

export const Step = styled.div`
  display: flex;
  align-items: center;
  ${({ notLast }) => notLast && "flex : 1;"}
`;

export const Indicator = styled.div`
  transition: 150ms ease-in-out;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 100%;
  border: 1px solid var(--primary-300);
  font-weight: bold;

  ${({ reached }) =>
    reached
      ? "background : var(--primary-300); color : var(--text-inverted);"
      : "color : var(--primary-300);"}

  height: 3rem;
  width: 3rem;

  .icon {
    height: 1.5rem;
    width: 1.5rem;
    @media (min-width: 768px) {
      height: 2rem;
      width: 2rem;
    }
  }
`;

export const Separator = styled.div`
  flex: 1;
  height: 1px;
  background: var(--primary-300);
`;
