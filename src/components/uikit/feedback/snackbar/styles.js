import styled from "styled-components";
import { ToastContainer } from "react-toastify";

export const StyledSnackbar = styled(ToastContainer)`
  .Toastify__toast {
    background: var(--error-300);
    color: var(--text-inverted);
    height: 3.75rem;
    border-radius: 0.25rem;
    box-shadow: none;
    .Toastify__toast-body {
      div {
        font-weight: medium;
        line-height: 1.25rem;
        font-size: 0.875rem;
        display: flex;
        align-items: center;
        justify-content: center;
        text-align: center;
      }
    }
    .Toastify__close-button {
      display: none;
    }
  }
`;
