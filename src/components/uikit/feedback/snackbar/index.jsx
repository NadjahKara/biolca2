import React from "react";
import { StyledSnackbar } from "./styles";
export default function Snackbar() {
  return (
    <StyledSnackbar
      position="bottom-center"
      autoClose={5000}
      hideProgressBar
      newestOnTop
      closeOnClick
      rtl={false}
      pauseOnFocusLoss
      draggable
      pauseOnHover
    />
  );
}
