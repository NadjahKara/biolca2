import styled from "styled-components";

export const StyledEmpty = styled.div`
  height: 66.666667%;
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  .icon {
    height: ${({ size }) => size ?? "2.5rem"};
    width: ${({ size }) => size ?? "2.5rem"};
    color: var(--text);
  }
`;
