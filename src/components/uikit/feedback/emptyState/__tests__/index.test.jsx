import { render, screen } from "@testing-library/react";
import EmptyState from "..";

describe("Empty state", () => {
  it("renders body text", () => {
    render(<EmptyState text="This is a test" size="2.5rem" />);
    expect(screen.getByTestId("empty-body")).toBeInTheDocument();
    expect(screen.getByText("This is a test")).toBeInTheDocument();
  });
  it("renders icon", () => {
    render(<EmptyState text="This is a test" size="2.5rem" />);
    expect(screen.getByTestId("empty-icon")).toBeInTheDocument();
  });
});
