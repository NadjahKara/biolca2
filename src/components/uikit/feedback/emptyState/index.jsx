import React from "react";
import { BiCylinder } from "react-icons/bi";
import { Text } from "../../typography";
import { StyledEmpty } from "./styles";

/**
 * Component for displaying an empty state
 * @component
 * @param {string} text The text to display when no data is displayed
 * @param {string} size thz size of the displayed icon, can either be in px, rem, vh/vw or any other metric
 *
 */

export default function EmptyState({ text = "Aucune données trouvées", size }) {
  return (
    <StyledEmpty size={size}>
      <BiCylinder className="icon" data-testid="empty-icon" />
      <Text variant="Body" className="my-2" data-testid="empty-body">
        {text}
      </Text>
    </StyledEmpty>
  );
}
