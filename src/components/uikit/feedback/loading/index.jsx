import React from "react";
import styled from "styled-components";
import { keyframes } from "styled-components";

/* ================================================================= */
/*                              STYLE                                */
/* ================================================================= */

const rotateAnim = keyframes`
  0% { transform: rotate(0deg) scale(1); }
  12.5% { transform: rotate(45deg) scale(0); }
  25% { transform: rotate(90deg) scale(1); }
  37.5% { transform: rotate(135deg) scale(0); }
  50% { transform: rotate(180deg) scale(1); }
  62.5% { transform: rotate(225deg) scale(0); }
  75% { transform: rotate(270deg) scale(1); }
  87.5% { transform: rotate(315deg) scale(0); }
  100% { transform: rotate(360deg) scale(1); }
`;

const Logo = styled("div")`
  position: relative;
  height: 100%;
  width: 100%;

  > * {
    position: absolute;
    height: 44%;
    width: 44%;
    animation: ${rotateAnim} 8s ease-in-out infinite;
  }
`;

const Y1 = styled("div")`
  border-radius: 0 50% 50% 0;
  animation-delay: -2s;

  ${({ white }) =>
    white ? "background: var(--neutral-100)" : "background: #fdc627;"}
`;

const Y2 = styled("div")`
  bottom: 0;
  animation-delay: -0.5s;
  border-radius: 0 50% 50% 0;

  ${({ white }) =>
    white ? "background: var(--neutral-100)" : "background: #fdc627;"}
`;

const B1 = styled("div")`
  right: 0;
  border-radius: 50%;
  animation-delay: -1.5s;

  ${({ white }) =>
    white ? "background: var(--neutral-100)" : "background: #0D2D44;"}
}
`;

const V1 = styled("div")`
  bottom: 0;
  right: 0;
  animation-delay: -1s;

  ${({ white }) =>
    white ? "background: var(--neutral-100)" : "background: #1caf8d;"}
`;

/* ================================================================= */
/*                            COMPONENTS                             */
/* ================================================================= */

/**
 * Component for displaying a loader
 * @component
 * @param {boolean} white If the loading animation should be completely white or have biollca's colors
 *
 */

const Loading = ({ white = false }) => {
  return (
    <Logo>
      <Y1 white={white} data-testid="logo-y1" />
      <B1 white={white} data-testid="logo-b1" />
      <Y2 white={white} data-testid="logo-y2" />
      <V1 white={white} data-testid="logo-v1" />
    </Logo>
  );
};

export default Loading;
