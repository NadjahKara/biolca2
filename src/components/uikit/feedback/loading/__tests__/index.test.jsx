import { render, screen } from "@testing-library/react";
import Loading from "..";

describe("Loading", () => {
  it("should render", () => {
    render(<Loading />);
    expect(screen.getByTestId("logo-y1")).toBeInTheDocument();
    expect(screen.getByTestId("logo-y2")).toBeInTheDocument();
    expect(screen.getByTestId("logo-b1")).toBeInTheDocument();
    expect(screen.getByTestId("logo-v1")).toBeInTheDocument();
  });

  it("should render in color", () => {
    render(<Loading />);
    expect(screen.getByTestId("logo-y1")).toHaveStyle(`background: #fdc627;`);
    expect(screen.getByTestId("logo-y2")).toHaveStyle(`background: #fdc627;`);
    expect(screen.getByTestId("logo-b1")).toHaveStyle(`background: #0D2D44;`);
    expect(screen.getByTestId("logo-v1")).toHaveStyle(`background: #1caf8d;`);
  });

  it("should render in white", () => {
    render(<Loading white={true} />);
    expect(screen.getByTestId("logo-y1")).toHaveStyle(
      `background-color: var(--neutral-100)`
    );
    expect(screen.getByTestId("logo-y2")).toHaveStyle(
      `background-color: var(--neutral-100)`
    );
    expect(screen.getByTestId("logo-b1")).toHaveStyle(
      `background-color: var(--neutral-100)`
    );
    expect(screen.getByTestId("logo-v1")).toHaveStyle(
      `background-color: var(--neutral-100)`
    );
  });
});
