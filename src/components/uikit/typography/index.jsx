import React from "react";
import {
  StyledPageTitle,
  StyledPageSubTitle,
  StyledHeading,
  StyledSubheading,
  StyledTitle,
  StyledBody,
  StyledCaption
} from "./styles";

export const Text = (props) => {
  const { children, variant = "Body" } = props;
  return variant === "PageTitle" ? (
    <StyledPageTitle {...props}>{children}</StyledPageTitle>
  ) : variant === "PageSubTitle" ? (
    <StyledPageSubTitle {...props}>{children}</StyledPageSubTitle>
  ) : variant === "Heading" ? (
    <StyledHeading {...props}>{children}</StyledHeading>
  ) : variant === "SubHeading" ? (
    <StyledSubheading {...props}>{children}</StyledSubheading>
  ) : variant === "Title" ? (
    <StyledTitle {...props}>{children}</StyledTitle>
  ) : variant === "Body" ? (
    <StyledBody {...props}>{children}</StyledBody>
  ) : (
    <StyledCaption {...props}>{children}</StyledCaption>
  );
};
