import styled from "styled-components";

export const StyledPageTitle = styled("h1")`
  transition: 150ms ease-in-out;
  font-family: "PT Sans", sans-serif;
  font-weight: bold;
  font-size: 2rem;
  line-height: 2rem;
  ${(props) =>
    props.inverted ? "color : var(--text-inverted)" : "color : var(--text)"};
`;

export const StyledPageSubTitle = styled("h2")`
  transition: 150ms ease-in-out;
  font-weight: normal;
  line-height: 1.5rem;
  font-size: 1.25rem;
  ${(props) =>
    props.inverted ? "color : var(--text-inverted)" : "color : var(--text)"};
`;

export const StyledHeading = styled("h1")`
  transition: 150ms ease-in-out;
  font-family: "PT Sans", sans-serif;
  font-weight: bold;
  line-height: 1.5rem;
  font-size: 1.5rem;
  ${(props) =>
    props.inverted ? "color : var(--text-inverted)" : "color : var(--text)"};
`;

export const StyledSubheading = styled("h2")`
  transition: 150ms ease-in-out;
  font-weight: normal;
  line-height: 1rem;
  font-size: 0.75rem;
  ${(props) =>
    props.inverted ? "color : var(--text-inverted)" : "color : var(--text)"};
`;

export const StyledTitle = styled("h1")`
  transition: 150ms ease-in-out;
  font-weight: medium;
  line-height: 1.25rem;
  font-size: 0.875rem;
  ${(props) =>
    !props.primary
      ? props.inverted
        ? "color : var(--text-inverted)"
        : "color : var(--text)"
      : ""};
`;

export const StyledBody = styled("p")`
  transition: 150ms ease-in-out;
  font-weight: normal;
  line-height: 1.25rem;
  font-size: 0.875rem;
  ${(props) =>
    props.inverted ? "color : var(--text-inverted)" : "color : var(--text)"};
  ${({ color }) => color && `color : ${color} !important;`};
`;

export const StyledCaption = styled("p")`
  transition: 150ms ease-in-out;
  font-weight: normal;
  line-height: 1rem;
  font-size: 0.75rem;
  ${(props) =>
    props.inverted ? "color : var(--text-inverted)" : "color : var(--text)"};
  ${({ color }) => color && `color : ${color} !important;`};
`;
