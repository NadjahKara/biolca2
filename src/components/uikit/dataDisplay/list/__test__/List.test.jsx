import { render, screen } from "@testing-library/react";
import { List } from "..";

describe("List", () => {
  it("should render List children", () => {
    // given
    render(<List children={<h1>hello test</h1>} />);
    // when
    const listContentElement = screen.getByText(/hello test/i);
    // then
    expect(listContentElement).toBeInTheDocument();
  });
});
