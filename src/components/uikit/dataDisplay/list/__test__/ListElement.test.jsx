import { render, screen } from "@testing-library/react";
import { ListElement } from "..";

describe("ListElement", () => {
  it("should render title", () => {
    // given
    render(<ListElement title="Test title" />);
    // when
    const titleElement = screen.getByText(/test title/i);
    // then
    expect(titleElement).toBeInTheDocument();
  });

  it("should render description", () => {
    // given
    render(<ListElement title="Test title" description="description random" />);
    // when
    const descriptionElement = screen.getByText(/description random/i);
    // then
    expect(descriptionElement).toBeInTheDocument();
  });

  it("should render children", () => {
    // given
    render(
      <ListElement
        title="Test title"
        description="description random"
        children={<h1>great child</h1>}
      />
    );
    // when
    const descriptionElement = screen.getByText(/great child/i);
    // then
    expect(descriptionElement).toBeInTheDocument();
  });
});
