import React from "react";
import styled from "styled-components";
import { Text } from "../../typography";

/* ================================================================= */
/*                              STYLE                                */
/* ================================================================= */

// List

const ListWrapper = styled("div")`
  position: relative;
  display: flex;
  flex-direction: column;
  gap: 16px;
  transition: 150ms ease-in-out;
  width: 100%;
  box-sizing: border-box;
`;

// ListElement

const Bg = styled("div")`
  position: relative;
  display: flex;
  justify-content: space-between;
  transition: 150ms ease-in-out;
  height: 80px;
  width: 100%;
  box-sizing: border-box;
  padding: 16px;
  background-color: var(--neutral-100);
  border-radius: 8px;
`;

const Info = styled("div")`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding: 0;
  margin-right: 80px;
`;

const Description = styled("div")`
  margin-top: auto;
`;

const ListBody = styled("div")`
  display: flex;
  align-items: center;
  flex-direction: row;
  height: 100%;
  margin-right: 4px;
`;

/* ================================================================= */
/*                            COMPONENTS                             */
/* ================================================================= */

/**
 * Display List of ListElement
 * @param {React.ReactNode} children component display inside the List, must
 * be a list of ListElement
 */
function List({ children }) {
  return <ListWrapper>{children}</ListWrapper>;
}

/**
 * Display ListElement (must be used inside a List)
 * @param {string} title Title display at the top of the element
 * @param {string} description display at the bottom of the Title
 * @param {React.ReactNode} children component display inside the element body
 */
function ListElement({ title, description, children }) {
  return (
    <Bg>
      <Info>
        <Text variant="Heading">{title}</Text>
        <Description>
          <Text variant="Body">{description}</Text>
        </Description>
      </Info>
      <ListBody>{children}</ListBody>
    </Bg>
  );
}

export { List, ListElement };
