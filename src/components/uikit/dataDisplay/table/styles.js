import styled from "styled-components";

export const StyledTable = styled.table`
  table-layout: auto;
  width: 100%;
  overflow: auto;
  // eq to divide-y
  border-top-width: 1px;
  border-bottom-width: 1px;
  border-color: var(--neutral-200);
  position: relative;
  min-height: ${({ minHeight }) => minHeight ?? "9rem"};
`;
