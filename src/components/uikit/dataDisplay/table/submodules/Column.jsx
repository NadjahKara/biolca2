import React from "react";
import { Text } from "../../../typography";
import { Th } from "./styles";

export const Column = (props) => {
  const { title } = props;
  return (
    <Th {...props}>
      <Text variant="Title" inverted>
        {title}
      </Text>
    </Th>
  );
};
