import React from "react";
import { Text } from "../../../typography";
import { Td } from "./styles";

export const RowCell = (props) => {
  let { row, onClick, children } = props;
  return (
    <Td
      onClick={async () => {
        if (onClick) {
          await onClick(row);
          return;
        }
      }}
      {...props}>
      <Text variant="Body" title={children}>
        {children}
      </Text>
    </Td>
  );
};
