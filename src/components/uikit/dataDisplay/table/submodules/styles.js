import styled from "styled-components";

export const Th = styled("th")`
  position: relative;
  background: var(--neutral-600);
  height: 40px;
  width: min-content;
  text-transform: uppercase;
  padding-left: 0.75rem;
  padding-right: 0.75rem;
  padding-top: 1rem;
  padding-bottom: 1rem;
  z-index: 20;
  text-align: center;
  // ${({ childrens }) => childrens && "text-align: center; "}
  ${({ fixed }) =>
    fixed &&
    "position:sticky; top : 0; left : 0; z-index: 30; background : var(--neutral-600); box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);"}
  ${({ width }) => width && `min-width : ${width}px`}
`;

export const Td = styled("td")`
  padding-left: 0.75rem;
  padding-right: 0.75rem;
  padding-top: 1rem;
  padding-bottom: 1rem;
  height: 40px;
  max-height: 40px;
  z-index: 20;
  border: 1px solid var(--neutral-300)
    ${({ column }) =>
      column.fixed &&
      "position:sticky; top : 0; left : 0; z-index: 40; box-shadow: 0 1px 2px 0 rgb(0 0 0 / 0.05);"};

  ${({ column }) => column?.width && `min-width : ${column?.width}px`};

  &:first-child {
    border-right: none;
  }
  &:last-child {
    border-right: none;
  }
  p {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    ${({ column }) => column?.width && `width : ${column?.width}px`};
  }
`;
