import React from "react";
import styled from "styled-components";

import { EmptyState, Loading } from "../../feedback";
import { Checkbox } from "../../inputs";
import { StyledTable } from "./styles";
import { Column } from "./submodules/Column";
import { RowCell } from "./submodules/Row";

export const LogoWrapper = styled("div")`
  height: 104px;
  width: 104px;
`;

const Table = ({
  columns,
  dataSource,
  loading,
  rowClassName = (_) => "",
  rowKey = (_) => "",
  onRowClick,
  rowSelection,
  minHeight = "9rem",
  ...props
}) => {
  return (
    <>
      <div className={"my-2 h-full w-full max-h-full overflow-y-auto relative"}>
        {loading && (
          <div className="z-50 h-full w-full flex justify-center items-center absolute top-0 left-0 bg-opacity-60 bg-black">
            <LogoWrapper>
              <Loading />
            </LogoWrapper>
          </div>
        )}
        <div className="w-full h-full">
          <div className="py-2 align-middle inline-block w-full h-full px-1">
            <div className="max-h-full w-full ring-2 ring-gray-200 overflow-auto rounded-md">
              <StyledTable minHeight={minHeight}>
                <thead className="bg-neutral-600 sticky top-0 z-40 shadow">
                  {/* main columns */}
                  <tr>
                    {rowSelection && (
                      <th
                        className="w-min px-6 py-3 text-center text-xs text-textInverted font-medium uppercase tracking-wider"
                        key={`head-select`}
                        rowSpan={2}
                      >
                        <Checkbox
                          checked={
                            rowSelection?.selectedRowKeys?.length ===
                            dataSource?.length
                          }
                          onCheck={(checked) => {
                            let aux = [];
                            if (checked) {
                              aux = [...dataSource];
                            }
                            rowSelection?.onChange(aux);
                          }}
                        />
                      </th>
                    )}
                    {columns.map(
                      ({ title, childrens, dataIndex, width, fixed }) => (
                        <Column
                          key={`head-${dataIndex}`}
                          colSpan={
                            ![null, undefined].includes(childrens) &&
                            childrens?.length
                          }
                          rowSpan={[null, undefined].includes(childrens) && 2}
                          title={title}
                          width={width}
                          fixed={fixed}
                          childrens={childrens}
                        />
                      )
                    )}
                  </tr>
                  {/* sub columns */}
                  <tr>
                    {columns.flatMap(({ childrens, dataIndex }) => {
                      return (childrens ?? []).map(
                        ({ title, width }, index) => (
                          <Column
                            scope="col"
                            className="text-gray-500"
                            key={`head-${dataIndex}-${index}`}
                            width={width}
                            title={title}
                          />
                        )
                      );
                    })}
                  </tr>
                </thead>
                <tbody className="bg-white flex-1 w-full divide-y divide-neutral-200 overflow-x-auto overflow-y-auto">
                  {dataSource.map((item, rowIndex) => {
                    return (
                      <tr
                        className={`cursor-pointer bg-neutral-100 hover:bg-neutral-200 transition ${rowClassName(
                          item
                        )}`}
                        key={rowKey(item)}
                      >
                        {rowSelection && (
                          <td
                            className={`px-6 py-4 whitespace-nowrap text-blackText-100 text-center border border-l-0 border-neutral-300`}
                            key={`column-select`}
                          >
                            <Checkbox
                              checked={rowSelection?.selectedRowKeys?.includes(
                                item.id
                              )}
                              onCheck={(checked) => {
                                const aux = dataSource?.filter((data) =>
                                  rowSelection?.selectedRowKeys.includes(
                                    data?.id
                                  )
                                );
                                if (checked) {
                                  aux.push(item);
                                } else {
                                  let index = aux.findIndex(
                                    (row) => row.id === item.id
                                  );
                                  aux.splice(index, 1);
                                }
                                rowSelection?.onChange(aux);
                              }}
                            />
                          </td>
                        )}
                        {columns.flatMap((column) => {
                          if (column.childrens) {
                            return column?.childrens.map((column, index) => {
                              return (
                                <RowCell
                                  column={column}
                                  row={item}
                                  onClick={onRowClick}
                                  key={`column-${column.dataIndex}-${index}`}
                                >
                                  {column.render
                                    ? column.render(
                                        item[column.dataIndex],
                                        item
                                      )
                                    : item[column.dataIndex]}
                                </RowCell>
                              );
                            });
                          } else
                            return (
                              <RowCell
                                column={column}
                                row={item}
                                onClick={onRowClick}
                                key={`column-${column.dataIndex}`}
                              >
                                {column.render
                                  ? column.render(item[column.dataIndex], item)
                                  : item[column.dataIndex]}
                              </RowCell>
                            );
                        })}
                      </tr>
                    );
                  })}
                  {(!loading && dataSource?.length) === 0 && (
                    <div className="absolute w-full py-8">
                      <EmptyState size="2.5rem" />
                    </div>
                  )}
                </tbody>
              </StyledTable>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Table.Col = Column;

export default Table;
