import React, { useState, useEffect } from "react";
import { Grid } from "@mui/material";
import { Text } from "../../../uikit/typography/index";
import { styled } from "@mui/system";
import { useFormContext } from "react-hook-form";
import { Table } from "../../../uikit/dataDisplay";
import { Select, SelectItem } from "../../../uikit/inputs";
import { Box } from "@mui/material";
import { Loading } from "../../../uikit/feedback";

const ModelContainer = styled("div")({
  width: "100%",
  height: 250,
  backgroundColor: "var(--neutral-300)",
  border: "1px solid",
  display: "flex"
});

const allColumns = [
  {
    title: "Attribut",
    dataIndex: "name",
    width: "150"
  },
  {
    title: "Type",
    dataIndex: "type",
    width: "250"
  }
];

export default function SelectionStep() {
  const { register, getValues, setValue } = useFormContext();

  const techModels = getValues("techModels");

  const [selectedValue, setSelectedValue] = useState(
    getValues("techModel") || {}
  );
  const [selectedRows, setSelectedRows] = useState(
    getValues("techModel.fields")
      ?.filter((field) => field.checked)
      .map((field) => field.name) || []
  );
  const [refresh, setRefresh] = useState(false);

  useEffect(() => {}, [refresh]);

  const handleCheckValue = (res) => {
    getValues(`techModel.fields`).map((field, index) => {
      const checked = res.includes(field.name);
      setValue(`techModel.fields.${index}.checked`, checked);
      setValue(`fields.${index}.checked`, checked);
      setSelectedValue(getValues("techModel"));
    });
    setRefresh(!refresh);
  };

  if (selectedValue && techModels) {
    return (
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Grid container direction="row" spacing={2} alignItems="center">
            <Grid item>
              <Text variant="Title">{"Choisir un modèle technique"}</Text>
            </Grid>
            <Grid item>
              <Text>
                <Select
                  value={selectedValue?.id}
                  defaultValue={
                    selectedValue?.id ? selectedValue?.id : undefined
                  }
                  register={register}
                  name="techModel"
                  placeholder={"Non séléctionné"}
                  onChange={(newTM) => {
                    const value = techModels?.filter(
                      (tm) => tm.id === newTM
                    )[0];
                    if (selectedValue?.name !== value?.name) {
                      const newValue = {
                        ...value,
                        fields: [...value.fields]?.map((field) => ({
                          ...field,
                          checked: false
                        }))
                      };
                      setSelectedValue(newValue);
                      setValue("techModel", newValue);
                      setValue(`fields`, newValue.fields);
                    }
                  }}
                >
                  {techModels?.map((value) => (
                    <SelectItem
                      label={value.name}
                      value={value.id}
                      key={value.id}
                    />
                  ))}
                </Select>
              </Text>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          {selectedValue?.id ? (
            <Box maxWidth={"100rem"} width="100%">
              <Text variant="Title">
                {"Selectionner les attributs du modèle à transmettre à l'objet"}
              </Text>
              <Table
                columns={allColumns}
                dataSource={getValues("techModel.fields")?.map((field) => ({
                  ...field,
                  id: field.name
                }))}
                rowSelection={{
                  onChange: (res) => {
                    const resId = res.map((field) => field.id);
                    setSelectedRows(resId);
                    handleCheckValue(resId);
                  },
                  selectedRowKeys: selectedRows
                }}
              />
            </Box>
          ) : (
            <ModelContainer />
          )}
        </Grid>
      </Grid>
    );
  } else {
    return <Loading />;
  }
}
