import { Grid } from "@mui/material";
import React from "react";
import { useFormContext } from "react-hook-form";
import { TextArea, TextInput } from "../../../uikit/inputs";
import InputText from "../../../uikit/inputs/textInput";
import { Text } from "../../../uikit/typography";

export default function RBOInformationStep() {
  const { register } = useFormContext();

  return (
    <Grid container direction="column">
      <Grid item>
        <Text variant="Title">{"Informations de l'objet métier"}</Text>
      </Grid>
      <Grid item>
        <Grid container>
          <Grid item xs={12} sm={8} md={4}>
            <InputText
              placeholder="Nom de l'objet métier"
              register={register}
              name="name"
            />
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <TextArea
          placeholder="Description de l'objet métier"
          register={register}
          name="description"
          rows={5}
        />
      </Grid>
      <Grid item>
        <Text>{"Informations du topic kafka"}</Text>
      </Grid>
      <Grid item>
        <Grid container spacing={2}>
          <Grid item xs={5}>
            <TextInput
              placeholder="Nom du topic kafka"
              register={register}
              name="kafkaSourceCustom"
            />
          </Grid>
          <Grid item xs={7}>
            <TextInput
              placeholder="Adresse du topic kafka"
              register={register}
              name="kafkaSource"
            />
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
