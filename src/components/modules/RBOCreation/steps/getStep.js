import RBODetailsStep from "./RBODetailsStep";
import RBOInformationStep from "./RBOInformationStep";
import RBOSummaryStep from "./RBOSummaryStep";
import SelectionStep from "./SelectionStep";

const steps = [
  <SelectionStep />,
  <RBOInformationStep />,
  <RBODetailsStep />,
  <RBOSummaryStep />
];

const getStep = (activeStep) => steps[activeStep];
export default getStep;
