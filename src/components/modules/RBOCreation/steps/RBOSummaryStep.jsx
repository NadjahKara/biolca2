import { Stack } from "@mui/material";
import React from "react";
import { useFormContext } from "react-hook-form";
import { Text } from "../../../uikit/typography";
import { Table } from "../../../uikit/dataDisplay";
import { Box } from "@mui/material";

const summaryColumns = [
  {
    title: "Nom de l'attribut",
    dataIndex: "name",
    width: 200
  },
  {
    title: "Type",
    dataIndex: "type",
    width: 200
  },
  {
    title: "Description",
    dataIndex: "description",
    width: 300
  }
];

export default function RBOSummaryStep() {
  const { getValues } = useFormContext();

  return (
    <Box>
      <Stack spacing={2}>
        <Text variant="Heading">{getValues("name")}</Text>
        <Text>{getValues("description")}</Text>
      </Stack>
      <Box width="100%" mt={5} maxWidth={"100rem"}>
        <Table
          columns={summaryColumns}
          dataSource={getValues("fields").filter((field) => field.checked)}
        />
      </Box>
    </Box>
  );
}
