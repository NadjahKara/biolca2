import { Box, Grid } from "@mui/material";
import React from "react";
import { useFormContext } from "react-hook-form";
import { TextInput } from "../../../uikit/inputs";
import { Text } from "../../../uikit/typography";

export default function RBODetailsStep() {
  const { register, getValues } = useFormContext();

  return (
    <Box minHeight={"400px"}>
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Text variant="Title">{"Détails de l'objet métier"}</Text>
        </Grid>
        {getValues("techModel")?.fields?.map((field, index) => {
          if (field.checked) {
            return (
              <Grid item key={field.name}>
                <Grid container direction="column">
                  <Grid item>
                    <Text variant="caption">{`${field.name} (${field.type})`}</Text>
                  </Grid>
                  <Grid item>
                    <Grid container spacing={2}>
                      <Grid item xs={4}>
                        <TextInput
                          placeholder="Nom de l'attribut"
                          register={register}
                          name={`fields.${index}.name`}
                        />
                      </Grid>
                      <Grid item xs={8}>
                        <TextInput
                          placeholder="Description de l'attribut"
                          register={register}
                          name={`fields.${index}.description`}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            );
          }
          return <></>;
        })}
      </Grid>
    </Box>
  );
}
