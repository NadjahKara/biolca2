import { screen, render } from "@testing-library/react";
import RBODetailsStep from "../steps/RBODetailsStep";

//  Mock de données pour le composant
const techModel = {
  id: "f86c3f3f-de10-4df2-92d0-36c23f57",
  name: "facture",
  jarVersion: "1.8.0",
  packageName: "monDeuxiemePaquetage",
  fields: [
    {
      name: "montant",
      checked: true,
      description: "Montant de la facture",
      source: "amount",
      type: "FIELD_FLOAT"
    },
    {
      name: "date",
      checked: false,
      description: "Date de la facture",
      source: "date",
      type: "FIELD_DATE"
    },
    {
      name: "numero",
      checked: true,
      description: "Numéro de la facture",
      source: "number",
      type: "Chaine de caractères"
    }
  ],
  refs: [
    {
      name: "entrepôt",
      description: "L'endroit où est stocké le produit",
      id: "3d022f86-65b7-435c-9852-e97dfd9ae09f"
    }
  ]
};

//  Permet de récupérer les méthodes handleSubmit, getValues et setValue
const mockGetValue = jest.fn();
jest.mock("react-hook-form", () => ({
  ...jest.requireActual("react-hook-form"),
  useFormContext: () => ({
    handleSubmit: () => jest.fn(),
    getValues: mockGetValue,
    setValue: () => jest.fn()
  })
}));

it("should successfully render", () => {
  //  given
  //  when
  //  then
  render(<RBODetailsStep />);
});

describe("RBODetailsStep is displayed correctly", () => {
  test("RBO details text is displayed", () => {
    //  given
    render(<RBODetailsStep />);

    //  when
    const text = screen.getByText("Détails de l'objet métier");

    //  then
    expect(text).toBeInTheDocument();
  });

  test("Each attribute get modification attributes", () => {
    //  given
    mockGetValue.mockReturnValue(techModel);
    render(<RBODetailsStep />);

    //  when
    let checkedNb = 0;
    techModel.fields.map((field, indexField) => {
      if (field.checked) {
        const title = screen.getByText(field.name + " (" + field.type + ")");
        const textInputName =
          screen.getAllByPlaceholderText("Nom de l'attribut")[checkedNb];
        const textInputDescription = screen.getAllByPlaceholderText(
          "Description de l'attribut"
        )[checkedNb];

        //  then
        expect(title).toBeInTheDocument();
        expect(textInputName.name).toBe("fields." + indexField + ".name");
        expect(textInputDescription.name).toBe(
          "fields." + indexField + ".description"
        );
        checkedNb += 1;
      }
    });
    expect(screen.getAllByPlaceholderText("Nom de l'attribut").length).toBe(
      checkedNb
    );
    expect(
      screen.getAllByPlaceholderText("Description de l'attribut").length
    ).toBe(checkedNb);
  });
});
