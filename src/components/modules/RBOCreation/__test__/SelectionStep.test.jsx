import { screen, render, fireEvent } from "@testing-library/react";
import SelectionStep from "../steps/SelectionStep";

//  Mock de données pour le composant
const jar = {
  techModels: [
    {
      id: "f86c3f3f-de10-4df2-92d0-cee336c23f57",
      name: "inventaire",
      jarVersion: "1.8.0",
      packageName: "monPaquetage",
      fields: [
        {
          name: "bananasNbbananasNbbananasNbbananasNbbananasNbbananasNb",
          description: "Le nombre de bananes présentes dans l'inventaire",
          source: "bananasNbbananasNbbananasNbbananasNbbananasNbbananasNb",
          type: "FIELD_INTEGERFIELD_INTEGERFIELD_INTEGERFIELD_INTEGERFIELD_INTEGERFIELD_INTEGER"
        }
      ],
      refs: [
        {
          name: "entrepôt",
          description: "L'endroit où est stocké le produit",
          id: "3d022f86-65b7-435c-9852-e97dfd9ae09f"
        }
      ]
    },
    {
      id: "f86c3f3f-de10-4df2-92d0-36c23f57",
      name: "facture",
      jarVersion: "1.8.0",
      packageName: "monDeuxiemePaquetage",
      fields: [
        {
          name: "montant",
          description: "Montant de la facture",
          source: "amount",
          type: "FIELD_FLOAT"
        },
        {
          name: "date",
          description: "Date de la facture",
          source: "date",
          type: "FIELD_DATE"
        },
        {
          name: "numero",
          description: "Numéro de la facture",
          source: "number",
          type: "Chaine de caractères"
        }
      ],
      refs: [
        {
          name: "entrepôt",
          description: "L'endroit où est stocké le produit",
          id: "3d022f86-65b7-435c-9852-e97dfd9ae09f"
        }
      ]
    }
  ]
};

//  Permet de récupérer les méthodes handleSubmit, getValues et setValue
const mockGetValue = jest.fn();
jest.mock("react-hook-form", () => ({
  ...jest.requireActual("react-hook-form"),
  useFormContext: () => ({
    handleSubmit: () => jest.fn(),
    getValues: mockGetValue,
    setValue: () => jest.fn()
  })
}));

it("should successfully render", () => {
  //  given
  //  when
  //  then
  mockGetValue.mockReturnValue(jar.techModels);
  render(<SelectionStep />);
});

describe("SelectionStep is displayed correctly", () => {
  test("Text is displayed", () => {
    //  given
    mockGetValue.mockImplementation((string) => {
      switch (string) {
        case "techModel":
          return jar.techModels;
        case "techModels":
          return jar.techModels;
        case "techModel.fields":
          return jar.techModels[0].fields;
      }
    });
    render(<SelectionStep />);

    //  when
    const text = screen.getByText("Choisir un modèle technique");

    //  then
    expect(text).toBeInTheDocument();
  });

  test("Selector is displayed", () => {
    //  given
    mockGetValue.mockImplementation((string) => {
      switch (string) {
        case "techModel":
          return jar.techModels;
        case "techModels":
          return jar.techModels;
        case "techModel.fields":
          return jar.techModels[0].fields;
      }
    });
    render(<SelectionStep />);

    //  when
    const selector = screen.getAllByText("Non séléctionné");

    //  then
    expect(selector[0]).toBeInTheDocument();
  });

  test("Selector displays the tech models name when deployed", () => {
    //  given
    mockGetValue.mockImplementation((string) => {
      switch (string) {
        case "techModel":
          return jar.techModels;
        case "techModels":
          return jar.techModels;
        case "techModel.fields":
          return jar.techModels[0].fields;
      }
    });
    render(<SelectionStep />);

    //  when
    const selector = screen.getAllByText("Non séléctionné");
    fireEvent.click(selector[1]);
    const list = selector[1].parentElement.parentElement;
    jar.techModels.find((techModel, indexTechModel) => {
      const techModelName =
        list.children[3].children[indexTechModel].children[0].innerHTML;

      //  then
      expect(techModelName).toBe(techModel.name);
    });
    expect(list).toHaveAttribute("open");
  });

  test("When a techModel is selected, the corresponding table is displayed", () => {
    //  given
    mockGetValue.mockImplementation((string) => {
      switch (string) {
        case "techModel":
          return jar.techModels;
        case "techModels":
          return jar.techModels;
        case "techModel.fields":
          return jar.techModels[0].fields;
      }
    });
    render(<SelectionStep />);

    jar.techModels.map((techModel) => {
      mockGetValue.mockImplementation((string) => {
        switch (string) {
          case "techModel":
            return techModel;
          case "techModels":
            return jar.techModels;
          case "techModel.fields":
            return techModel.fields;
        }
      });

      //  when
      const selector = screen.getAllByText("Non séléctionné");
      fireEvent.click(selector[0]);
      const techModelName = screen.getAllByText(techModel.name);
      fireEvent.click(techModelName[1]);
      const table = screen
        .getByText(
          "Selectionner les attributs du modèle à transmettre à l'objet"
        )
        .parentElement.getElementsByTagName("table")[0];
      techModel.fields.map((field, indexField) => {
        const tableBody = table.getElementsByTagName("tbody")[0];
        const row = tableBody.getElementsByTagName("tr")[indexField];
        const attribute = row
          .getElementsByTagName("td")
          .item(1)
          .getElementsByTagName("p")
          .item(0).innerHTML;
        const type = row
          .getElementsByTagName("td")
          .item(2)
          .getElementsByTagName("p")
          .item(0).innerHTML;

        //  then
        expect(attribute).toBe(field.name);
        expect(type).toBe(field.type);
      });
      expect(
        screen
          .getByText(
            "Selectionner les attributs du modèle à transmettre à l'objet"
          )
          .parentElement.getElementsByTagName("table")[0]
      ).toBeInTheDocument();
    });
  });
});
