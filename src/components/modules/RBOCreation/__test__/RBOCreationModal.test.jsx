import { screen, render } from "@testing-library/react";
import RBOCreationModal from "../RBOCreationModal";

//  Mock de données pour le composant
const jar = {
  techModels: [
    {
      id: "f86c3f3f-de10-4df2-92d0-cee336c23f57",
      name: "inventaire",
      jarVersion: "1.8.0",
      packageName: "monPaquetage",
      fields: [
        {
          name: "bananasNbbananasNbbananasNbbananasNbbananasNbbananasNb",
          description: "Le nombre de bananes présentes dans l'inventaire",
          source: "bananasNbbananasNbbananasNbbananasNbbananasNbbananasNb",
          type: "FIELD_INTEGERFIELD_INTEGERFIELD_INTEGERFIELD_INTEGERFIELD_INTEGERFIELD_INTEGER"
        }
      ],
      refs: [
        {
          name: "entrepôt",
          description: "L'endroit où est stocké le produit",
          id: "3d022f86-65b7-435c-9852-e97dfd9ae09f"
        }
      ]
    },
    {
      id: "f86c3f3f-de10-4df2-92d0-36c23f57",
      name: "facture",
      jarVersion: "1.8.0",
      packageName: "monDeuxiemePaquetage",
      fields: [
        {
          name: "montant",
          description: "Montant de la facture",
          source: "amount",
          type: "FIELD_FLOAT"
        },
        {
          name: "date",
          description: "Date de la facture",
          source: "date",
          type: "FIELD_DATE"
        },
        {
          name: "numero",
          description: "Numéro de la facture",
          source: "number",
          type: "Chaine de caractères"
        }
      ],
      refs: [
        {
          name: "entrepôt",
          description: "L'endroit où est stocké le produit",
          id: "3d022f86-65b7-435c-9852-e97dfd9ae09f"
        }
      ]
    }
  ]
};

//  Permet de récupérer les méthodes handleSubmit, getValues et setValue
const mockGetValue = jest.fn();
jest.mock("react-hook-form", () => ({
  ...jest.requireActual("react-hook-form"),
  useFormContext: () => ({
    handleSubmit: () => jest.fn(),
    getValues: mockGetValue,
    setValue: () => jest.fn()
  })
}));

it("should successfully render", () => {
  //  given
  //  when
  //  then
  render(<RBOCreationModal />);
});

describe("RBOCreationModal is displayed correctly", () => {
  test("Title is displayed", () => {
    //  given
    render(<RBOCreationModal isOpen={true} setIsOpen={jest.fn()} />);

    //  when
    const title = screen.getByText("Création d'un objet métier racine");

    //  then
    expect(title).toBeInTheDocument();
  });

  test("Separator is displayed", () => {
    //  given
    render(<RBOCreationModal isOpen={true} setIsOpen={jest.fn()} />);

    //  when
    const separator = screen.getByRole("separator");

    //  then
    expect(separator).toBeInTheDocument();
  });

  test("Stepper is displayed", () => {
    //  given
    render(<RBOCreationModal isOpen={true} setIsOpen={jest.fn()} />);

    //  when
    const stepperNumber1 = screen.getByText("1");
    const stepperNumber2 = screen.getByText("2");
    const stepperNumber3 = screen.getByText("3");
    const stepperNumber4 = screen.getByText("4");

    //  then
    expect(stepperNumber1).toBeInTheDocument();
    expect(stepperNumber2).toBeInTheDocument();
    expect(stepperNumber3).toBeInTheDocument();
    expect(stepperNumber4).toBeInTheDocument();
    expect(
      stepperNumber1.parentElement.parentElement.parentElement ==
        stepperNumber2.parentElement.parentElement.parentElement
    ).toBeTruthy();
    expect(
      stepperNumber3.parentElement.parentElement.parentElement ==
        stepperNumber4.parentElement.parentElement.parentElement
    ).toBeTruthy();
    expect(
      stepperNumber2.parentElement.parentElement.parentElement ==
        stepperNumber3.parentElement.parentElement.parentElement
    ).toBeTruthy();
  });

  test("Modal form is displayed", () => {
    //  given
    mockGetValue.mockReturnValue(jar.techModels);
    render(<RBOCreationModal isOpen={true} setIsOpen={jest.fn()} />);

    //  when
    const form = screen.getByText("Choisir un modèle technique");

    //  then
    expect(form).toBeInTheDocument();
  });

  test("Cancel button is displayed", () => {
    //  given
    render(<RBOCreationModal isOpen={true} setIsOpen={jest.fn()} />);

    //  when
    const cancelButton = screen.getByText("Annuler");

    //  then
    expect(cancelButton).toBeInTheDocument();
  });

  test("Next button is displayed", () => {
    //  given
    render(<RBOCreationModal isOpen={true} setIsOpen={jest.fn()} />);

    //  when
    const nextButton = screen.getByText("Suivant");

    //  then
    expect(nextButton).toBeInTheDocument();
  });
});
