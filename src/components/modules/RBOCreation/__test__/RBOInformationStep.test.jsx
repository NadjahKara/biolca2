import { screen, render } from "@testing-library/react";
import RBOInformationStep from "../steps/RBOInformationStep";

//  Permet de récupérer les méthodes handleSubmit et register
jest.mock("react-hook-form", () => ({
  ...jest.requireActual("react-hook-form"),
  useFormContext: () => ({
    handleSubmit: () => jest.fn(),
    register: () => jest.fn()
  })
}));

it("should successfully render", () => {
  //  given
  //  when
  //  then
  render(<RBOInformationStep />);
});

describe("SelectionStep is displayed correctly", () => {
  test("RBO information text is displayed", () => {
    //  given
    render(<RBOInformationStep />);

    //  when
    const text = screen.getByText("Informations de l'objet métier");

    //  then
    expect(text).toBeInTheDocument();
  });

  test("Kafka topic information text is displayed", () => {
    //  given
    render(<RBOInformationStep />);

    //  when
    const text = screen.getByText("Informations du topic kafka");

    //  then
    expect(text).toBeInTheDocument();
  });

  test("RBO name text input is displayed", () => {
    //  given
    render(<RBOInformationStep />);

    //  when
    const textInput = screen.getByPlaceholderText("Nom de l'objet métier");

    //  then
    expect(textInput).toBeInTheDocument();
  });

  test("RBO description text input is displayed", () => {
    //  given
    render(<RBOInformationStep />);

    //  when
    const textInput = screen.getByPlaceholderText(
      "Description de l'objet métier"
    );

    //  then
    expect(textInput).toBeInTheDocument();
  });

  test("Kafka topic name text input is displayed", () => {
    //  given
    render(<RBOInformationStep />);

    //  when
    const textInput = screen.getByPlaceholderText("Nom du topic kafka");

    //  then
    expect(textInput).toBeInTheDocument();
  });

  test("Kafka topic address text input is displayed", () => {
    //  given
    render(<RBOInformationStep />);

    //  when
    const textInput = screen.getByPlaceholderText("Adresse du topic kafka");

    //  then
    expect(textInput).toBeInTheDocument();
  });
});
