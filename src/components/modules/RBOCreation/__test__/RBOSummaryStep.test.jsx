import { screen, render } from "@testing-library/react";
import RBOSummaryStep from "../steps/RBOSummaryStep";

//  Mock de données pour le composant
const rbo = {
  name: "RBO Mock",
  description: "Ceci est un mock d'un RBO",
  fields: [
    {
      name: "montant",
      checked: true,
      description: "Montant de la facture",
      source: "amount",
      type: "FIELD_FLOAT"
    },
    {
      name: "date",
      checked: false,
      description: "Date de la facture",
      source: "date",
      type: "FIELD_DATE"
    },
    {
      name: "numero",
      checked: true,
      description: "Numéro de la facture",
      source: "number",
      type: "Chaine de caractères"
    }
  ]
};

//  Permet de récupérer la méthode getValues
const mockGetValue = jest.fn();
jest.mock("react-hook-form", () => ({
  ...jest.requireActual("react-hook-form"),
  useFormContext: () => ({
    getValues: mockGetValue
  })
}));

it("should successfully render", () => {
  //  given
  //  when
  //  then
  mockGetValue.mockReturnValue([]);
  render(<RBOSummaryStep />);
});

describe("RBOSummaryStep is displayed correctly", () => {
  test("RBO name text is displayed", () => {
    //  given
    mockGetValue.mockImplementation((string) => {
      switch (string) {
        case "name":
          return rbo.name;
        case "description":
          return rbo.description;
        case "fields":
          return rbo.fields;
      }
    });
    render(<RBOSummaryStep />);

    //  when
    const text = screen.getByText("RBO Mock");

    //  then
    expect(text).toBeInTheDocument();
  });

  test("RBO description text is displayed", () => {
    //  given
    mockGetValue.mockImplementation((string) => {
      switch (string) {
        case "name":
          return rbo.name;
        case "description":
          return rbo.description;
        case "fields":
          return rbo.fields;
      }
    });
    render(<RBOSummaryStep />);

    //  when
    const text = screen.getByText("Ceci est un mock d'un RBO");

    //  then
    expect(text).toBeInTheDocument();
  });

  test("RBO fields table is correctly displayed", () => {
    //  given
    mockGetValue.mockImplementation((string) => {
      switch (string) {
        case "name":
          return rbo.name;
        case "description":
          return rbo.description;
        case "fields":
          return rbo.fields;
      }
    });
    render(<RBOSummaryStep />);

    //  when
    const table = screen
      .getByText("Ceci est un mock d'un RBO")
      .parentElement.parentElement.getElementsByTagName("table")[0];
    let checkedNb = 0;
    rbo.fields.map((field) => {
      if (field.checked) {
        const tableBody = table.getElementsByTagName("tbody")[0];
        const row = tableBody.getElementsByTagName("tr")[checkedNb];
        const attribute = row
          .getElementsByTagName("td")
          .item(0)
          .getElementsByTagName("p")
          .item(0).innerHTML;
        const type = row
          .getElementsByTagName("td")
          .item(1)
          .getElementsByTagName("p")
          .item(0).innerHTML;
        const description = row
          .getElementsByTagName("td")
          .item(2)
          .getElementsByTagName("p")
          .item(0).innerHTML;
        checkedNb += 1;

        //  then
        expect(attribute).toBe(field.name);
        expect(type).toBe(field.type);
        expect(description).toBe(field.description);
      }
    });
    expect(
      table.getElementsByTagName("tbody")[0].getElementsByTagName("tr").length
    ).toBe(checkedNb);
  });
});
