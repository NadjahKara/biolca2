import * as yup from "yup";

const fieldSchema = yup.object().shape({
  name: yup.string().required(),
  description: yup.string().required(),
  type: yup.string().required()
});

const schemaStep1 = yup
  .object()
  .shape({
    techModel: yup
      .object()
      .shape({
        fields: yup
          .array()
          .of(fieldSchema.required())
          .compact((field) => !field?.checked)
          .min(1)
          .required()
      })
      .required(),
    fields: yup.array().of(fieldSchema.required())
  })
  .required();

const schemaStep2 = yup
  .object()
  .shape({
    name: yup.string().required(),
    description: yup.string().required(),
    kafkaSource: yup.string().required(),
    kafkaSourceCustom: yup.string().required()
  })
  .required();

const schemaStep4 = yup
  .object()
  .shape({
    name: yup.string().required(),
    description: yup.string().required(),
    kafkaSource: yup.string().required(),
    kafkaSourceCustom: yup.string().required()
  })
  .required();

const schema = yup
  .object()
  .shape({
    id: yup.string().required(),
    name: yup.string().required(),
    description: yup.string().required(),
    kafkaSource: yup.string().required(),
    kafkaSourceCustom: yup.string().required(),
    fields: yup.array().of(fieldSchema.required()).required(),
    techModel: schemaStep1
  })
  .required();

const allSchemas = [schemaStep1, schemaStep2, schemaStep1, schemaStep4];

const getSchema = (value) => {
  if (allSchemas.length > value && value >= 0) {
    return allSchemas[value];
  }
  return schema;
};

export default getSchema;
