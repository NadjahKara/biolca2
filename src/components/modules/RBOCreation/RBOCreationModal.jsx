import { Box, Grid } from "@mui/material";
import React, { useEffect, useState } from "react";
import { Stepper } from "../../uikit/feedback/index";
import { Modal } from "../../uikit/surfaces/index";
import getStep from "./steps/getStep";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm, FormProvider } from "react-hook-form";
import getSchema from "./RBOCreationSchema";
import useMediaQuery from "@mui/material/useMediaQuery";

import { getTechModels, createRBOModel } from "../../../api/index";

const steps = ["1", "2", "3", "4"];

export default function RBOCreationModal(props) {
  const { isOpen, setIsOpen, jarId } = props;

  const getAllTechModel = getTechModels(jarId);

  const isLargeScreen = useMediaQuery("(min-width:1400px)");

  const [currentStep, setCurrentStep] = useState(0);
  const [canContinue, setCanContinue] = useState(false);

  const methods = useForm({
    resolver: yupResolver(getSchema())
  });

  const { watch, reset } = methods;

  const watchAll = watch();

  const isEndOfStepper = () => currentStep === steps.length - 1;
  const isFirstStep = () => currentStep === 0;

  const handleNext = () => {
    if (!isEndOfStepper()) {
      setCurrentStep((prevActiveStep) => prevActiveStep + 1);
      setCanContinue(false);
    }
  };

  const handleBack = () => {
    if (currentStep === 0) {
      setIsOpen(false);
      reset();
      return;
    }
    setCurrentStep((prevActiveStep) => prevActiveStep - 1);
  };

  const onSubmit = (result) => {
    const techModel = result.techModel;
    const res = {
      jarId: jarId,
      techModelId: techModel.id,
      name: techModel.name,
      description: result.description,
      kafkaSource: result.kafkaSource,
      kafkaSourceCustom: result.kafkaSourceCustom,
      fields: result.fields,
      refs: result.refs
    };
    createRBOModel(res);
  };

  useEffect(() => {
    methods.setValue("techModels", getAllTechModel.techModels);
  }, [jarId]);

  useEffect(() => {
    getSchema(currentStep)
      .validate(methods.getValues())
      .then(() => {
        setCanContinue(true);
      })
      .catch(() => {
        setCanContinue(false);
      });
  }, [watchAll, currentStep, methods]);

  return (
    <Modal
      maxWidth={isLargeScreen ? (currentStep < 3 ? "40%" : "60%") : "90%"}
      isOpen={isOpen}
      setIsOpen={setIsOpen}
      title={"Création d'un objet métier racine"}
      buttonProps={{
        variant: "primary",
        children: isEndOfStepper() ? "Valider" : "Suivant",
        onClick: isEndOfStepper()
          ? () => onSubmit(methods.getValues())
          : handleNext,
        type: "button",
        form: "hook-form",
        disabled: !canContinue
      }}
      cancelButtonProps={{
        children: isFirstStep() ? "Annuler" : "Retour",
        onClick: handleBack
      }}
    >
      <Box width={"100%"}>
        <Grid container direction="column" spacing={2} justifyContent="center">
          <Grid item>
            <Box>
              <Stepper steps={steps} current={currentStep} />
            </Box>
          </Grid>
          <Grid item>
            <FormProvider {...methods}>
              <form id="hook-form"></form>
              {getStep(currentStep)}
            </FormProvider>
          </Grid>
        </Grid>
      </Box>
    </Modal>
  );
}
