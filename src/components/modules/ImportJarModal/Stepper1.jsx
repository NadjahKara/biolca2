import { DropArea } from "../../uikit/inputs";
import { FileUploader } from "react-drag-drop-files";

//Fenêtre de choix du fichier à uploader
function Stepper1({ file, setFile }) {
  const handleChange = (file) => {
    setFile(file);
  };

  //Permet de retirer le style de la zone de drag and drop par défaut
  const removeDefaultStyle = (dragging) => {
    if (
      dragging &&
      document.getElementById("Child") !== null &&
      document.getElementById("Child").children[0].childElementCount === 3
    ) {
      document.getElementById(
        "Child"
      ).children[0].children[1].style.borderStyle = "none";
    }
  };

  return (
    <FileUploader
      hoverTitle=" "
      children={
        <DropArea
          title={
            !file
              ? "Cliquer ou glisser un fichier à cette zone pour l'uploader"
              : "L'archive " + file.name + " a bien été uploadée !"
          }
        />
      }
      handleChange={handleChange}
      name="file"
      types={["JAR"]}
      onDraggingStateChange={removeDefaultStyle}
    />
  );
}
export default Stepper1;
