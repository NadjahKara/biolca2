import React, { useState } from "react";
import { Modal } from "../../uikit/surfaces";
import { Stepper } from "../../uikit/feedback";
import { Button } from "../../uikit/inputs";
import Stepper1 from "./Stepper1";
import Stepper2 from "./Stepper2";
import { createJar, deleteJar } from "../../../api/index";

function ImportJarModal() {
  //Gestion de l'affichage du modal
  const [show, setShow] = useState(false);
  const handleShow = () => setShow(true);

  //Gestion du fichier uploadé
  const [file, setFile] = useState(null);

  //Gestion de l'avancement du stepper
  const [activeStep, setActiveStep] = useState(0);

  //Gestion de l'id du JAR importé
  const [id, setId] = useState(0);

  //Contenu du modal selon l'étape
  const children = [
    <Stepper1 file={file} setFile={setFile} />,
    <Stepper2 id={id} />
  ];

  return (
    <div>
      {/* ############################################# {/*
      {/* BOUTON A SUPPRIMER AVANT MERGE, N'EST LA QUE POUR LES TESTS */}
      <Button variant="primary" children="Ouvrir Modal" onClick={handleShow} />
      {/* ############################################# */}
      <Modal
        isOpen={show}
        setIsOpen={setShow}
        cancelButtonProps={{
          children: !(activeStep > 0) ? "Annuler" : "Retour",
          onClick: () => {
            if (activeStep === 0) {
              setShow(false);
            } else {
              deleteJar(id);
              setActiveStep((prevActiveStep) => prevActiveStep - 1);
            }
            setId(null);
            setFile(null);
          }
        }}
        buttonProps={{
          children: !(activeStep > 0) ? "Importer" : "Valider",
          disabled: !file,
          variant: "primary",
          onClick: () => {
            if (activeStep === 0) {
              var formData = new FormData();
              formData.append("file", file, file.name);
              var jarId = createJar(formData);
              setId(jarId);
              setActiveStep((prevActiveStep) => prevActiveStep + 1);
            } else {
              setShow(false);
              setActiveStep(0);
              setId(null);
              setFile(null);
            }
          }
        }}
        title="Importer un modèle technique"
        children={[
          <Stepper steps={[1, 2]} current={activeStep} />,
          <div title="Child">{children[activeStep]}</div>
        ]}
      />
    </div>
  );
}
export default ImportJarModal;
