import { Text } from "../../uikit/typography";
import { Table } from "../../uikit/dataDisplay";
import { getTechModels } from "../../../api/index";

//Colonnes du tableau récapitulatif, avec les index correspondant dans le DTO reçu via GET
const columns = [
  {
    title: "Attribut",
    dataIndex: "name"
  },
  {
    title: "Type",
    dataIndex: "type"
  }
];

//Fenêtre récapitulative des modèles techniques importés
function Stepper2({ id }) {
  var techModelCollection = getTechModels(id);
  return (
    <div id="RecapTable">
      <Text style={{ right: "5%", position: "absolute" }}>
        Version du jar : {techModelCollection.techModels[0].jarVersion}
      </Text>
      {techModelCollection.techModels.map((techModel) => {
        return (
          <div>
            <Text>
              {techModel.name}, {techModel.packageName}
            </Text>
            <Table columns={columns} dataSource={techModel.fields} />
          </div>
        );
      })}
    </div>
  );
}
export default Stepper2;
