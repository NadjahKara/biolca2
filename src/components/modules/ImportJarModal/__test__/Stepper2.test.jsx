import { screen, render } from "@testing-library/react";
import Stepper2 from "../Stepper2";
import index from "../../../../api/index";

//  Mock de données pour le composant
const DTO = {
  techModels: [
    {
      id: "f86c3f3f-de10-4df2-92d0-cee336c23f57",
      name: "inventaire",
      jarVersion: "1.8.0",
      packageName: "monPaquetage",
      fields: [
        {
          name: "nb_bananes",
          description: "Le nombre de bananes présentes dans l'inventaire",
          source: "bananasNb",
          type: "FIELD_INTEGER"
        }
      ]
    },
    {
      id: "f86c3f3f-de10-4df2-92d0-cee336c23f57",
      name: "facture",
      jarVersion: "1.8.0",
      packageName: "monDeuxiemePaquetage",
      fields: [
        {
          name: "montant",
          description: "Montant de la facture",
          source: "amount",
          type: "FIELD_FLOAT"
        },
        {
          name: "date",
          description: "Date de la facture",
          source: "date",
          type: "FIELD_DATE"
        },
        {
          name: "numero",
          description: "Numéro de la facture",
          source: "number",
          type: "FIELD_INTEGER"
        }
      ]
    }
  ]
};

//  Permet de récupérer la méthode getTechModels pour le composant à tester
jest.mock("../../../../api/index", () => ({
  ...jest.requireActual("../../../../api/index"),
  getTechModels: jest.fn()
}));

it("Render is successful", () => {
  //  given

  //  when

  //  then
  index.getTechModels.mockReturnValue(DTO);
  render(<Stepper2 />);
});

describe("Stepper2 is displayed correctly", () => {
  test("JAR Version is displayed correctly", () => {
    //  given
    index.getTechModels.mockReturnValue(DTO);
    render(<Stepper2 />);

    //  when
    const jarVersion = screen.getByText("Version du jar : 1.8.0");

    //  then
    expect(jarVersion).toBeInTheDocument();
  });

  test("There is 2 tables for two tech models", () => {
    //  given
    index.getTechModels.mockReturnValue(DTO);
    render(<Stepper2 />);

    //  when
    const attributes = screen.getAllByText("Attribut");
    const types = screen.getAllByText("Type");

    //  then
    expect(attributes.length).toBe(2);
    expect(types.length).toBe(2);
  });

  test("The tables matches the DTO", () => {
    //  given
    index.getTechModels.mockReturnValue(DTO);
    render(<Stepper2 />);

    DTO.techModels.map((techModel, indexTechModel) => {
      //when
      const title = screen.getByText(
        techModel.name + ", " + techModel.packageName
      );
      const tableDiv = title.parentElement;
      const table =
        tableDiv.parentElement.getElementsByTagName("table")[indexTechModel];

      //  then
      expect(title).toBeInTheDocument();
      expect(
        tableDiv ==
          table.parentElement.parentElement.parentElement.parentElement
            .parentElement
      ).toBeTruthy();
      techModel.fields.map((field, indexField) => {
        //  when
        const tableBody = table.getElementsByTagName("tbody")[0];
        const row = tableBody.getElementsByTagName("tr")[indexField];
        const attribute = row
          .getElementsByTagName("td")
          .item(0)
          .getElementsByTagName("p")
          .item(0).innerHTML;
        const type = row
          .getElementsByTagName("td")
          .item(1)
          .getElementsByTagName("p")
          .item(0).innerHTML;

        //  then
        expect(attribute).toBe(field.name);
        expect(type).toBe(field.type);
      });
    });
  });
});
