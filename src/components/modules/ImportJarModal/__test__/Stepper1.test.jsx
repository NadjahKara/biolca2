import { screen, render } from "@testing-library/react";
import Stepper1 from "../Stepper1";

it("Render is successful", () => {
  //  given

  //  when

  //  then
  render(<Stepper1 />);
});

describe("Stepper1 is displayed correctly", () => {
  describe("Before uploading a file", () => {
    test("Title is displayed", () => {
      //  given
      render(<Stepper1 file={null} setFile={jest.fn()} />);

      //  when
      const title = screen.getByText(
        "Cliquer ou glisser un fichier à cette zone pour l'uploader"
      );

      //  then
      expect(title).toBeInTheDocument();
    });
  });

  describe("After uploading a file", () => {
    test("Title is displayed", () => {
      //  given
      render(
        <Stepper1
          file={
            new File(["mockFile"], "mockFile.jar", {
              type: "application/java-archive"
            })
          }
          setFile={jest.fn()}
        />
      );

      //  when
      const title = screen.getByText(
        "L'archive mockFile.jar a bien été uploadée !"
      );

      //  then
      expect(title).toBeInTheDocument();
    });
  });
});
