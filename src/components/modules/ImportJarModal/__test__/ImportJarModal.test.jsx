import { screen, render, fireEvent, createEvent } from "@testing-library/react";
import ImportJarModal from "../ImportJarModal";
import React from "react";
import index from "../../../../api/index";

//  Mock de données pour le composant
const DTO = {
  techModels: [
    {
      id: "f86c3f3f-de10-4df2-92d0-cee336c23f57",
      name: "inventaire",
      jarVersion: "1.8.0",
      packageName: "monPaquetage",
      fields: [
        {
          name: "nb_bananes",
          description: "Le nombre de bananes présentes dans l'inventaire",
          source: "bananasNb",
          type: "FIELD_INTEGER"
        }
      ]
    },
    {
      id: "f86c3f3f-de10-4df2-92d0-cee336c23f57",
      name: "facture",
      jarVersion: "1.8.0",
      packageName: "monDeuxiemePaquetage",
      fields: [
        {
          name: "montant",
          description: "Montant de la facture",
          source: "amount",
          type: "FIELD_FLOAT"
        },
        {
          name: "date",
          description: "Date de la facture",
          source: "date",
          type: "FIELD_DATE"
        },
        {
          name: "numero",
          description: "Numéro de la facture",
          source: "number",
          type: "FIELD_INTEGER"
        }
      ]
    }
  ]
};

//  Permet de récupérer les méthodes getTechModels, createJar et deleteJar pour le composant à tester
jest.mock("../../../../api/index", () => ({
  ...jest.requireActual("../../../../api/index"),
  getTechModels: jest.fn(),
  createJar: jest.fn(),
  deleteJar: jest.fn()
}));

it("Render is successful", () => {
  //  given

  //  when

  //  then
  render(<ImportJarModal />);
});

describe("Modal opening button", () => {
  test("Modal opening button is displayed", () => {
    //  given
    render(<ImportJarModal />);

    //  when
    let button = screen.getByText("Ouvrir Modal");
    expect(button).toBeInTheDocument();
  });

  test("Click on the modal opening button", () => {
    //  given
    render(<ImportJarModal />);
    let button = screen.getByText("Ouvrir Modal");
    fireEvent.click(button);

    //  when
    const title = screen.getByText("Importer un modèle technique");

    //  then
    expect(title).toBeInTheDocument();
  });
});

describe("ImportJarModal is displayed correctly", () => {
  describe("Step 1", () => {
    test("Title is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);

      //  when
      const title = screen.getByText("Importer un modèle technique");

      //  then
      expect(title).toBeInTheDocument();
    });

    test("Separator is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);

      //  when
      const separator = screen.getByRole("separator");

      //  then
      expect(separator).toBeInTheDocument();
    });

    test("Stepper is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);

      //  when
      const stepperNumber1 = screen.getByText("1");
      const stepperNumber2 = screen.getByText("2");

      //  then
      expect(stepperNumber1).toBeInTheDocument();
      expect(stepperNumber2).toBeInTheDocument();
      expect(
        stepperNumber1.parentElement.parentElement.parentElement ==
          stepperNumber2.parentElement.parentElement.parentElement
      ).toBeTruthy();
    });

    test("Modal child is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);

      //  when
      const child = screen.getByTitle("Child");

      //  then
      expect(child).not.toBeEmptyDOMElement();
    });

    test("Cancel button is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);

      //  when
      const cancelButton = screen.getByText("Annuler");

      //  then
      expect(cancelButton).toBeInTheDocument();
    });

    test("Import button is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);

      //  when
      const importButton = screen.getByText("Importer");

      //  then
      expect(importButton).toBeInTheDocument();
    });
  });

  describe("Step 2", () => {
    test("Title is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);
      const dragAndDropZone = screen.getByText(
        "Cliquer ou glisser un fichier à cette zone pour l'uploader"
      );
      fireEvent.drop(dragAndDropZone, {
        dataTransfer: {
          files: [
            new File(["mockFile"], "mockFile.jar", {
              type: "application/java-archive"
            })
          ]
        }
      });
      const importButton = screen.getByText("Importer");
      fireEvent.click(importButton);

      //  when
      const title = screen.getByText("Importer un modèle technique");

      //  then
      expect(title).toBeInTheDocument();
    });

    test("Separator is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);
      const dragAndDropZone = screen.getByText(
        "Cliquer ou glisser un fichier à cette zone pour l'uploader"
      );
      fireEvent.drop(dragAndDropZone, {
        dataTransfer: {
          files: [
            new File(["mockFile"], "mockFile.jar", {
              type: "application/java-archive"
            })
          ]
        }
      });
      const importButton = screen.getByText("Importer");
      fireEvent.click(importButton);

      //  when
      const separator = screen.getByRole("separator");

      //  then
      expect(separator).toBeInTheDocument();
    });

    test("Stepper is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);
      const dragAndDropZone = screen.getByText(
        "Cliquer ou glisser un fichier à cette zone pour l'uploader"
      );
      fireEvent.drop(dragAndDropZone, {
        dataTransfer: {
          files: [
            new File(["mockFile"], "mockFile.jar", {
              type: "application/java-archive"
            })
          ]
        }
      });
      const importButton = screen.getByText("Importer");
      fireEvent.click(importButton);

      //  when
      const stepperNumber1 = screen.queryByText("1");
      const stepperNumber2 = screen.getByText("2");

      //  then
      expect(stepperNumber1).not.toBeInTheDocument();
      expect(stepperNumber2).toBeInTheDocument();
    });

    test("Modal child is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);
      const dragAndDropZone = screen.getByText(
        "Cliquer ou glisser un fichier à cette zone pour l'uploader"
      );
      fireEvent.drop(dragAndDropZone, {
        dataTransfer: {
          files: [
            new File(["mockFile"], "mockFile.jar", {
              type: "application/java-archive"
            })
          ]
        }
      });
      const importButton = screen.getByText("Importer");
      fireEvent.click(importButton);

      //  when
      const child = screen.getByTitle("Child");

      //  then
      expect(child).not.toBeEmptyDOMElement();
    });

    test("Return button is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);
      const dragAndDropZone = screen.getByText(
        "Cliquer ou glisser un fichier à cette zone pour l'uploader"
      );
      fireEvent.drop(dragAndDropZone, {
        dataTransfer: {
          files: [
            new File(["mockFile"], "mockFile.jar", {
              type: "application/java-archive"
            })
          ]
        }
      });
      const importButton = screen.getByText("Importer");
      fireEvent.click(importButton);

      //  when
      const returnButton = screen.getByText("Retour");

      //  then
      expect(returnButton).toBeInTheDocument();
    });

    test("Validate button is displayed", () => {
      //  given
      index.getTechModels.mockReturnValue(DTO);
      render(<ImportJarModal />);
      const button = screen.getByText("Ouvrir Modal");
      fireEvent.click(button);
      const dragAndDropZone = screen.getByText(
        "Cliquer ou glisser un fichier à cette zone pour l'uploader"
      );
      fireEvent.drop(dragAndDropZone, {
        dataTransfer: {
          files: [
            new File(["mockFile"], "mockFile.jar", {
              type: "application/java-archive"
            })
          ]
        }
      });
      const importButton = screen.getByText("Importer");
      fireEvent.click(importButton);

      //  when
      const validateButton = screen.getByText("Valider");

      //  then
      expect(validateButton).toBeInTheDocument();
    });
  });
});
