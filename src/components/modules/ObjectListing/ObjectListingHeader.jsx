import { Grid } from "@mui/material";
import { Logo } from "../../uikit/surfaces/index.js";
import { Text } from "../../uikit/typography/index.jsx";

/**
 * Affiche le logo + un titre + un ensemble d'éléments positionnés à droite.
 *
 * @param title Le titre du bandeau
 * @param elements Les éléments positionnés à droite
 * @returns {JSX.Element}
 * @constructor
 */
function ObjectListingHeader({ title, elements }) {
  return (
    <Grid
      container
      alignItems={"center"}
      direction={"row"}
      wrap={"nowrap"}
      justifyContent={"space-between"}
    >
      {/* Logo + Titre */}
      <Grid container item alignItems={"center"} gap={4} xs={10}>
        <Grid item>
          <Logo size={"xs"} />
        </Grid>
        <Grid item>
          <Text variant={"PageTitle"}>{title}</Text>
        </Grid>
      </Grid>

      {/* Les éléments sur la droite */}
      <Grid container item xs={1} justifyContent={"right"}>
        {elements.map((element, index) => {
          return (
            <Grid key={index} item>
              {element}
            </Grid>
          );
        })}
      </Grid>
    </Grid>
  );
}

export default ObjectListingHeader;
