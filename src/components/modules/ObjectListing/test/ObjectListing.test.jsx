import { BrowserRouter as Router } from "react-router-dom";
import { screen, render, fireEvent } from "@testing-library/react";
import ObjectListing from "../ObjectListing.jsx";
import "@testing-library/jest-dom";

//  Mock la fonction de navigation
const mockNavigate = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockNavigate
}));

//  Permet de récupérer un context Router pour le composant à tester
const MockedObjectListing = (props) => {
  return (
    <Router>
      <ObjectListing {...props} />
    </Router>
  );
};

//  Mock de données pour le composant
const mockedData = [
  {
    id: "1",
    name: "title1",
    description: "desc1"
  },
  {
    id: "2",
    name: "title2",
    description: "desc2"
  }
];

describe("props of <ObjectListing />", () => {
  describe("props:data, when there is data", () => {
    it("should display all elements", () => {
      //  given
      render(<MockedObjectListing data={mockedData} />);

      //  when
      const elements = screen.getAllByText(/title/i);

      //  then
      expect(elements).toHaveLength(mockedData.length);

      elements.forEach((element) => {
        expect(element).toBeInTheDocument();
      });
    });

    it("should display the title", () => {
      //  given
      render(<MockedObjectListing data={mockedData} />);

      //  when
      const element = screen.getByText(/title1/i);

      //  then
      expect(element).toBeInTheDocument();
    });

    it("should display the description", () => {
      //  given
      render(<MockedObjectListing data={mockedData} />);

      //  when
      const element = screen.getByText(/desc1/i);

      //  then
      expect(element).toBeInTheDocument();
    });

    it("should display the consult button", () => {
      //  given
      render(<MockedObjectListing data={mockedData} />);

      //  when
      const button = screen.getAllByText(/consulter/i)[0];

      //  then
      expect(button).toBeInTheDocument();
      expect(button).toBeEnabled();
    });
  });

  describe("props:data, when there is no data", () => {
    it("should display a message", () => {
      //  given
      render(<MockedObjectListing data={[]} />);

      //  when
      const message = screen.getByText(/aucune/i);

      //  then
      expect(message).toBeInTheDocument();
    });
  });
});

describe("event handlers of <ObjectListing />", () => {
  it("should navigate to /{baseLink}{id} when consulting an element", () => {
    //  given
    const baseLink = "link/";
    render(<MockedObjectListing data={mockedData} baseLink={baseLink} />);

    //  when
    const button = screen.getAllByText(/consulter/i)[0];
    fireEvent.click(button);

    //  then
    expect(button).toBeInTheDocument();
    expect(mockNavigate).toHaveBeenCalledTimes(1);
    expect(mockNavigate).toHaveBeenLastCalledWith(
      "/" + baseLink + mockedData[0].id
    );
  });
});
