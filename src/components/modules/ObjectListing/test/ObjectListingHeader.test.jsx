import { screen, render } from "@testing-library/react";
import { Dropdown, DropdownItem } from "../../../uikit/inputs/index.js";
import ObjectListingHeader from "../ObjectListingHeader.jsx";
import "@testing-library/jest-dom";

describe("props of <ObjectListingHeader />", () => {
  it("should display title", () => {
    //  given
    render(<ObjectListingHeader title={"titre"} elements={[]} />);

    //  when
    const elem = screen.getByText("titre");

    //  then
    expect(elem).toBeInTheDocument();
  });

  it("should display all elements in elements list", () => {
    //  given
    const elements = [
      <Dropdown label={"..."}>
        <DropdownItem label={"Nouvel objet métier personnalisé"} />
        <DropdownItem label={"Exporter des objets en CSV"} />
      </Dropdown>,
      <div>Test</div>
    ];

    render(<ObjectListingHeader elements={elements} />);

    //  when
    const elem1 = screen.getByText("...");
    const elem2 = screen.getByText("Test");

    //  then
    expect(elem1).toBeInTheDocument();
    expect(elem2).toBeInTheDocument();
  });
});

describe("intrinsic render of <ObjectListingHeader /> ", () => {
  it("should display the xs-sized logo", () => {
    //  given
    render(<ObjectListingHeader elements={[]} />);

    //  when
    const logo = screen.getByAltText("app's logo");

    //  then
    expect(logo).toBeInTheDocument();
    expect(logo).toHaveClass("xs");
  });
});
