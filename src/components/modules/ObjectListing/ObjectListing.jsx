import { List, ListElement } from "../../uikit/dataDisplay/index.js";
import { EmptyState } from "../../uikit/feedback/";
import { Button } from "../../uikit/inputs/index.js";
import { useNavigate } from "react-router-dom";

/**
 * Un listage générique d'élément qui affiche le titre + la description +
 * un bouton pour consulter cet élément.
 *
 * @param data Tableau de données à afficher dans la liste
 * @param baseLink Lien de la page de consultation des éléments
 * @returns {JSX.Element}
 * @constructor
 */
function ObjectListing({ data, baseLink }) {
  const navigate = useNavigate();

  const handleClickConsult = (id) => {
    navigate("/" + baseLink + id);
  };

  return data.length === 0 ? (
    <EmptyState size="5rem" />
  ) : (
    <List>
      {data.map((item, index) => {
        return (
          <ListElement
            key={index}
            title={item.name}
            description={item.description}
          >
            <Button
              onClick={() => {
                handleClickConsult(item.id);
              }}
            >
              Consulter
            </Button>
          </ListElement>
        );
      })}
    </List>
  );
}

export default ObjectListing;
