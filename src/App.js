import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/home";
import { Layout } from "./pages/layout";
import ReportListing from "./pages/reportListing";

function App() {
  return (
    <div className="App">
      <Router>
        <Layout>
          <Routes>
            {/* Comment seront gérées les différentes routes '/' du DAL ? */}
            <Route path="/" element={<Home />} />
            <Route path="/report-listing" element={<ReportListing />} />
          </Routes>
        </Layout>
      </Router>
    </div>
  );
}

export default App;
