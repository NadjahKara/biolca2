import { BrowserRouter as Router } from "react-router-dom";
import { screen, render, waitFor } from "@testing-library/react";
import PboListing from "../PboListing.jsx";
import "@testing-library/jest-dom";
import { getAllPbo } from "../../../api/pbo.instance.js";

jest.mock("../../../api/pbo.instance", () => ({
  getAllPbo: jest.fn()
}));

//  Permet de récupérer un context Router pour le composant à tester
const MockedPboListing = (props) => {
  return (
    <Router>
      <PboListing {...props} />
    </Router>
  );
};

//  Mock de données pour le composant
//  Voir la définition des objets renvoyés par le Back-end
const mockedData = {
  data: {
    payload: {
      pbos: [
        {
          id: "1",
          name: "title1",
          description: "desc1"
        },
        {
          id: "2",
          name: "title2",
          description: "desc2"
        }
      ]
    }
  }
};
const mockedEmpty = {
  data: {
    payload: {
      pbos: []
    }
  }
};

describe("functions of <PboListing />", () => {
  it("should fetch data on first render", async () => {
    //  given
    getAllPbo.mockResolvedValue(mockedEmpty);

    //  when
    render(<MockedPboListing />);

    //  then
    await waitFor(() => {
      expect(getAllPbo).toBeCalledTimes(1);
    });
  });

  describe("when there is data fetched", () => {
    it("should display all elements", async () => {
      //  given
      getAllPbo.mockResolvedValue(mockedData);

      //  when
      render(<MockedPboListing />);

      //  then
      await waitFor(() => {
        const elements = screen.getAllByText(/title/i);

        expect(elements).toHaveLength(mockedData.data.payload.pbos.length);
        elements.forEach((element) => {
          expect(element).toBeInTheDocument();
        });
      });
    });
  });

  describe("when there is no data fetched", () => {
    it("should display a message", async () => {
      //  given
      getAllPbo.mockResolvedValue([]);

      //  when
      render(<MockedPboListing />);

      //  then
      await waitFor(() => {
        const message = screen.getByText(/aucune/i);
        expect(message).toBeInTheDocument();
      });
    });
  });
});

describe("intrinsic render <PboListing />", () => {
  it("should display the title of the page", async () => {
    //  given
    //  Rien de plus que le composant lui-même

    //  when
    render(<MockedPboListing />);
    const title = screen.getByText(/Liste des objets métier personnalisés/i);

    //  then
    await waitFor(() => {
      expect(title).toBeInTheDocument();
    });
  });

  it("should display the correct buttons in the 3 dot dropdown", async () => {
    //  given
    //  Rien de plus que le composant lui-même

    //  when
    render(<MockedPboListing />);
    const createButton = screen.getByText(/nouvel/i);
    const exportButton = screen.getByText(/exporter/i);

    //  then
    await waitFor(() => {
      expect(createButton).toBeInTheDocument();
      expect(exportButton).toBeInTheDocument();
    });
  });
});
