import { Grid } from "@mui/material";
import { useEffect, useState } from "react";
import ObjectListingHeader from "../../components/modules/ObjectListing/ObjectListingHeader.jsx";
import ObjectListing from "../../components/modules/ObjectListing/ObjectListing.jsx";
import { Dropdown, DropdownItem } from "../../components/uikit/inputs/index.js";
import { getAllPbo } from "../../api/pbo.instance.js";
import { Header } from "../../components/uikit/surfaces";

/**
 * Affiche le listage pour la gestion des PBO.
 *
 * @returns {JSX.Element}
 * @constructor
 */
function PboListing() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData().then((data) => {
      setData(data);
    });
  }, []);

  //  TODO: rajouter un spinner le temps du wait
  const fetchData = async () => {
    try {
      const res = await getAllPbo(0, 20);
      return res.data.payload.pbos;
    } catch (err) {
      //  TODO: remplacer par un toast d'erreur
      console.log(err);
      return [];
    }
  };

  return (
    <>
      <Header />
      <Grid
        container
        sx={{
          paddingLeft: { xs: "16px", lg: "160px" },
          paddingRight: { xs: "16px", lg: "160px" },
          paddingTop: "158px"
        }}
      >
        {/* Le bandeau du haut */}
        <Grid container item sx={{ paddingBottom: "26px" }}>
          <ObjectListingHeader
            title={"Liste des objets métier personnalisés"}
            elements={[
              /* Le menu "..." */
              <Dropdown label={"..."}>
                <DropdownItem label={"Nouvel objet métier personnalisé"} />
                <DropdownItem label={"Exporter des objets en CSV"} />
              </Dropdown>
            ]}
          />
        </Grid>

        {/* Le listage des PBO */}
        <Grid container item>
          <ObjectListing data={data} baseLink={"edit-pbo/"} />
        </Grid>
      </Grid>
    </>
  );
}

export default PboListing;
