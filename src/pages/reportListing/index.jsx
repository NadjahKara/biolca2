import React, { useState } from "react";
import { Button, TextArea, TextInput } from "../../components/uikit/inputs";
import { Modal } from "../../components/uikit/surfaces";
import { Text } from "../../components/uikit/typography";
import { Snackbar,EmptyState } from "../../components/uikit/feedback";
import { useForm } from "react-hook-form";
import { addReport, getReport, getFiltredReport } from "../../service/report.service";
import { toast } from "react-toastify";
import  {useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { ListElement } from "../../components/uikit/dataDisplay";
import { BiSearch } from "react-icons/bi";
import { Logo } from "../../components/uikit/surfaces";
import "react-toastify/dist/ReactToastify.css";



function ReportListing() {
  const [reports,setReports] = useState(null);
  const [open, setOpen] = useState(false);
  const [errorTitle, setErrorTitle] = useState("");
  const [errorDescription, setErrorDescription] = useState("");
  const { register, handleSubmit } = useForm();
  const pageSize = 10;


  let navigate = useNavigate();




  const onSubmit = async (data) => {
    reset();
    if (data?.title !== "" && data?.description !== "") {
      const res = await addReport(data);
      if (res?.status === "error" || res === undefined) {
        toast("Erreur lors de la création du rapport");
      }
      setOpen(false);
    } else {
      if (data?.title === "") {
        setErrorTitle("Veuillez indiquer un nom");
      }
      if (data?.description === "") {
        setErrorDescription("Veuillez indiquer une description");
      }
    }
  };

  const reset = () => {
    setErrorTitle("");
    setErrorDescription("");
  }; 
 
 useEffect( () => {  
  fetchData();
 },[])


 

 const onChange = async (data) => {  
  
   if(data.search.length > 0) {
      const res =  await getFiltredReport(0, pageSize, data.search);
      setReports(res);
      if (res.status === "error") {
        toast("Erreur lors de l'affichage des rapports");
      }
   } else {
     fetchData();
   }
 }

 async function fetchData(){
  const res =  await getReport(0,pageSize);
  if (res?.status === "error") {
    toast("Erreur lors de l'affichage des rapports");
   }
   setReports(res); 
}

const listItems = reports?.map(r =>        
  <ListElement key={r.id}
    description={r.description}
    title = {r.title}
  >  
  
    <Button onClick={() => navigate("/report/" + r.id)} >
        Consulter
      </Button>
  </ListElement>
);

  return (
    <div className="flex items-center justify-center pt-[60px] bg-neutral-300 h-full w-full" >
      <div className="flex flex-col w-10/12 h-full py-20 gap-y-2">
        <div className="flex w-full justify-between">
          <div className="flex items-center gap-x-8">
            <Logo 
              size="xs" 
              small 
            />
            <Text variant="PageTitle" className="my-2">
              Liste des rapports 
            </Text>
          </div>
          <div className="flex items-center justify-end w-fit gap-x-2 ">
            <form onSubmit={handleSubmit(onChange)}>
              <div className="w-64">
                <TextInput
                name="search"
                defaultValue=""
                error=""
                label=""
                placeholder="Rechercher un rapport"
                underText=""
                register={register}
                />
              </div> 
            </form>
            <Button onClick={() => setOpen(true)}>Nouveau rapport</Button>
            <Modal
              isOpen={open}
              setIsOpen={setOpen}
              title="Création d'un rapport"
              maxWidth="33%"
              cancelButtonProps={{
                children: "Annuler",
                onClick: () => {
                  reset();
                  setOpen(false);
                }
              }}
              buttonProps={{
                children: "Valider",
                variant: "primary",
                type: "submit",
                form: "hook-form"
              }}
            >
              <form onSubmit={handleSubmit(onSubmit)} id="hook-form">
                <Text variant="Title">Informations du rapport</Text>
                <div className="w-fit" data-testid="title-input">
                  <TextInput
                    placeholder="Nom du rapport"
                    register={register}
                    error={errorTitle}
                    name="title"
                    shouldUnregister={true}
                  />
                </div>
                <div data-testid="description-input">
                  <TextArea
                    placeholder="Description du rapport"
                    register={register}
                    name="description"
                    error={errorDescription}
                    rows="4"
                    shouldUnregister={true}
                  />
                </div>
              </form>
            </Modal>
          </div>
        </div>
        <Snackbar />
        <div className="flex flex-col   gap-y-4">    
          {reports?.length > 0 && listItems }
          {reports?.length == 0 && 
            <div className="flex flex-col items-center"  >
              <EmptyState
                size="2.5rem"
                text="Aucune données trouvées"
              />
            </div> 
          }
          
        </div>
    
      </div> 
    </div>
  );
}

export default ReportListing;
