import { screen, render, waitFor, fireEvent } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import { addReport, getReport, getFiltredReport } from "../../../service/report.service";
import ReportListing from "../index.jsx";
import { ListElement } from "../../../components/uikit/dataDisplay";
import "@testing-library/jest-dom";

const MockReportListing = () => {
  return (
    <Router>
      <ReportListing />
    </Router>
  );
};


jest.mock("../../../service/report.service", () => ({
  addReport: jest.fn(),
  getReport: jest.fn(),
  getFiltredReport:  jest.fn()
}));


global.console = { error: jest.fn() };

const mockedData = [
  {
    id: "2GQaB38B38_FKhOVJNLF",
    title: "title1",
    description: "les 100 produits vendus en été 2021"
  },
  {
      id: "2WQaB38B38_FKhOVe9KP",
      title: "title2",
      description: " vendus en été 2021"
  }
];
const mockedEmpty = [];

describe("functions of <ReportListing />", () => {
  it("should fetch data on first render", async () => {
     //  given
     getReport.mockResolvedValue(mockedEmpty);
     //  when 
     render(<MockReportListing />);
     // then
     await waitFor(() => {
       expect(getReport).toHaveBeenCalledTimes(1);
     });
  });

  describe("when there is data fetched", () => {
    it(('should display all elements'), async () => {
      //  given
      getReport.mockResolvedValue(mockedData);
      //  when 
      render(<MockReportListing />);
      // then
      await waitFor(() => {
        const elements = screen.getAllByText(/title/i);

        expect(elements).toHaveLength(mockedData.length);
        elements.forEach((e) => {
          expect(e).toBeInTheDocument();
        });
      });
    });
  });

  describe("when there is no data fetched", () => {
    it("should display a message", async () => {
      //  given
      getReport.mockResolvedValue(mockedEmpty);

      //  when
      render(<MockReportListing />);

      //then
      await waitFor(() => {
        const message = screen.getByText(/Aucune données trouvées/i);
        expect(message).toBeInTheDocument();
      });
    });
  })
});

describe("intrinsic render <ReportListing />", () => {
  it("should display the title of the page", async () => {
    //  given
  
    //  when
    render(<MockReportListing />);
    const title = screen.getByText(/Liste des rapports/i);
  
    //  then
    await waitFor(() => {
      expect(title).toBeInTheDocument();
    });
  });
  
  it("should display the correct button nouveau rapport", async () => {
    //  given
    //  Rien 
  
    //  when
    render(<MockReportListing />);
    const button = screen.getByText(/Nouveau rapport/i);
    //  then
    await waitFor(() => {
      expect(button).toBeInTheDocument();
    });
  });
});



describe('Report listing after clicking on "Nouveau rapport" button', () => {
  const openModal = () => {
    const button = screen.getByText("Nouveau rapport");
    fireEvent.click(button);
  };

  it("should display modal", async () => {
    //  given
    render(<MockReportListing />);
    //  when
    openModal();
    //  then
    expect(screen.getByText("Création d'un rapport")).toBeInTheDocument();
  });

  it("should only display title error if title input is empty after clicking on validate", async () => {
    //  given
    render(<MockReportListing />);
    openModal();
    const description = screen.getByPlaceholderText("Description du rapport");
    const validateButton = screen.getByRole("button", { name: "Valider" });

    //  when
    fireEvent.input(description, {
      target: {
        value: "test"
      }
    });
    fireEvent.click(validateButton);

    // then
    await waitFor(() => {
      expect(screen.getByText("Nouveau rapport")).toBeInTheDocument();
      expect(
        screen.queryByText(/veuillez indiquer un nom/i)
      ).toBeInTheDocument();
      expect(
        screen.queryByText(/veuillez indiquer une description/i)
      ).toBeNull();
    });
  });

  it("should only display description error if description input is empty after clicking on validate", async () => {
    //  given
    render(<MockReportListing />);
    openModal();
    const title = screen.getByPlaceholderText("Nom du rapport");
    const validateButton = screen.getByRole("button", { name: "Valider" });

    //  when
    fireEvent.input(title, {
      target: {
        value: "test"
      }
    });
    fireEvent.click(validateButton);

    // then
    await waitFor(() => {
      expect(screen.getByText("Nouveau rapport")).toBeInTheDocument();
      expect(screen.queryByText(/veuillez indiquer un nom/i)).toBeNull();
      expect(
        screen.queryByText(/veuillez indiquer une description/i)
      ).toBeInTheDocument();
    });
  });

  it("should display both description and title error when both input are empty after clicking on validate", async () => {
    //  given
    render(<MockReportListing />);
    openModal();
    const validateButton = screen.getByRole("button", { name: "Valider" });

    //  when
    fireEvent.click(validateButton);

    //  then
    await waitFor(() => {
      expect(screen.getByText("Nouveau rapport")).toBeInTheDocument();
      expect(screen.queryByText(/veuillez indiquer un nom/i)).toBeNull();
      expect(
        screen.queryByText(/veuillez indiquer une description/i)
      ).toBeNull();
    });
  });

  it("should close modal after clicking on cancel", async () => {
    //  given
    render(<MockReportListing />);
    openModal();
    const cancelButton = screen.getByRole("button", { name: "Annuler" });

    //  when
    fireEvent.click(cancelButton);

    // then
    await waitFor(() => {
      expect(screen.queryByText("Création d'un rapport")).toBeNull();
    });
  });

  it("should close modal and diplay error if addReport call return error after clicking on validate", async () => {
    //  given
    render(<MockReportListing />);
    openModal();
    const title = screen.getByPlaceholderText("Nom du rapport");
    const description = screen.getByPlaceholderText("Description du rapport");
    const validateButton = screen.getByRole("button", { name: "Valider" });
    const mockResponse = {
      status: "error"
    };
    addReport.mockResolvedValue(mockResponse);

    //  when
    fireEvent.input(title, {
      target: {
        value: "title"
      }
    });
    fireEvent.input(description, {
      target: {
        value: "desc"
      }
    });
    fireEvent.click(validateButton);

    // then
    await waitFor(() => {
      expect(screen.queryByText("Création d'un rapport")).toBeNull();
      expect(
        screen.queryByText(/erreur lors de la création du rapport/i)
      ).toBeInTheDocument();
    });
  });

  it("should close modal and diplay error if addReport return undefined after clicking on validate", async () => {
    //  given
    render(<MockReportListing />);
    openModal();
    const title = screen.getByPlaceholderText("Nom du rapport");
    const description = screen.getByPlaceholderText("Description du rapport");
    const validateButton = screen.getByRole("button", { name: "Valider" });
    const mockResponse = undefined;
    addReport.mockResolvedValue(mockResponse);

    //  when
    fireEvent.input(title, {
      target: {
        value: "title"
      }
    });
    fireEvent.input(description, {
      target: {
        value: "desc"
      }
    });
    fireEvent.click(validateButton);

    // then
    await waitFor(() => {
      expect(screen.queryByText("Création d'un rapport")).toBeNull();
      expect(
        screen.queryByText(/erreur lors de la création du rapport/i)
      ).toBeInTheDocument();
    });
  });

  it("should close modal and not display error if status is not undefined and is not error after clicking on validate", async () => {
    //  given
    render(<MockReportListing />);
    openModal();
    const title = screen.getByPlaceholderText("Nom du rapport");
    const description = screen.getByPlaceholderText("Description du rapport");
    const validateButton = screen.getByRole("button", { name: "Valider" });
    const mockResponse = {
      status: "success"
    };
    addReport.mockResolvedValue(mockResponse);

    //  when
    fireEvent.input(title, {
      target: {
        value: "title"
      }
    });
    fireEvent.input(description, {
      target: {
        value: "desc"
      }
    });
    fireEvent.click(validateButton);

    // then
    await waitFor(() => {
      expect(screen.queryByText("Création d'un rapport")).toBeNull();
      expect(
        screen.queryByText(/erreur lors de la création du rapport/i)
      ).toBeNull();
    });
  });
});
