import styled from "styled-components";

const Container = styled.div`
  min-height: 100vh;
  background: var(--neutral-300);
  display: flex;
  flex-direction: column;
`;

const Main = styled.main`
  width: 100%;
  height: calc(100vh - 3.75rem);
`;

export const Layout = ({ children, ...props }) => {
  return (
    <Container>
      <Main>{children}</Main>
    </Container>
  );
};
