import { Grid } from "@mui/material";
import { useEffect, useState } from "react";
import { getAllRbo } from "../../api/index.js";
import ObjectListingHeader from "../../components/modules/ObjectListing/ObjectListingHeader.jsx";
import ObjectListing from "../../components/modules/ObjectListing/ObjectListing.jsx";
import { Dropdown, DropdownItem } from "../../components/uikit/inputs/index.js";
import { Header } from "../../components/uikit/surfaces";

/**
 * Affiche le listage pour la gestion des RBO.
 *
 * @returns {JSX.Element}
 * @constructor
 */
function RboListing() {
  const [data, setData] = useState([]);

  useEffect(() => {
    fetchData().then((data) => {
      setData(data);
    });
  }, []);

  //  TODO: rajouter un spinner le temps du wait
  const fetchData = async () => {
    try {
      const res = await getAllRbo(0, 20);
      return res.data.payload.rbos;
    } catch (err) {
      //  TODO: remplacer par un toast d'erreur
      console.log(err);
      return [];
    }
  };

  return (
    <>
      <Header />
      <Grid
        container
        sx={{
          paddingLeft: { xs: "16px", lg: "160px" },
          paddingRight: { xs: "16px", lg: "160px" },
          paddingTop: "158px"
        }}
      >
        {/* Le bandeau du haut */}
        <Grid container item sx={{ paddingBottom: "26px" }}>
          <ObjectListingHeader
            title={"Liste des objets métier racines"}
            elements={[
              /* Le menu "..." */
              <Dropdown label={"..."}>
                <DropdownItem label={"Importer une archive JAR"} />
                <DropdownItem label={"Nouvel objet métier racine"} />
              </Dropdown>
            ]}
          />
        </Grid>

        {/* Le listage des RBO */}
        <Grid container item>
          <ObjectListing data={data} baseLink={"edit-rbo/"} />
        </Grid>
      </Grid>
    </>
  );
}

export default RboListing;
