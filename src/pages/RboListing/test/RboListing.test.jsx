import { BrowserRouter as Router } from "react-router-dom";
import { screen, render, waitFor } from "@testing-library/react";
import { getAllRbo } from "../../../api/rbo.instance";
import RboListing from "../RboListing.jsx";
import "@testing-library/jest-dom";

jest.mock("../../../api/rbo.instance", () => ({
  getAllRbo: jest.fn()
}));

//  Permet de récupérer un context Router pour le composant à tester
const MockedRboListing = (props) => {
  return (
    <Router>
      <RboListing {...props} />
    </Router>
  );
};

//  Mock de données pour le composant
//  Voir la définition des objets renvoyés par le Back-end
const mockedData = {
  data: {
    payload: {
      rbos: [
        {
          id: "1",
          name: "title1",
          description: "desc1"
        },
        {
          id: "2",
          name: "title2",
          description: "desc2"
        }
      ]
    }
  }
};
const mockedEmpty = {
  data: {
    payload: {
      rbos: []
    }
  }
};

describe("functions of <RboListing />", () => {
  it("should fetch data on first render", async () => {
    //  given
    getAllRbo.mockResolvedValue(mockedEmpty);

    //  when
    render(<MockedRboListing />);

    //  then
    await waitFor(() => {
      expect(getAllRbo).toBeCalledTimes(1);
    });
  });

  describe("when there is data fetched", () => {
    it("should display all elements", async () => {
      //  given
      getAllRbo.mockResolvedValue(mockedData);

      //  when
      render(<MockedRboListing />);

      //  then
      await waitFor(() => {
        const elements = screen.getAllByText(/title/i);

        expect(elements).toHaveLength(mockedData.data.payload.rbos.length);
        elements.forEach((element) => {
          expect(element).toBeInTheDocument();
        });
      });
    });
  });

  describe("when there is no data fetched", () => {
    it("should display a message", async () => {
      //  given
      getAllRbo.mockResolvedValue([]);

      //  when
      render(<MockedRboListing />);

      //  then
      await waitFor(() => {
        const message = screen.getByText(/aucune/i);
        expect(message).toBeInTheDocument();
      });
    });
  });
});

describe("intrinsic render <RboListing />", () => {
  it("should display the title of the page", async () => {
    //  given
    //  Rien de plus que le composant lui-même

    //  when
    render(<MockedRboListing />);
    const title = screen.getByText(/Liste des objets métier racines/i);

    //  then
    await waitFor(() => {
      expect(title).toBeInTheDocument();
    });
  });

  it("should display the correct buttons in the 3 dot dropdown", async () => {
    //  given
    //  Rien de plus que le composant lui-même

    //  when
    render(<MockedRboListing />);
    const importButton = screen.getByText(/importer/i);
    const createButton = screen.getByText(/nouvel/i);

    //  then
    await waitFor(() => {
      expect(createButton).toBeInTheDocument();
      expect(importButton).toBeInTheDocument();
    });
  });
});
