import React, { useEffect } from "react";
import AuthPage from "../authPage";
import PboListing from "../PboListing/PboListing";
import RboListing from "../RboListing/RboListing";

import { auth } from "../../service/auth.service";
import { useNavigate } from "react-router-dom";

function Home() {
  const params = new URLSearchParams(document.location.search);
  const code = params.get("code");

  function parseJwt(token) {
    if (token == null) return null;
    var base64Url = token.split(".")[1];
    var base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
    var jsonPayload = decodeURIComponent(
      atob(base64)
        .split("")
        .map(function (c) {
          return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
        })
        .join("")
    );

    return JSON.parse(jsonPayload);
  }

  async function authentication() {
    if (code !== null) {
      await auth(code);
      navigate("/");
    }
  }
  const navigate = useNavigate();

  useEffect(() => {
    authentication();
  }, []);

  const decodedToken = parseJwt(localStorage.getItem("access_token"));

  if (code) return null;
  if (decodedToken === null) return <AuthPage />;
  if (decodedToken.resource_access.biollca.roles.includes("ROLE_ANALYST"))
    return <PboListing />;
  if (decodedToken.resource_access.biollca.roles.includes("ROLE_ADMIN"))
    return <RboListing />;
  else return <AuthPage />;
}

export default Home;
