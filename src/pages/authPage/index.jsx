import { Button } from "../../components/uikit/inputs";
import Logo from "../../components/uikit/surfaces/logo";
import { Text } from "../../components/uikit/typography";
import { Header } from "../../components/uikit/surfaces";

function AuthPage() {
  const URL = `https://preprod-sso.ollca.com/auth/realms/ollca/protocol/openid-connect/auth?response_type=code&client_id=biollca&client_secret=b11df7bf-b98d-4cb2-810d-9d1225a1c1a0&redirect_uri=${process.env.REACT_APP_URL}&grant_type=implicit`;

  const redirect = () => {
    window.location.replace(URL);
  };

  return (
    <>
      <Header />
      <div className="flex items-center justify-center h-full w-full">
        <div className="flex flex-col items-center justify-center gap-2.5 bg-white border rounded-[50px] w-1/3 h-1/3">
          <div className="mb-4" data-testid="logo">
            <Logo small={false} />
          </div>
          <Text variant="Body">Connectez-vous avec...</Text>
          <Button variant="login" onClick={redirect} data-testid="button" />
        </div>
      </div>
    </>
  );
}

export default AuthPage;
