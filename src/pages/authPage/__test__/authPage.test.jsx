import { screen, render, fireEvent } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import Home from "../index.jsx";
import "@testing-library/jest-dom";

const MockHome = () => {
  return (
    <Router>
      <Home />
    </Router>
  );
};

global.console = { error: jest.fn() };

describe("Home page when not connected", () => {
  it("should display homePage", async () => {
    //  given
    render(<MockHome />);
    //  when
    //  then
    expect(screen.getByTestId("logo")).toBeInTheDocument();
    expect(screen.getByText("Connectez-vous avec...")).toBeInTheDocument();
    expect(screen.getByTestId("button")).toBeInTheDocument();
  });

  it("should redirect on ollca login page on login button click", async () => {
    //  given
    render(<MockHome />);
    const loginButton = screen.getByTestId("button");
    delete window.location;
    window.location = Object.assign(new URL("http://localhost:3000"), {
      replace: jest.fn()
    });
    //  when
    fireEvent.click(loginButton);
    //  then
    expect(window.location.replace).toHaveBeenCalled();
  });
});
