import React from 'react';
import { Logo } from "../../components/uikit/surfaces";
// import { Button, TextInput } from '../components/uikit/inputs';
// import { Text } from "../components/uikit/typography";
// import { ListElement } from "../components/uikit/dataDisplay";
// import { BiSearch } from "react-icons/bi";
//import axios from 'axios';



function RapportModification(props) {
 
  
    return (
        <div className="bg-neutral-300 h-full">
            <div className='flex bg-neutral-200 h-20 items-center'>
                <Logo   
                size="xs" 
                small 
                />
            </div>    
            <div className='flex h-full w-full'>
                <div className='flex w-[14%] bg-white h-full'>                    
                </div>
                <div className='flex bg-neutral-300 w-full h-full items-center justify-center'>
                    <div className='flex w-2/3 h-[90%] bg-white'>
                    </div>
                </div>
            </div>       
    
                        
        </div>
    );
}

export default RapportModification;
