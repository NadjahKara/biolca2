# How to

Create a file that regroups all the related verbs for a specific route, ex :
`example.instance.js` that would deal with an "example" entity; it's the same
principle as a controller in a java env.

<p>
The exported function would return the promise with no additional treatment; ex :
</p>
```js
export const getExample = () => {
  return axios.get("my-awesome-endpoint");
};
```

When done, export the functions from the `index.js` file so that it would be
later imported from the `api` folder

---

Treatment such as mapping a body, custom throws, etc would be done in the
service folder!
