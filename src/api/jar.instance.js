import { instanceBOICC } from "./instance.js";

export const createJar = (data) => {
  return instanceBOICC
    .post("/api/boicc/jar/import", data);
};

export const deleteJar = (id) => {
  return instanceBOICC.delete("/api/boicc/jar/" + id);
};

export const getTechModels = (id) => {
  return instanceBOICC
    .get("/api/boicc/jar/" + id + "/tech-models");
};
