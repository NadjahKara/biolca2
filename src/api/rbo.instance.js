import { instanceBOICC } from "./instance.js";

export const getAllRbo = (offset, limit) => {
  return instanceBOICC.get(`/rbo/models?offset=${offset}&limit=${limit}`);
};

export const getRBOById = (id) => {
  return instanceBOICC.get(`/models/${id}`);
};

export const createRBOModel = (data) => {
  return instanceBOICC.post(`/models`, data);
};

export const updateRBOModel = (id, data) => {
  return instanceBOICC.put(`/models/${id}`, data);
};

export const deleteRBOModel = (id) => {
  return instanceBOICC.delete(`/models/${id}`);
};
