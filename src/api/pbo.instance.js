import { instanceBOICC } from "./instance.js";

export const getAllPbo = (offset, limit) => {
  return instanceBOICC.get(`/pbo/models?offset=${offset}&limit=${limit}`);
};
