import { instanceDRC } from "./instance.js";

export const getReportById = (id) => {
  return instanceDRC.get("/report/" + { id });
};

export const createReport = (data) => {
  return instanceDRC.post("/report", data);
};

export const getAllReport = (offset, limit) => {
  return instanceDRC.get("/report?offset="+offset+"&limit="+limit);
};

export const getReportByTitle = (offset, limit, title) => {
  return instanceDRC.get("/report/search?offset="+offset+"&limit="+limit+"&title="+title);
  
};


