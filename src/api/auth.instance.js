import axios from "axios";

export const loginAuth = async (code) => {
  const config = {
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    }
  };

  const form = {
    grant_type: "authorization_code",
    client_id: "biollca",
    code: code,
    redirect_uri: process.env.REACT_APP_URL,
    client_secret: "b11df7bf-b98d-4cb2-810d-9d1225a1c1a0"
  };

  var formBody = [];
  for (let property in form) {
    var encodedKey = encodeURIComponent(property);
    var encodedValue = encodeURIComponent(form[property]);
    formBody.push(encodedKey + "=" + encodedValue);
  }
  formBody = formBody.join("&");

  let response = axios.post(
    "https://preprod-sso.ollca.com/auth/realms/ollca/protocol/openid-connect/token",
    formBody,
    config
  );
  return response;
};
