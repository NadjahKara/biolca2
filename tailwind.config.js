const buillcaColors = {
  primary: {
    100: "#61E5C6",
    200: "#2CDDB4",
    300: "#1CAF8D",
    400: "#178C71",
    500: "#116955",
  },
  secondary: {
    100: "#FEE086",
    200: "#FDD55D",
    300: "#FDC627",
    400: "#FDC10D",
    500: "#F2B602",
  },
  neutral: {
    100: "#FFFFFF",
    200: "#F4F5F7",
    300: "#E1E1E1",
    400: "#737581",
    500: "#4a4b53",
    600: "#000000",
  },
  text: "#181818",
  textInverted: "#FBF4E4",
};
module.exports = {
  // content: ["./src/**/*.{html,js}"],
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        ...buillcaColors,
      },
      fontSize: {
        xs: "0.75rem",
        base: "0.875rem",
        md: "1.25rem",
        lg: "1.5rem",
        xl: "2rem",
      },
      fontFamily: {
        sans: ["Roboto Mono", "monospace"],
        pt: ["PT Sans", "sans-serif"],
      },
    },
  },
  plugins: [],
};
